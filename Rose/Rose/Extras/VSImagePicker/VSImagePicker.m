//
//  VSImagePicker.m
//  Created by Vakul on 27/07/16.
//

#import "VSImagePicker.h"

NSString *const PHOTO_GALLERY = @"Photo Gallery";
NSString *const CAMERA = @"Camera";
NSString *const CANCEL = @"Cancel";


@interface VSImagePicker () <UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (strong, nonatomic) VSImagePickerBlock myBlock;
@property (strong, nonatomic) UIViewController *myController;
@property (strong, nonatomic) UIImagePickerController *imagePicker;

@end

@implementation VSImagePicker

- (id)init {
    self = [super init];
    if (self) {
        self.allowsEditing = YES;
    }
    return self;
}

- (void)showInController:(UIViewController *)controller withCompletionBlock:(VSImagePickerBlock)block {
    self.myBlock = block;
    self.myController = controller;
    
    NSString *heading = @"Choose:";
    
    // Make UIAlertController with ActionSheet style
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:heading message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
    
    // Add actions
    // PhotoGallery
    UIAlertAction *alertActionGallery = [UIAlertAction actionWithTitle:PHOTO_GALLERY style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self showImagePickerOfType:PHOTO_GALLERY];
        [alert dismissViewControllerAnimated:YES completion:NULL];
    }];
    
    [alert addAction:alertActionGallery];
    
    // Camera
    UIAlertAction *alertActionCamera = [UIAlertAction actionWithTitle:CAMERA style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self showImagePickerOfType:CAMERA];
        [alert dismissViewControllerAnimated:YES completion:NULL];
    }];
    
    [alert addAction:alertActionCamera];
    
    
    // Cancel
    UIAlertAction *alertActionCancel = [UIAlertAction actionWithTitle:CANCEL style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [alert dismissViewControllerAnimated:YES completion:NULL];
    }];
    
    [alert addAction:alertActionCancel];
    
    // Present it
    [controller presentViewController:alert animated:YES completion:^{
        
    }];
}


- (void)showImagePickerOfType:(NSString *)type {
    UIImagePickerControllerSourceType sourceType = ([type isEqualToString:PHOTO_GALLERY]) ? UIImagePickerControllerSourceTypePhotoLibrary : UIImagePickerControllerSourceTypeCamera;
    if (![UIImagePickerController isSourceTypeAvailable:sourceType]) {
        [self.myController showAlertWithTitle:@"Oops!" message:@"Device doesn't support this feature." okButtonTitle:@"OK"];
        return;
    }
    
    self.imagePicker = [[UIImagePickerController alloc] init];
    self.imagePicker.delegate = self;
    self.imagePicker.allowsEditing = self.allowsEditing;
    self.imagePicker.sourceType = sourceType;
    [self.myController presentViewController:self.imagePicker animated:YES completion:^{
        
    }];
}

- (UIImage *)imageResize:(UIImage *)sourceImage scaleToWidth:(CGFloat)scaleToWidth {
    
    CGFloat oldWidth    = sourceImage.size.width;
    CGFloat scaleFactor = scaleToWidth / oldWidth;
    
    CGFloat newHeight   = sourceImage.size.height * scaleFactor;
    CGFloat newWidth    = oldWidth * scaleFactor;
    
    UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
    [sourceImage drawInRect:CGRectMake(0.0, 0.0, newWidth, newHeight)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

#pragma mark - ImagePicker Delegate
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    if (self.myBlock) {
        self.myBlock(nil);
    }
    
    [self.imagePicker dismissViewControllerAnimated:YES completion:^{
        
    }];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    
    UIImage *image = [info objectForKey:self.imagePicker.allowsEditing ? UIImagePickerControllerEditedImage : UIImagePickerControllerOriginalImage];

    if (image) {
        image = [self imageResize:image scaleToWidth:600];
    }
    
    if (self.myBlock) {
        self.myBlock(image);
    }
    
    [self.imagePicker dismissViewControllerAnimated:YES completion:^{
        
    }];
}


@end
