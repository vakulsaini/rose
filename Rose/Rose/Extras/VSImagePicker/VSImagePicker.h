//
//  VSImagePicker.h
//  Created by Vakul on 27/07/16.
//

#import <Foundation/Foundation.h>

typedef void (^VSImagePickerBlock) (UIImage *image);

@interface VSImagePicker : NSObject

@property (nonatomic) BOOL allowsEditing;

- (void)showInController:(UIViewController *)controller withCompletionBlock:(VSImagePickerBlock)block;

@end
