//
//  CustomBottomButton.m
//  Rose
//
//  Created by Virender on 02/01/20.
//  Copyright © 2020 QualiteSoft. All rights reserved.
//

#import "CustomBottomButton.h"
#import "UIFont+Custom.h"

@implementation CustomBottomButton


-(void)awakeFromNib{
 [super awakeFromNib];
   self.font = [UIFont fontOfSize:13.0f];
 }


@end
