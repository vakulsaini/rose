//
//  VSRefreshControl.h
//  Fancast
//
//  Created by Vakul Saini on 03/07/18.
//  Copyright © 2018 TheTiger. All rights reserved.
//

#import <Foundation/Foundation.h>

@class VSRefreshControl;

typedef void (^VSRefreshControlDidFinishBlock) (void);

@interface VSRefreshControl : NSObject

- (void)setUpOnTable:(UITableView *)tableView withCompletion:(VSRefreshControlDidFinishBlock)block;
- (void)endRefreshing;
- (void)clear;

@end
