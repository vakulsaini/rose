//
//  VSRefreshControl.m
//  Fancast
//
//  Created by Vakul Saini on 03/07/18.
//  Copyright © 2018 TheTiger. All rights reserved.
//

#import "VSRefreshControl.h"

@interface VSRefreshControl()
@property (strong, nonatomic) UIRefreshControl *refreshControl;
@property (strong, nonatomic) VSRefreshControlDidFinishBlock block;
@end

@implementation VSRefreshControl

- (id)init {
    self = [super init];
    if (self) {
        self.refreshControl = [[UIRefreshControl alloc] init];
        [self.refreshControl addTarget:self action:@selector(refresh) forControlEvents:UIControlEventValueChanged];
        // self.refreshControl.tintColor = FColorMaroon;
    }
    return self;
}

- (void)setUpOnTable:(UITableView *)tableView
      withCompletion:(VSRefreshControlDidFinishBlock)block {
    [tableView addSubview:self.refreshControl];
    self.block = block;
}

- (void)refresh {
    if (self.block) {
        self.block();
    }
}

- (void)endRefreshing {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.refreshControl endRefreshing];
    });
}

- (void)clear {
    [self endRefreshing];
    [self.refreshControl removeFromSuperview];
    self.refreshControl = nil;
}

@end
