//
//  Helper.h
//
//  Created by Vakul Saini on 14/12/18.
//  Copyright © 2018 TheTiger. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kDateFormatFullServerSide  @"yyyy-MM-dd HH:mm:ss"
#define kDateFormatLocalClientSide @"yyyy-MM-dd hh:mma"

@interface Helper : NSObject


+ (instancetype)shared;

- (NSString *)convertDateString:(NSString *)dateStr
                     fromFormat:(NSString *)fromFormat
                       toFormat:(NSString *)toFormat;

@end
