//
//  LoadMoreCell.m
//  Fancast
//
//  Created by Vakul Saini on 27/07/18.
//  Copyright © 2018 TheTiger. All rights reserved.
//

#import "LoadMoreCell.h"

@interface LoadMoreCell()
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activity;
@end

@implementation LoadMoreCell

+ (instancetype)cell {
    LoadMoreCell *_cell = [self loadFromNib];
    _cell.selectionStyle = UITableViewCellSelectionStyleNone;
    _cell.activity.color = APP_PINK_COLOR;
    return _cell;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.activity.color = APP_PINK_COLOR;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)startAnimating {
    [self.activity startAnimating];
    self.activity.hidden = NO;
}

- (void)stopAnimating {
    [self.activity stopAnimating];
    self.activity.hidden = YES;
}

@end
