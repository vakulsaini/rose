//
//  LoadMoreCell.h
//  Fancast
//
//  Created by Vakul Saini on 27/07/18.
//  Copyright © 2018 TheTiger. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoadMoreCell : UITableViewCell

+ (instancetype)cell;

- (void)startAnimating;
- (void)stopAnimating;

@end
