//
//  VSLoadMoreControl.m
//  Fancast
//
//  Created by Vakul Saini on 03/07/18.
//  Copyright © 2018 TheTiger. All rights reserved.
//

#import "VSLoadMoreControl.h"

@implementation VSLoadMoreControl

- (id)init {
    self = [super init];
    if (self) {
        self.currentPage = 0;
        self.noMoreData = NO;
        self.refreshing = NO;
        self.numberOfItemsInOnePageRequest = 5;
    }
    return self;
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    // UITableView only moves in one direction, y axis
    CGFloat currentOffset = scrollView.contentOffset.y;
    CGFloat maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height;
    
    // Change 10.0 to adjust the distance from bottom
    if (maximumOffset - currentOffset <= 10.0) {
        
        /// Load more request when, Data is available and not currently not refreshing.
        if (self.block && self.isNoMoreData == NO && self.isRefreshing == NO) {
            self.block();
        }
    }
}

@end
