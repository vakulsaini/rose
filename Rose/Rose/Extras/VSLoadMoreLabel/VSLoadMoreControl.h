//
//  VSLoadMoreControl.h
//  Fancast
//
//  Created by Vakul Saini on 03/07/18.
//  Copyright © 2018 TheTiger. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LoadMoreCell.h"

#define LOAD_MORE_CELL_HEIGHT 44.0

typedef void (^VSLoadMoreControlDidFinishBlock) (void);

@interface VSLoadMoreControl : NSObject

@property (nonatomic) NSInteger currentPage;
@property (nonatomic, getter=isNoMoreData) BOOL noMoreData;
@property (nonatomic, getter=isRefreshing) BOOL refreshing;
@property (nonatomic) NSInteger numberOfItemsInOnePageRequest;
@property (strong, nonatomic) VSLoadMoreControlDidFinishBlock block;

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate;

@end
