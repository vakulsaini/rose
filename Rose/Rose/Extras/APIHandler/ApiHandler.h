//
//  ApiHandler.h
//
//  Created by Vakul Saini on 21/05/18.
//  Copyright © 2018 TheTiger. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworking.h>

// #define MAIN_URL                        @"http://seera.ca/webservices/"
 
// #define MAIN_URL                        @"http://13.232.91.93/"
// #define MAIN_URL                        @"http://3.6.133.230/"

#define MAIN_URL                        @"http://99.79.23.145/"
#define WEB_SERVICE_LOGIN               @"loginuser"
#define WEB_SERVICE_SIGNUP              @"signup"
#define WEB_SERVICE_NEW_SIGNUP          @"profile"
#define WEB_SERVICE_FORGOT_PASSWORD     @"forgotpassword"
#define WEB_SERVICE_CHANGE_PASSWORD     @"changepassword"
#define WEB_SERVICE_PATIENT             @"patient"
#define WEB_SERVICE_ENABLE_LIST         @"enablelist"
#define WEB_SERVICE_CALL_LOG            @"calllog"
#define WEB_SERVICE_SHARE_TRANSCRIPT    @"sharetranscript"
#define WEB_SERVICE_PATIENT_TRANSCRIPT  @"transcription/patient"
#define WEB_SERVICE_TRANSCRIPT          @"transcription"
#define WEB_SERVICE_USER_TRANSCRIPT     @"transcription/user"
#define WEB_SERVICE_FCM_TOKEN           @"fcm"

#define WEB_SERVICE_ENABLE_CONSULT_NOTIFICATIONS         @"enablenotification/consultant"
#define WEB_SERVICE_ENABLE_TRANSCRIPT_NOTIFICATIONS      @"enablenotification/transcription"

#define WEB_SERVICE_NOTIFICATIONS                    @"notification"
#define WEB_SERVICE_UNREAD_NOTIFICATIONS_COUNT       @"counter"
#define WEB_SERVICE_ALERT       @"alert"


#define WEB_SERVICE_LOGOUT              @"logout"
#define WEB_SERVICE_MY_PROFILE          @"myprofile"


typedef NS_ENUM(NSInteger, APIRequestType) {
    APIRequestTypeGET = 1,
    APIRequestTypePOST = 2, 
    APIRequestTypePUT = 3,
    APIRequestTypeDELETE = 4
};

/// Few basics API response codes
typedef NS_ENUM(NSUInteger, APIResponseCode) {
    APIResponseCodeOK = 200,
    APIResponseCodeBadRequest = 400,
    APIResponseCodePageNotFound = 401
};

typedef void (^APIClientCompletion)(id response, NSError *error);
typedef void (^APIClientProgressBlock)(NSProgress *uploadProgress);

@interface ApiHandler : NSObject

+ (ApiHandler *)shared;

- (NSURLSessionDataTask *)sendHTTPRequest:(APIRequestType)requestType
                                      url:(NSString *)URL
                                   params:(NSDictionary *)params
                           withCompletion:(APIClientCompletion)completion;


- (NSURLSessionDataTask *)sendHTTPRequest:(APIRequestType)requestType
                                  fullUrl:(NSString *)URL
                                   params:(NSDictionary *)params
                           withCompletion:(APIClientCompletion)completion;

- (NSURLSessionUploadTask *)uploadImage:(UIImage *)image
                               withName:(NSString *)imageName
                                  atURL:(NSString *)URL
                            withParamas:(NSDictionary *)params
                      withProgressBlock:(APIClientProgressBlock)progressBlock
                     andCompletionBlock:(APIClientCompletion)completion;

- (NSURLSessionUploadTask *)uploadImages:(NSArray *)images
                                andFiles:(NSArray *)files
                                   atURL:(NSString *)URL
                             withParamas:(NSDictionary *)params
                       withProgressBlock:(APIClientProgressBlock)progressBlock
                      andCompletionBlock:(APIClientCompletion)completion;

@end
