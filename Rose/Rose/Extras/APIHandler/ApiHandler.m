//
//  ApiHandler.m
//
//  Created by Vakul Saini on 21/05/18.
//  Copyright © 2018 TheTiger. All rights reserved.
//

#import "ApiHandler.h"

@implementation ApiHandler


+ (ApiHandler *)shared {
    static ApiHandler *apiHandlder = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        apiHandlder = [ApiHandler new];
    });
    return apiHandlder;
}

- (NSString *)methodType:(APIRequestType)requestType {
    NSString *methodType = @"POST";
    if (requestType == APIRequestTypeGET) {
        methodType = @"GET";
    }
    else if (requestType == APIRequestTypePUT) {
        methodType = @"PUT";
    }
    else if (requestType == APIRequestTypeDELETE) {
        methodType = @"DELETE";
    }
    return methodType;
}

- (NSURLSessionDataTask *)sendHTTPRequest:(APIRequestType)requestType
                                      url:(NSString *)URL
                                   params:(NSDictionary *)params
                           withCompletion:(APIClientCompletion)completion {
    
    
    URL = [MAIN_URL stringByAppendingString:URL];
    
    NSString *methodType = [self methodType:requestType];
    
    
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] requestWithMethod:methodType URLString:URL parameters:params error:nil];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    AFJSONResponseSerializer *serializer = [AFJSONResponseSerializer serializer];
    serializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];
    manager.responseSerializer = serializer;
    
    NSURLSessionDataTask *dataTask =
    [manager dataTaskWithRequest:request uploadProgress:nil downloadProgress:nil completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        if (error) {
            NSLog(@"Error: %@", error);
        } else {
            NSLog(@"%@ %@", response, responseObject);
        }
        
        if (completion) {
            completion(responseObject, error);
        }
    }];
    
    [dataTask resume];
    return dataTask;
}


- (NSURLSessionDataTask *)sendHTTPRequest:(APIRequestType)requestType
                                  fullUrl:(NSString *)URL
                                   params:(NSDictionary *)params
                           withCompletion:(APIClientCompletion)completion {
    
    NSString *methodType = [self methodType:requestType];
    
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] requestWithMethod:methodType URLString:URL parameters:params error:nil];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    AFJSONResponseSerializer *serializer = [AFJSONResponseSerializer serializer];
    serializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];
    manager.responseSerializer = serializer;
    NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        if (error) {
            NSLog(@"Error: %@", error);
        } else {
            NSLog(@"%@ %@", response, responseObject);
        }
        if (completion) {
            completion(responseObject, error);
        }
    }];
    
    [dataTask resume];
    return dataTask;
    
}



- (NSURLSessionUploadTask *)uploadImage:(UIImage *)image
                               withName:(NSString *)imageName
                                  atURL:(NSString *)URL
                            withParamas:(NSDictionary *)params
                      withProgressBlock:(APIClientProgressBlock)progressBlock
                     andCompletionBlock:(APIClientCompletion)completion {
    
    URL = [MAIN_URL stringByAppendingString:URL];
    
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:URL parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        
        if (image) {
            NSData *imageData = UIImageJPEGRepresentation(image, 0.5);
            [formData appendPartWithFileData:imageData name:imageName fileName:@"image.jpg" mimeType:@"image/jpg"];
        }
        
    } error:nil];
    
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    AFJSONResponseSerializer *serializer = [AFJSONResponseSerializer serializer];
    serializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html", nil];
    manager.responseSerializer = serializer;
    
    NSURLSessionUploadTask *uploadTask;
    uploadTask = [manager uploadTaskWithStreamedRequest:request
                                               progress:^(NSProgress * _Nonnull uploadProgress) {
                                                   // This is not called back on the main queue.
                                                   // You are responsible for dispatching to the main queue for UI updates
                                                   if (progressBlock) {
                                                       progressBlock(uploadProgress);
                                                   }
                                               }
                                      completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
                                          
                                          if (error) {
                                              NSLog(@"Error: %@", error);
                                          } else {
                                              NSLog(@"%@ %@", response, responseObject);
                                          }
                                          
                                          if (completion) {
                                              completion(responseObject, error);
                                          }
                                      }];
    [uploadTask resume];
    return uploadTask;
}


- (NSURLSessionUploadTask *)uploadImages:(NSArray *)images
                                andFiles:(NSArray *)files
                                  atURL:(NSString *)URL
                            withParamas:(NSDictionary *)params
                      withProgressBlock:(APIClientProgressBlock)progressBlock
                     andCompletionBlock:(APIClientCompletion)completion {
    
    URL = [MAIN_URL stringByAppendingString:URL];
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:URL parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        
        for (NSDictionary *imageInfo in images) {
            NSData *imageData = UIImageJPEGRepresentation(imageInfo[@"image"], 0.5);
            [formData appendPartWithFileData:imageData name:imageInfo[@"name"] fileName:@"image.png" mimeType:@"image/png"];
        }
        
        for (NSDictionary *fileInfo in files) {
            NSData *fileData = fileInfo[@"file"];
            [formData appendPartWithFormData:fileData name:fileInfo[@"name"]];
        }
        
    } error:nil];
    
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    AFJSONResponseSerializer *serializer = [AFJSONResponseSerializer serializer];
    serializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html", nil];
    manager.responseSerializer = serializer;
    
    NSURLSessionUploadTask *uploadTask;
    uploadTask = [manager uploadTaskWithStreamedRequest:request
                                               progress:^(NSProgress * _Nonnull uploadProgress) {
                                                   // This is not called back on the main queue.
                                                   // You are responsible for dispatching to the main queue for UI updates
                                                   if (progressBlock) {
                                                       progressBlock(uploadProgress);
                                                   }
                                               }
                                      completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
                                          
                                          if (error) {
                                              NSLog(@"Error: %@", error);
                                          } else {
                                              NSLog(@"%@ %@", response, responseObject);
                                          }
                                          
                                          if (completion) {
                                              completion(responseObject, error);
                                          }
                                      }];
    [uploadTask resume];
    return uploadTask;
}

@end
