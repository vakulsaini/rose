//
//  CustomBottomLabel.h
//  Rose
//
//  Created by Virender on 02/01/20.
//  Copyright © 2020 QualiteSoft. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CustomBottomLabel : UILabel

@end

NS_ASSUME_NONNULL_END
