//
//  NSString+Custom.h
//  Fancast
//
//  Created by Vakul Saini on 26/06/18.
//  Copyright © 2018 TheTiger. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Custom)

- (NSString *)trimmed;
- (NSString *)trimSpace;
- (NSString *)replaceSpaceBy:(NSString *)str;
- (BOOL)isEmpty;
- (BOOL)isValidEmail;
+ (NSString *)timeTextFromSeconds:(NSInteger)seconds
                isAlwaysShowHours:(BOOL)alwaysShowHours;
+ (NSString *)humanReadableSeconds:(NSInteger)seconds;
- (BOOL)hasValidRangeMin:(NSInteger)min max:(NSInteger)max;
- (BOOL)isValidName;
//- (BOOL)isValidPhoneNumber;
- (NSString *)showThisIfEmpty:(NSString *)replacement;
- (NSDate *)dateWithFormat:(NSString *)format;

@end
