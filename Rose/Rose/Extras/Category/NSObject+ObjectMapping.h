//
//  NSObject+ObjectMapping.h
//  PaintPad
//
//  Created by Vakul Saini on 22/05/18.
//  Copyright © 2018 TheTiger. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (ObjectMapping)

/// Will returnn dictionary object of current class. Contains only NSNumber, NSString, NSArray, NSDictionary
- (NSDictionary *)getDictionary;
- (NSDictionary *)getDictionaryWithExludedKeys:(NSArray *)excludedKeys;

/// Set dictionary values to peroperty of this class.
- (void)setDictionary:(NSDictionary *)dictionary;
- (void)setDictionary:(NSDictionary *)dictionary withExcludedKeys:(NSArray *)excludedKeys;


@end
