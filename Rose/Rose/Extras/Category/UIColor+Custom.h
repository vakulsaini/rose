//
//  UIColor+Custom.h
//  PaintPad
//
//  Created by Vakul Saini on 15/06/18.
//  Copyright © 2018 TheTiger. All rights reserved.
//

#import <UIKit/UIKit.h>

// Some shortcuts
#define COLOR_FROM_HEX(hexRGBA)         [UIColor colorFromHex:hexRGBA]
#define HEX_STRING_FROM_COLOR(color)    [UIColor hexFromColor:color]
#define COLOR_FROM_RGBA(rgbaString)     [UIColor colorFromRGBAString:rgbaString]
#define RGBA_STRING_FROM_COLOR(color)   [UIColor rgbaStringFromColor:color]

@interface UIColor (Custom)

/// Returning UIColor object from Hex Color String
+ (UIColor *)colorFromHex:(NSString *)hex;

/// Returning Hex Color String object from UIColor instance
+ (NSString *)hexFromColor:(UIColor *)color;

/// Custom method to convert (R,G,B,A) to UIColor
+ (UIColor *)colorFromRGBAString:(NSString *)rgbaString;

/// Custom method to convert UIColor to (R,G,B,A) String
+ (NSString *)rgbaStringFromColor:(UIColor *)color;

@end
