//
//  UITextField+Custom.m
//  Fancast
//
//  Created by Vakul Saini on 06/06/18.
//  Copyright © 2018 enAct eServices. All rights reserved.
//

#import "UITextField+Custom.h"

@implementation UITextField (Custom)

- (void)setPlaceholderColor:(UIColor *)color {
    NSString *text = self.placeholder;
    if (!text) { text = @""; }
    
    NSAttributedString *attributedText = [[NSAttributedString alloc] initWithString:text attributes:@{NSForegroundColorAttributeName: color, NSFontAttributeName: self.font}];
    self.attributedPlaceholder = attributedText;
}

- (void)setPlaceholderFont:(UIFont *)font {
    NSString *text = self.placeholder;
    if (!text) { text = @""; }
    
    NSAttributedString *attributedText = [[NSAttributedString alloc] initWithString:text attributes:@{NSFontAttributeName: font}];
    self.attributedPlaceholder = attributedText;
}

- (void)setPlaceholder:(NSString *)text color:(UIColor *)color font:(UIFont *)font {
    if (!text) { text = @""; }
    
    NSAttributedString *attributedText = [[NSAttributedString alloc] initWithString:text attributes:@{NSForegroundColorAttributeName: color, NSFontAttributeName: font}];
    self.attributedPlaceholder = attributedText;
}


@end
