//
//  NSDate+Utility.h
//  PaintPad
//
//  Created by enAct eServices on 08/06/18.
//  Copyright © 2018 enAct eServices. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (Utility)

- (NSDate *)toLocal;
- (NSDate *)toUTC;

- (NSDate *)dateByAddingDays:(NSInteger)days;
- (NSInteger)daysDifferenceFrom:(NSDate *)toDateTime;

- (NSString *)stringWithFormat:(NSString *)format;

@end
