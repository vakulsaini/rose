//
//  UIColor+Custom.m
//  PaintPad
//
//  Created by Vakul Saini on 15/06/18.
//  Copyright © 2018 TheTiger. All rights reserved.
//

#import "UIColor+Custom.h"

@implementation UIColor (Custom)

+ (UIColor *)colorFromHex:(NSString *)hex {
    
    if (!hex) {
        return nil;
    }
    
    NSString *cString = [hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].uppercaseString;
    NSScanner *scanner = [[NSScanner alloc] initWithString:cString];
    
    
    // Check and remove # prefix
    NSString *prefix = [cString substringToIndex:1];
    if ([prefix isEqualToString:@"#"]) {
        [scanner setScanLocation:1]; //Skip the "#" at 0 index
    }
    
    // Hex length must be 6 (without alpha) or 8 (with alpha) characters long
    BOOL isValidHex = (cString.length == 7 || cString.length == 9);
    if (!isValidHex) {
        // Return clear color
        return [UIColor clearColor];
    }
    
    // If hex string including alpha
    BOOL isAlphaHex = (cString.length == 9);
    
    unsigned int rgbaValue = 0;
    [scanner scanHexInt:&rgbaValue];
    
    // Right to Left
    // If hex is RGBA then this is -
    // #1. Alpha
    // #2. Blue
    // #3. Green
    // #4. Red
    
    // #1. (outVal & 0xFF)                  (Also known as - 0x000000FF)
    // #2. (outVal & 0xFF00) >> 8           (Also known as - 0x0000FF00)
    // #3. (outVal & 0xFF0000) >> 16        (Also known as - 0x00FF0000)
    // #4. (outVal & 0xFF000000) >> 16
    
    
//    CGFloat red     = ((rgbaValue & 0xFF000000) >> 24) / 255.0;
//    CGFloat green   = ((rgbaValue & 0xFF0000)   >> 16) / 255.0;
//    CGFloat blue    = ((rgbaValue & 0xFF00)     >>  8) / 255.0;
//    CGFloat alpha   = isAlphaHex ? (rgbaValue & 0xFF)  / 255.0 : 1.0;

    CGFloat red     = (rgbaValue & 0xFF)  / 255.0 ;
    CGFloat green   = ((rgbaValue & 0xFF00)     >>  8) / 255.0;
    CGFloat blue    = ((rgbaValue & 0xFF0000)   >> 16) / 255.0;
    CGFloat alpha   = isAlphaHex ? ((rgbaValue & 0xFF000000) >> 24) / 255.0 : 1.0;

    
    UIColor *color = [UIColor colorWithRed:red green:green blue:blue alpha:alpha];
    return color;
}

+ (NSString *)hexFromColor:(UIColor *)color {
    
    if (!color) {
        return nil;
    }
    
    if (color == [UIColor whiteColor]) {
        // Special case, as white doesn't fall into the RGB color space
        return @"#FFFFFFFF";
    }
    
    CGFloat red;
    CGFloat blue;
    CGFloat green;
    CGFloat alpha;
    
    [color getRed:&red green:&green blue:&blue alpha:&alpha];
    
    unsigned int redDec     = red * 255;
    unsigned int greenDec   = green * 255;
    unsigned int blueDec    = blue * 255;
    unsigned int alphaDec   = alpha * 255;
    
    NSString *hexString = [NSString stringWithFormat:@"#%02x%02x%02x%02x", redDec, greenDec, blueDec, alphaDec].uppercaseString;
    
    return hexString;
}

+ (UIColor *)colorFromRGBAString:(NSString *)rgbaString {
    
    if (!rgbaString) {
        return nil;
    }
    
    // Replacing spaces with empty string
    rgbaString = [rgbaString stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    // Components
    NSArray *components = [rgbaString componentsSeparatedByString:@","];
    if ([components count] != 4) {
        // Invalid color string
        return nil;
    }
    
    CGFloat red = [components[0] floatValue];
    CGFloat grn = [components[1] floatValue];
    CGFloat blu = [components[2] floatValue];
    CGFloat alp = [components[3] floatValue];
    
    UIColor *color = [UIColor colorWithRed:red/255.0 green:grn/255.0 blue:blu/255.0 alpha:alp];
    
    return color;
}

+ (NSString *)rgbaStringFromColor:(UIColor *)color {
    
    if (!color) {
        return nil;
    }
    
    CGFloat red;
    CGFloat grn;
    CGFloat blu;
    CGFloat alp;
    
    BOOL success = [color getRed:&red green:&grn blue:&blu alpha:&alp];
    if (!success) {
        // Invalid color
        return nil;
    }
    
    NSString *rgba = [NSString stringWithFormat:@"%.1f,%.1f,%.1f,%.1f", (red*255.0), (grn*255.0), (blu*255.0), alp];
    return rgba;
}

@end
