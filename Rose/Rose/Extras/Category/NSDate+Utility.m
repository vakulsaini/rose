//
//  NSDate+Utility.m
//  PaintPad
//
//  Created by enAct eServices on 08/06/18.
//  Copyright © 2018 enAct eServices. All rights reserved.
//

#import "NSDate+Utility.h"

@implementation NSDate (Utility)

- (NSDate *)toLocal {
    NSTimeZone *tz = [NSTimeZone localTimeZone];
    NSInteger seconds = [tz secondsFromGMTForDate:self];
    return [NSDate dateWithTimeInterval:seconds sinceDate:self];
}

- (NSDate *)toUTC {
    NSTimeZone *tz = [NSTimeZone localTimeZone];
    NSInteger seconds = -[tz secondsFromGMTForDate:self];
    return [NSDate dateWithTimeInterval:seconds sinceDate:self];
}

- (NSDate *)dateByAddingDays:(NSInteger)days {
    NSDateComponents *dayComponent = [[NSDateComponents alloc] init];
    dayComponent.day = days;
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDate *afterDate = [calendar dateByAddingComponents:dayComponent toDate:self options:0];
    return afterDate;
}

- (NSInteger)daysDifferenceFrom:(NSDate *)toDateTime {
    
    NSDate *fromDate;
    NSDate *toDate;
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    [calendar rangeOfUnit:NSCalendarUnitDay startDate:&fromDate
                 interval:NULL forDate:self];
    [calendar rangeOfUnit:NSCalendarUnitDay startDate:&toDate
                 interval:NULL forDate:toDateTime];
    
    NSDateComponents *difference = [calendar components:NSCalendarUnitDay
                                               fromDate:fromDate toDate:toDate options:0];
    
    return [difference day];
}

- (NSString *)stringWithFormat:(NSString *)format {
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    df.dateFormat = format;
    return [df stringFromDate:self];
}

@end
