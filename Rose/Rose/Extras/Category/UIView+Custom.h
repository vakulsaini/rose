//
//  UIView+Custom.h
//  PaintPad
//
//  Created by Vakul Saini on 12/06/18.
//  Copyright © 2018 TheTiger. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Custom)

+ (id)loadFromNib;

- (void)makeRound;
- (void)makeRoundWithRadius:(CGFloat)radius;
- (void)makeCornersRound:(UIRectCorner)corners withRadius:(CGFloat)radius;

- (UILabel *)getLabel;
- (UIButton *)getButton;
- (UIImageView *)getImageView;
- (UITextField *)getTextField;
- (UITextView *)getTextView;
- (id)getControlOfType:(Class)type;

- (void)showWithAnimation:(BOOL)animated;
- (void)hideWithAnimation:(BOOL)animated;

- (void)fadeTransition:(CFTimeInterval)duration;
- (void)pushTransition:(CFTimeInterval)duration;

- (CGFloat)x;
- (CGFloat)y;
- (CGFloat)maxX;
- (CGFloat)maxY;
- (CGFloat)height;
- (CGFloat)width;
- (CGPoint)origin;
- (CGSize)size;

@end
