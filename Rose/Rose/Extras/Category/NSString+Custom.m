//
//  NSString+Custom.m
//  Fancast
//
//  Created by Vakul Saini on 26/06/18.
//  Copyright © 2018 TheTiger. All rights reserved.
//

#import "NSString+Custom.h"
//#import <libPhoneNumber-iOS/NBPhoneNumber.h>
//#import <libPhoneNumber-iOS/NBPhoneNumberUtil.h>

@implementation NSString (Custom)

- (NSString *)trimmed {
    return [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

- (NSString *)trimSpace {
    return [self stringByReplacingOccurrencesOfString:@" " withString:@""];
}

- (NSString *)replaceSpaceBy:(NSString *)str {
    return [self stringByReplacingOccurrencesOfString:@" " withString:str];
}

- (BOOL)isEmpty {
    return ([self trimmed].length == 0);
}

- (BOOL)isValidEmail {
    return [[NSPredicate predicateWithFormat:@"SELF MATCHES %@", @"\\A[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\z"] evaluateWithObject:self.lowercaseString];
}

+ (NSString *)timeTextFromSeconds:(NSInteger)seconds
                isAlwaysShowHours:(BOOL)alwaysShowHours {
    
    NSInteger oneMinute = 1 * 60;
    NSInteger oneHour   = oneMinute * 60;
    
    NSInteger secs = seconds;
    NSInteger hours = secs / oneHour;
    secs -= hours * oneHour;
    NSInteger minutes = secs / oneMinute;
    secs -= minutes * oneMinute;
    
    if (seconds >= oneHour) {
        NSString *time = [NSString stringWithFormat:@"%.2ld:%.2ld:%.2ld", (long)hours, (long)minutes, (long)secs];
        return  time;
    }
    else {
        NSString *time = [NSString stringWithFormat:@"%.2ld:%.2ld", (long)minutes, (long)secs];
        if (alwaysShowHours) {
            time = [NSString stringWithFormat:@"%.2ld:%.2ld:%.2ld", (long)hours, (long)minutes, (long)secs];
        }
        return  time;
    }
}

+ (NSString *)humanReadableSeconds:(NSInteger)seconds {
    NSInteger interval = labs(seconds);
    NSDateComponentsFormatter *formatter = [[NSDateComponentsFormatter alloc] init];
    formatter.allowedUnits = NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond;
    formatter.unitsStyle = NSDateComponentsFormatterUnitsStyleShort;
    return [formatter stringFromTimeInterval:interval];
}

- (BOOL)hasValidRangeMin:(NSInteger)min max:(NSInteger)max {
    BOOL success = (self.length >= min && self.length <= max);
    return success;
}

- (BOOL)isValidName {
    NSError *error = NULL;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"[a-zA-Z ]" options:0 error:&error];
    NSUInteger numberOfMatches = [regex numberOfMatchesInString:self options:0 range:NSMakeRange(0, [self length])];
    return (numberOfMatches == self.length);
}

- (NSString *)showThisIfEmpty:(NSString *)replacement {
    if ([self isEmpty]) {
        return replacement;
    }
    return self;
}


//- (BOOL)isValidPhoneNumber {
//    NBPhoneNumberUtil *phoneUtil = [[NBPhoneNumberUtil alloc] init];
//    NSError *anError = nil;
//    
//    NSLocale *currentLocale = [NSLocale currentLocale];  // get the current locale.
//    NSString *countryCode = [[currentLocale objectForKey:NSLocaleCountryCode] uppercaseString];
//    
//    NBPhoneNumber *myNumber = [phoneUtil parse:self defaultRegion:countryCode error:&anError];
//    return [phoneUtil isValidNumber:myNumber];
//}



- (NSDate *)dateWithFormat:(NSString *)format {
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    df.dateFormat = format;
    return [df dateFromString:self];
}

@end
