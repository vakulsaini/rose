//
//  UIView+Custom.m
//  PaintPad
//
//  Created by Vakul Saini on 12/06/18.
//  Copyright © 2018 TheTiger. All rights reserved.
//

#import "UIView+Custom.h"

@implementation UIView (Custom)


+ (id)loadFromNib {
    Class class = [self class];
    NSString *nibName = NSStringFromClass(class);
    NSLog(@"nibName:%@", nibName);
    NSArray *outlets = [[NSBundle mainBundle] loadNibNamed:nibName owner:self options:nil];
    id _v = nil;
    for (id outlet in outlets) {
        if ([outlet isKindOfClass:class]) {
            _v = outlet;
            break;
        }
    }
    return _v;
}



- (void)makeRound {
    [self makeRoundWithRadius:self.frame.size.width/2.0];
}

- (void)makeRoundWithRadius:(CGFloat)radius {
    self.clipsToBounds = YES;
    self.layer.cornerRadius = radius;
}

- (void)makeCornersRound:(UIRectCorner)corners withRadius:(CGFloat)radius {
    self.clipsToBounds = YES;
    UIBezierPath *bezierPath = [UIBezierPath bezierPathWithRoundedRect:self.bounds byRoundingCorners:corners cornerRadii:CGSizeMake(radius, radius)];
    CAShapeLayer *shapeLayer = [[CAShapeLayer alloc] init];
    shapeLayer.path = bezierPath.CGPath;
    self.layer.mask = shapeLayer;
}



- (UILabel *)getLabel {
    return (UILabel *)[self getControlOfType:[UILabel class]];
}

- (UIButton *)getButton {
    return (UIButton *)[self getControlOfType:[UIButton class]];
}

- (UIImageView *)getImageView {
    return (UIImageView *)[self getControlOfType:[UIImageView class]];
}

- (UITextField *)getTextField {
    return (UITextField *)[self getControlOfType:[UITextField class]];
}

- (UITextView *)getTextView {
    return (UITextView *)[self getControlOfType:[UITextView class]];
}

- (id)getControlOfType:(Class)type {
    NSArray *subviews = [self subviews];
    id result = nil;
    for (UIView *subview in subviews) {
        if ([subview isKindOfClass:type]) {
            result = subview;
            break;
        }
    }
    return result;
}


- (void)showWithAnimation:(BOOL)animated {
    
    if (!animated) {
        self.alpha = 1.0;
        [self setHidden:NO];
        return;
    }
    
    self.alpha = 0.0;
    [self setHidden:NO];
    [UIView animateWithDuration:0.2 animations:^{
        self.alpha = 1.0;
    }];
}

- (void)hideWithAnimation:(BOOL)animated {
    
    if (!animated) {
        [self setHidden:YES];
        return;
    }
    
    [UIView animateWithDuration:0.2 animations:^{
        self.alpha = 0.0;
    } completion:^(BOOL finished) {
        [self setHidden:YES];
        self.alpha = 1.0;
    }];
}





- (void)fadeTransition:(CFTimeInterval)duration {
    CATransition *animation = [[CATransition alloc] init];
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
    animation.type = kCATransitionFade;
    animation.duration = duration;
    animation.removedOnCompletion = YES;
    [self.layer addAnimation:animation forKey:kCATransitionFade];
}

- (void)pushTransition:(CFTimeInterval)duration {
    CATransition *animation = [[CATransition alloc] init];
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
    animation.type = kCATransitionPush;
    animation.duration = duration;
    animation.removedOnCompletion = YES;
    [self.layer addAnimation:animation forKey:kCATransitionPush];
}





- (CGFloat)x {
    return self.origin.x;
}

- (CGFloat)y {
    return self.origin.y;
}

- (CGFloat)maxX {
    return [self origin].x + [self width];
}

- (CGFloat)maxY {
    return [self origin].y + [self height];
}

- (CGFloat)height {
    return [self size].height;
}

- (CGFloat)width {
    return [self size].width;
}

- (CGPoint)origin {
    return self.frame.origin;
}

- (CGSize)size {
    return self.frame.size;
}

@end
