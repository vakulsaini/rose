//
//  UIViewController+Custom.h
//  PaintPad
//
//  Created by Vakul Saini on 19/10/18.
//  Copyright © 2018 TheTiger. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (Custom)

/// Only Simple title and message, No completion Block.
- (void)showAlertWithTitle:(NSString *)title
                   message:(NSString *)message
               okButtonTitle:(NSString *)buttonTitle;

/// Alert with title and message with completion Block.
- (void)showAlertWithTitle:(NSString *)title
                   message:(NSString *)message
               okButtonTitle:(NSString *)buttonTitle
       withCompletionBlock:(void (^)(NSString *, UIAlertController *alert))block;

/// Alert with title, message and cancel button with completion Block.
- (void)showAlertWithTitle:(NSString *)title
                   message:(NSString *)message
             okButtonTitle:(NSString *)buttonTitle
         cancelButtonTitle:(NSString *)cancelButtonTitle
       withCompletionBlock:(void (^)(NSString *, UIAlertController *alert))block;

/// Alert with title, message, cancel and other button with completion Block.
- (void)showAlertWithTitle:(NSString *)title
                   message:(NSString *)message
             okButtonTitle:(NSString *)buttonTitle
         cancelButtonTitle:(NSString *)cancelButtonTitle
          otherButtonTitle:(NSString *)otherButtonTitle
       withCompletionBlock:(void (^)(NSString *, UIAlertController *alert))block;

@end
