//
//  UIViewController+Custom.m
//  PaintPad
//
//  Created by Vakul Saini on 19/10/18.
//  Copyright © 2018 TheTiger. All rights reserved.
//

#import "UIViewController+Custom.h"

@implementation UIViewController (Custom)

- (void)showAlertWithTitle:(NSString *)title
                   message:(NSString *)message
             okButtonTitle:(NSString *)buttonTitle {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *saveAction = [UIAlertAction actionWithTitle:buttonTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [alert dismissViewControllerAnimated:YES completion:nil];
    }];
    [alert addAction:saveAction];
    alert.view.tintColor = APP_PINK_COLOR;
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)showAlertWithTitle:(NSString *)title
                   message:(NSString *)message
             okButtonTitle:(NSString *)buttonTitle
       withCompletionBlock:(void (^)(NSString *, UIAlertController *alert))block {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *saveAction = [UIAlertAction actionWithTitle:buttonTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        block(buttonTitle, alert);
    }];
    [alert addAction:saveAction];
    alert.view.tintColor = APP_PINK_COLOR;
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)showAlertWithTitle:(NSString *)title
                   message:(NSString *)message
             okButtonTitle:(NSString *)buttonTitle
         cancelButtonTitle:(NSString *)cancelButtonTitle
       withCompletionBlock:(void (^)(NSString *, UIAlertController *alert))block {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *saveAction = [UIAlertAction actionWithTitle:buttonTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        block(buttonTitle, alert);
    }];
    [alert addAction:saveAction];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:cancelButtonTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        block(cancelButtonTitle, alert);
    }];
    [alert addAction:cancelAction];
    alert.view.tintColor = APP_PINK_COLOR;
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)showAlertWithTitle:(NSString *)title
                   message:(NSString *)message
             okButtonTitle:(NSString *)buttonTitle
         cancelButtonTitle:(NSString *)cancelButtonTitle
          otherButtonTitle:(NSString *)otherButtonTitle
       withCompletionBlock:(void (^)(NSString *, UIAlertController *alert))block {
   
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *saveAction = [UIAlertAction actionWithTitle:buttonTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        block(buttonTitle, alert);
    }];
    [alert addAction:saveAction];
    
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:cancelButtonTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        block(cancelButtonTitle, alert);
    }];
    [alert addAction:cancelAction];
    
    UIAlertAction *otherAction = [UIAlertAction actionWithTitle:otherButtonTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        block(otherButtonTitle, alert);
    }];
    [alert addAction:otherAction];
    
    alert.view.tintColor = APP_PINK_COLOR;
    [self presentViewController:alert animated:YES completion:nil];
}

@end
