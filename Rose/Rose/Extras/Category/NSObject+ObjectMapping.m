//
//  NSObject+ObjectMapping.m
//  PaintPad
//
//  Created by Vakul Saini on 22/05/18.
//  Copyright © 2018 TheTiger. All rights reserved.
//


#import "NSObject+ObjectMapping.h"
#import <objc/runtime.h>

NSString *const KExcludedKeys = @"excludedKeys";

@implementation NSObject (ObjectMapping)

- (NSDictionary *)getDictionary {
    
    // Check there is any excluded keys
    if([self respondsToSelector:NSSelectorFromString(KExcludedKeys)]) {
        NSArray *excludedKeys = [self valueForKey:KExcludedKeys];
        if ([excludedKeys count] > 0) {
            return [self getDictionaryWithExludedKeys:excludedKeys];
        }
    }
    
    
    unsigned int count = 0;
    NSMutableDictionary *dictionary = [NSMutableDictionary new];
    objc_property_t *properties = class_copyPropertyList([self class], &count);
    
    for (int i = 0; i < count; i++) {
        
        NSString *key = [NSString stringWithUTF8String:property_getName(properties[i])];
        
        NSLog(@"%@", key);
        
        if ([self respondsToSelector:NSSelectorFromString(key)]) {
            
            id value = [self valueForKey:key];
            
            if (value == nil) {
                // nothing todo
            }
            else if ([value isKindOfClass:[NSNumber class]]
                     || [value isKindOfClass:[NSString class]]
                     || [value isKindOfClass:[NSDictionary class]] || [value isKindOfClass:[NSMutableDictionary class]] || [value isKindOfClass:[NSMutableArray class]] || [value isKindOfClass:[NSArray class]]) {
                // TODO: extend to other types
                [dictionary setObject:value forKey:key];
            }
            else if ([value isKindOfClass:[NSObject class]]) {
                [dictionary setObject:[value getDictionary] forKey:key];
            }
            else {
                NSLog(@"Invalid type for %@ (%@)", NSStringFromClass([self class]), key);
            }
        }
        
    }
    
    free(properties);
    return dictionary;
}

- (NSDictionary *)getDictionaryWithExludedKeys:(NSArray *)excludedKeys {
    
    unsigned int count = 0;
    NSMutableDictionary *dictionary = [NSMutableDictionary new];
    objc_property_t *properties = class_copyPropertyList([self class], &count);
    
    for (int i = 0; i < count; i++) {
        
        NSString *key = [NSString stringWithUTF8String:property_getName(properties[i])];
        
        if ([self respondsToSelector:NSSelectorFromString(key)] && ![excludedKeys containsObject:key] && ![key isEqualToString:KExcludedKeys]) {
            
            id value = [self valueForKey:key];
            
            if (value == nil) {
                // nothing todo
            }
            else if ([value isKindOfClass:[NSNumber class]]
                     || [value isKindOfClass:[NSString class]]
                     || [value isKindOfClass:[NSDictionary class]] || [value isKindOfClass:[NSMutableDictionary class]] || [value isKindOfClass:[NSMutableArray class]] || [value isKindOfClass:[NSArray class]]) {
                // TODO: extend to other types
                [dictionary setObject:value forKey:key];
            }
            else if ([value isKindOfClass:[NSObject class]]) {
                [dictionary setObject:[value getDictionary] forKey:key];
            }
            else {
                NSLog(@"Invalid type for %@ (%@)", NSStringFromClass([self class]), key);
            }
        }
        
    }
    
    free(properties);
    return dictionary;
}


- (void)setDictionary:(NSDictionary *)dictionary {
    
    // Check there is any excluded keys
    if([self respondsToSelector:NSSelectorFromString(KExcludedKeys)]) {
        NSArray *excludedKeys = [self valueForKey:KExcludedKeys];
        if ([excludedKeys count] > 0) {
            [self setDictionary:dictionary withExcludedKeys:excludedKeys];
            return;
        }
    }
    
    
    for (NSString *key in [dictionary allKeys]) {
        if ([self respondsToSelector:NSSelectorFromString(key)] && ![[dictionary objectForKey:key] isKindOfClass:[NSNull class]]) {
            [self setValue:[dictionary objectForKey:key] forKey:key];
        }
    }
}

- (void)setDictionary:(NSDictionary *)dictionary withExcludedKeys:(NSArray *)excludedKeys {
    
    for (NSString *key in [dictionary allKeys]) {
        if ([self respondsToSelector:NSSelectorFromString(key)] && ![excludedKeys containsObject:key] && ![key isEqualToString:KExcludedKeys] && ![[dictionary objectForKey:key] isKindOfClass:[NSNull class]]) {
            [self setValue:[dictionary objectForKey:key] forKey:key];
        }
    }
}


@end
