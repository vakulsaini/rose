//
//  UITextField+Custom.h
//  Fancast
//
//  Created by Vakul Saini on 06/06/18.
//  Copyright © 2018 enAct eServices. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextField (Custom)

- (void)setPlaceholderColor:(UIColor *)color;
- (void)setPlaceholderFont:(UIFont *)font;
- (void)setPlaceholder:(NSString *)text color:(UIColor *)color font:(UIFont *)font;

@end
