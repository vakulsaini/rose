//
//  CustomTitleSubLabel.m
//  Rose
//
//  Created by Virender on 02/01/20.
//  Copyright © 2020 QualiteSoft. All rights reserved.
//

#import "CustomTitleSubLabel.h"
#import "UIFont+Custom.h"

@implementation CustomTitleSubLabel

-(void)awakeFromNib{
  [super awakeFromNib];
     self.font = [UIFont fontOfSize:13.0f];
}

-(void)setIsBoldFont:(BOOL)isBoldFont {
   if (isBoldFont) {
       self.font = [UIFont fontSemiBoldOfSize:13.0f];
   }
   else{
       self.font = [UIFont fontOfSize:13.0f];
   }
}
 
@end
