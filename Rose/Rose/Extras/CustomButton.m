//
//  CustomButton.m
//  Rose
//
//  Created by Virender on 02/01/20.
//  Copyright © 2020 QualiteSoft. All rights reserved.
//

#import "CustomButton.h"
#import "UIFont+Custom.h"

@implementation CustomButton

-(void)awakeFromNib {
    [super awakeFromNib];
    [self.titleLabel setFont:[UIFont fontOfSize:15.0f]];
    self.backgroundColor = APP_PINK_COLOR;
}

@end
