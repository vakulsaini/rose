//
//  CustomHeadingLabel.m
//  Rose
//
//  Created by Virender on 02/01/20.
//  Copyright © 2020 QualiteSoft. All rights reserved.
//

#import "CustomHeadingLabel.h"
#import "UIFont+Custom.h"

@implementation CustomHeadingLabel

-(void)awakeFromNib{
    [super awakeFromNib];
    self.font = [UIFont fontOfSize:18.0f];
}
 
 -(void)setIsBoldFont:(BOOL)isBoldFont {
    if (isBoldFont) {
        self.font = [UIFont fontSemiBoldOfSize:18.0f];
    }
    else{
        self.font = [UIFont fontOfSize:18.0f];
    }
 }
  
 @end
 
