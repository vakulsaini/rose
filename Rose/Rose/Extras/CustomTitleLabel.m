//
//  CustomTitleLabel.m
//  Rose
//
//  Created by Virender on 02/01/20.
//  Copyright © 2020 QualiteSoft. All rights reserved.
//

#import "CustomTitleLabel.h"
#import "UIFont+Custom.h"

@implementation CustomTitleLabel

-(void)awakeFromNib{
   [super awakeFromNib];
    self.font = [UIFont fontOfSize:15.0f];
 }

//Setter method
-(void)setIsBoldFont:(BOOL)isBoldFont {
    if (isBoldFont) {
        self.font = [UIFont fontSemiBoldOfSize:15.0f];
    }
    else{
        self.font = [UIFont fontOfSize:15.0f];
    }
 }
 
@end
