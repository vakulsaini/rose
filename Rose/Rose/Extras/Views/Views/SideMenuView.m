//
//  SideMenuView.m
//  Seera
//
//  Created by Vakul Saini on 29/11/18.
//  Copyright © 2018 TheTiger. All rights reserved.
//

#import "SideMenuView.h"
#import "SideMenuCell.h"

@interface SideMenuView() <UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tblView;
@property (nonatomic, strong) UIView *viewBlackOverlay;
@property (nonatomic, getter=isOpen) BOOL open;
@end

@implementation SideMenuView

#pragma mark - LifeCycle
+ (instancetype)view {
    SideMenuView *instance = [SideMenuView loadFromNib];
    instance.tblView.delegate = instance;
    instance.tblView.dataSource = instance;
    instance.tblView.separatorStyle = UITableViewCellSeparatorStyleNone;
    instance.tblView.backgroundColor = [UIColor clearColor];
    [instance.tblView registerNib:[UINib nibWithNibName:@"SideMenuCell" bundle:nil] forCellReuseIdentifier:@"SideMenuCell"];
    
    instance.viewBlackOverlay = [[UIView alloc] init];
    instance.viewBlackOverlay.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:instance action:@selector(tapAction:)];
    [instance.viewBlackOverlay addGestureRecognizer:tap];
    
    return instance;
}

#pragma mark - Actions
- (void)tapAction:(UITapGestureRecognizer *)tapGesture {
    [self toggle];
}

#pragma mark - Methods
- (void)showInView:(UIView *)view {
    
    if (!view) {
        return;
    }
    
    CGRect rect = self.frame;
    rect.origin.x = view.width;
    rect.origin.y = 64.0;
    rect.size.height = view.height - 64.0;
    rect.size.width = view.width/1.7;
    self.frame = rect;
    
    self.viewBlackOverlay.alpha = 0.0;
    self.viewBlackOverlay.frame = view.bounds;
    [view addSubview:self.viewBlackOverlay];
    [UIView animateWithDuration:0.3 animations:^{
        self.viewBlackOverlay.alpha = 1.0;
    }];
    
    [view addSubview:self];
    rect.origin.x = view.width - rect.size.width;
    [UIView animateWithDuration:0.3 animations:^{
        self.frame = rect;
    }];
    self.open = YES;
}

- (void)show {
    
    if (!self.superview) {
        [self showInView:self.container];
        return;
    }
    
    UIView *view = self.superview;
    [UIView animateWithDuration:0.3 animations:^{
        self.viewBlackOverlay.alpha = 1.0;
    }];
    
    CGRect rect = self.frame;
    rect.origin.x = view.width - rect.size.width;
    [UIView animateWithDuration:0.3 animations:^{
        self.frame = rect;
    }];
    
    self.open = YES;
}

- (void)hide {
    if (!self.superview) {return;}
    
    UIView *view = self.superview;
    
    [UIView animateWithDuration:0.3 animations:^{
        self.viewBlackOverlay.alpha = 0.0;
    }];
    
    CGRect rect = self.frame;
    rect.origin.x = view.width;
    [UIView animateWithDuration:0.3 animations:^{
        self.frame = rect;
    }];
    
    self.open = NO;
}

- (void)toggle {
    if (self.isOpen) {
        [self hide];
    }
    else {
        [self show];
    }
}

#pragma mark - TableView Delegates & DataSources
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 12;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellId = @"SideMenuCell";
    SideMenuCell *cell = (SideMenuCell *)[tableView dequeueReusableCellWithIdentifier:cellId];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor clearColor];
    cell.iVIcon.clipsToBounds = YES;
    cell.iVIcon.layer.cornerRadius = 0.0;
    if (indexPath.row == 0) {
        cell.lblName.text = @"My Profile";
        cell.iVIcon.layer.cornerRadius = cell.iVIcon.frame.size.width/2.0;
        NSURL *imageUrl = [NSURL URLWithString:APP_DELEGATE.currentUser.profile_pic];
        [cell.iVIcon sd_setImageWithURL:imageUrl placeholderImage:[UIImage imageNamed:@"profile_white"]];
    }
    else if (indexPath.row == 1) {
        cell.lblName.text = @"Projects";
        cell.iVIcon.image = [UIImage imageNamed:@"projects"];
    }
    else if (indexPath.row == 2) {
        cell.lblName.text = @"Calendar";
        cell.iVIcon.image = [UIImage imageNamed:@"calendar"];
    }
    else if (indexPath.row == 3) {
        cell.lblName.text = @"Assignments";
        cell.iVIcon.image = [UIImage imageNamed:@"assignments"];
    }
    else if (indexPath.row == 4) {
        cell.lblName.text = @"News Feed";
        cell.iVIcon.image = [UIImage imageNamed:@"newsfeed"];
    }
    else if (indexPath.row == 5) {
        cell.lblName.text = @"Refer An Agent";
        cell.iVIcon.image = [UIImage imageNamed:@"refer"];
    }
    else if (indexPath.row == 6) {
        cell.lblName.text = @"Check In";
        cell.iVIcon.image = [[UIImage imageNamed:@"barcode"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    }
    else if (indexPath.row == 7) {
        cell.lblName.text = @"Rewards";
        cell.iVIcon.image = [[UIImage imageNamed:@"reward"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    }
    else if (indexPath.row == 8) {
        cell.lblName.text = @"Downloads";
        cell.iVIcon.image = [UIImage imageNamed:@"download"];
    }
    else if (indexPath.row == 9) {
        cell.lblName.text = @"Privacy";
        cell.iVIcon.image = [[UIImage imageNamed:@"privacy"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    }
    else if (indexPath.row == 10) {
        cell.lblName.text = @"About";
        cell.iVIcon.image = [[UIImage imageNamed:@"about"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    }
    else {
        cell.lblName.text = @"Logout";
        cell.iVIcon.image = [[UIImage imageNamed:@"logout"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 54.0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self toggle];
    if ([self.delegate respondsToSelector:@selector(sideMenu:clickedAtIndex:)]) {
        [self.delegate sideMenu:self clickedAtIndex:indexPath];
    }
}

@end
