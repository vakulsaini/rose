//
//  PriceSheetPopup.h
//  Seera
//
//  Created by Vakul Saini on 10/01/19.
//  Copyright © 2019 TheTiger. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PriceSheetPopup;
@class PriceSheetPopupDelegate;

@protocol PriceSheetPopupDelegate <NSObject>
- (void)priceSheetPopup:(PriceSheetPopup *)popup didSelectAtIndex:(NSInteger)index;
@end

@interface PriceSheetPopup : UIView

@property (nonatomic, weak) IBOutlet UIButton *btnView;
@property (nonatomic, weak) IBOutlet UIButton *btnDownload;
@property (nonatomic, weak) IBOutlet UIButton *btnInfo;

@property (nonatomic, assign) id <PriceSheetPopupDelegate> delegate;

+ (instancetype)view;

- (void)showInView:(UIView *)view atPosition:(CGPoint)position;
- (void)hide;

@end
