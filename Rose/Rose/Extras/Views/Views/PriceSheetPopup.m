//
//  PriceSheetPopup.m
//  Seera
//
//  Created by Vakul Saini on 10/01/19.
//  Copyright © 2019 TheTiger. All rights reserved.
//

#import "PriceSheetPopup.h"

@implementation PriceSheetPopup

#pragma mark - LifeCycle
+ (instancetype)view {
    PriceSheetPopup *instance = [PriceSheetPopup loadFromNib];
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:instance.bounds];
    instance.layer.masksToBounds = NO;
    instance.layer.shadowColor = [UIColor blackColor].CGColor;
    instance.layer.shadowOffset = CGSizeMake(0.0f, 5.0f);
    instance.layer.shadowOpacity = 0.5f;
    instance.layer.shadowPath = shadowPath.CGPath;
    return instance;
}

- (IBAction)viewAction:(id)sender {
    [self hide];
    if ([self.delegate respondsToSelector:@selector(priceSheetPopup:didSelectAtIndex:)]) {
        [self.delegate priceSheetPopup:self didSelectAtIndex:0];
    }
}

- (IBAction)downloadAction:(id)sender {
    [self hide];
    if ([self.delegate respondsToSelector:@selector(priceSheetPopup:didSelectAtIndex:)]) {
        [self.delegate priceSheetPopup:self didSelectAtIndex:1];
    }
}

- (IBAction)infoAction:(id)sender {
    [self hide];
    if ([self.delegate respondsToSelector:@selector(priceSheetPopup:didSelectAtIndex:)]) {
        [self.delegate priceSheetPopup:self didSelectAtIndex:2];
    }
}

#pragma mark - Methods
- (void)showInView:(UIView *)view atPosition:(CGPoint)position {
    CGRect frame = self.frame;
    frame.origin = position;
    self.frame = frame;
    self.alpha = 0.0;
    [view addSubview:self];
    [UIView animateWithDuration:0.4 animations:^{
        self.alpha = 1.0;
    }];
}

- (void)hide {
    [UIView animateWithDuration:0.4 animations:^{
        self.alpha = 0.0;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

@end
