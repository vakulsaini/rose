//
//  TermsView.m
//  Seera
//
//  Created by Vakul Saini on 17/01/19.
//  Copyright © 2019 TheTiger. All rights reserved.
//

#import "TermsView.h"

@interface TermsView () <WKNavigationDelegate>
@property (strong, nonatomic) UIView *overlayView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@end

@implementation TermsView

#pragma mark - LifeCycle
+ (instancetype)view {
    TermsView *instance = [TermsView loadFromNib];
    instance.overlayView = [[UIView alloc] init];
    instance.overlayView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
    instance.webView.backgroundColor = [UIColor whiteColor];
    instance.layer.cornerRadius = 10.0;
    instance.clipsToBounds = YES;
    return instance;
}

#pragma mark - Action
- (IBAction)cancelAction:(id)sender {
    [self hide];
    if (self.actionBlock) {
        self.actionBlock(TermsViewActionCancel);
    }
}

- (IBAction)iAgreeAction:(id)sender {
    if (self.actionBlock) {
        self.actionBlock(TermsViewActionIAgree);
    }
}

#pragma mark - Methods
- (void)showInView:(UIView *)view {
    
    self.overlayView.alpha = 0.0;
    self.alpha = 0.0;
    
    self.overlayView.frame = view.bounds;
    [view addSubview:self.overlayView];
    
    CGRect rect = self.frame;
    rect.origin.y = (view.frame.size.height - rect.size.height)/2.0;
    rect.origin.x = (view.frame.size.width - rect.size.width)/2.0;
    self.frame = rect;
    [view addSubview:self];
    
    [UIView animateWithDuration:0.3 animations:^{
        self.alpha = 1.0;
        self.overlayView.alpha = 1.0;
    } completion:^(BOOL finished) {
        self.webView.navigationDelegate = self;
        [self showLoader];
        
        if (self.url) {
            [self loadUrl:self.url];
        }
        else if (self.text) {
            [self loadText:self.text];
        }
    }];
}

- (void)hide {
    [UIView animateWithDuration:0.3 animations:^{
        self.alpha = 0.0;
        self.overlayView.alpha = 0.0;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
        [self.overlayView removeFromSuperview];
    }];
}

#pragma mark - Methods
- (void)loadUrl:(NSString *)str {
    NSURL *url = [NSURL URLWithString:str];
    NSURLRequest *request = [NSURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:30];
    [self.webView loadRequest:request];
}

- (void)loadText:(NSString *)text {
    NSString *str = [NSString stringWithFormat:@"<html><body><p style=\"padding: 20\"><font size=\"14\" face=\"Helvetica\">%@</font></p></body></html>", text];
    [self.webView loadHTMLString:str baseURL:nil];
}

- (void)showLoader {
    dispatch_async(dispatch_get_main_queue(), ^{
        self.webView.hidden = YES;
        self.activityIndicator.hidden = NO;
        [self.activityIndicator startAnimating];
    });
}

- (void)hideLoader {
    dispatch_async(dispatch_get_main_queue(), ^{
        self.webView.hidden = NO;
        self.activityIndicator.hidden = YES;
        [self.activityIndicator stopAnimating];
    });
}

#pragma mark - WebKit Delegates
- (void)webView:(WKWebView *)webView didFailNavigation:(WKNavigation *)navigation withError:(NSError *)error {
    [self hideLoader];
}

- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
    [self hideLoader];
}

@end
