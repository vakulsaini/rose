//
//  TermsView.h
//  Seera
//
//  Created by Vakul Saini on 17/01/19.
//  Copyright © 2019 TheTiger. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>

typedef enum : NSUInteger {
    TermsViewActionCancel,
    TermsViewActionIAgree,
} TermsViewAction;

typedef void(^TermsViewActionBlock)(TermsViewAction action);

@interface TermsView : UIView

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet WKWebView *webView;
@property (weak, nonatomic) IBOutlet UIButton *btnCancel;
@property (weak, nonatomic) IBOutlet UIButton *btnIAgree;

@property (nonatomic, strong) NSString *text;
@property (nonatomic, strong) NSString *url;

@property (nonatomic) TermsViewActionBlock actionBlock;

+ (instancetype)view;
- (void)showInView:(UIView *)view;
- (void)hide;

@end
