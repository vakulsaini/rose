//
//  LoadingView.h
//  Virtual Staging
//
//  Created by Vakul on 02/03/17.
//  Copyright © 2017 TheTiger. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoadingView : UIView

+ (instancetype)sharedInstance;
+ (instancetype)view;
- (void)show;
- (void)showInView:(UIView *)view;
- (void)hide;

@end
