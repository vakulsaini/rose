//
//  PreviewView.h
//  Seera
//
//  Created by Vakul Saini on 13/02/19.
//  Copyright © 2019 TheTiger. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>

@interface PreviewView : UIView

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet WKWebView *webView;

@property (nonatomic, strong) NSString *text;
@property (nonatomic, strong) NSString *url;

+ (instancetype)view;
- (void)showInView:(UIView *)view;
- (void)hide;

@end
