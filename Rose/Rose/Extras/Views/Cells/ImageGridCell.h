//
//  ImageGridCell.h
//  Seera
//
//  Created by Vakul Saini on 07/01/19.
//  Copyright © 2019 TheTiger. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ImageGridCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *iVImage;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIView *viewSelecionOverlay;
@property (weak, nonatomic) IBOutlet UIView *viewProgressContainer;
@property (weak, nonatomic) IBOutlet UIProgressView *progressView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *acivity;

- (void)makeSelection:(BOOL)selected;

@end

NS_ASSUME_NONNULL_END
