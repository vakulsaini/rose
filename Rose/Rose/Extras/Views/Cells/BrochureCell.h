//
//  BrochureCell.h
//  Seera
//
//  Created by Vakul Saini on 21/12/18.
//  Copyright © 2018 TheTiger. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BrochureCell : UICollectionViewCell

@property (nonatomic, weak) IBOutlet UIImageView *iVIcon;
@property (nonatomic, weak) IBOutlet UILabel *lbl;
@property (nonatomic, weak) IBOutlet UIImageView *iVAdd;

@end
