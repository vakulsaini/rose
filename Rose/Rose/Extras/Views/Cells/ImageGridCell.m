//
//  ImageGridCell.m
//  Seera
//
//  Created by Vakul Saini on 07/01/19.
//  Copyright © 2019 TheTiger. All rights reserved.
//

#import "ImageGridCell.h"

@implementation ImageGridCell

- (void)makeSelection:(BOOL)selected {
    if (selected) {
        self.viewSelecionOverlay.hidden = NO;
    }
    else {
        self.viewSelecionOverlay.hidden = YES;
    }
}

@end
