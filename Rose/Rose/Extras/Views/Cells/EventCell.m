//
//  EventCell.m
//  Seera
//
//  Created by Vakul Saini on 18/11/18.
//  Copyright © 2018 TheTiger. All rights reserved.
//

#import "EventCell.h"

@implementation EventCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setEvent:(Event *)event {
    _event = event;
    self.lblTitle.text = event.event_name;
    self.lblDesc.text = event.event_description;
    self.lblDate.text = event.event_date;
    [self.iVIcon sd_setImageWithURL:[NSURL URLWithString:event.event_pic] placeholderImage:kEventPlaceholder];
}

@end
