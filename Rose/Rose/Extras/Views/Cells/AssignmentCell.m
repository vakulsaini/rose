//
//  AssignmentCell.m
//  Seera
//
//  Created by Vakul Saini on 20/12/18.
//  Copyright © 2018 TheTiger. All rights reserved.
//

#import "AssignmentCell.h"

@implementation AssignmentCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setAssignment:(Assignment *)assignment {
    _assignment = assignment;
    
    /*self.lblName.text = [NSString stringWithFormat:@"%@ %@", assignment.agent_first_name, assignment.agent_last_name];
    self.lblEmail.text = assignment.agent_email.lowercaseString;
    self.lblContact.text = [NSString stringWithFormat:@"%@", assignment.agent_contact_no];
    
    NSURL *url = [NSURL URLWithString:assignment.picture1 ? assignment.picture1 : @""];
    [self.iVIcon sd_setImageWithURL:url placeholderImage:kImagePlaceholder];*/
    
    self.lblName.text = assignment.project_name;
    self.lblEmail.text = [NSString stringWithFormat:@"Status: %@", [assignment status]];
    self.lblContact.text = assignment.project_description;
    
    NSURL *url = [NSURL URLWithString:assignment.project_pic ? assignment.project_pic : @""];
    [self.iVIcon sd_setImageWithURL:url placeholderImage:kImagePlaceholder];
}

@end
