//
//  ProjectCell.m
//  Seera
//
//  Created by Vakul Saini on 17/11/18.
//  Copyright © 2018 TheTiger. All rights reserved.
//

#import "ProjectCell.h"

@implementation ProjectCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

- (void)setProject:(Project *)project {
    _project = project;
    self.lblTitle.text = project.project_name;
    
    NSString *text = [@"Status: " stringByAppendingString:project.status];
    NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:text];
    [attributedText addAttributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:self.lblStatus.font.pointSize]} range:[text rangeOfString:@"Status:"]];
    
    self.lblStatus.attributedText = attributedText;
    self.lblDesc.text = project.project_description;
}

@end
