//
//  PictureCell.h
//  Seera
//
//  Created by Vakul Saini on 21/12/18.
//  Copyright © 2018 TheTiger. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PictureCell : UICollectionViewCell

@property (nonatomic, weak) IBOutlet UIImageView *iVIcon;
@property (nonatomic, weak) IBOutlet UIImageView *iVAdd;

@end
