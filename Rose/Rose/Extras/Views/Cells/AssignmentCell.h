//
//  AssignmentCell.h
//  Seera
//
//  Created by Vakul Saini on 20/12/18.
//  Copyright © 2018 TheTiger. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AssignmentCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *iVIcon;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblEmail;
@property (weak, nonatomic) IBOutlet UILabel *lblContact;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;

@property (nonatomic, strong) Assignment *assignment;

@end
