//
//  DashboardCell.h
//  Seera
//
//  Created by Vakul Saini on 17/11/18.
//  Copyright © 2018 TheTiger. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DashboardCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *iVIcon;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@property (weak, nonatomic) IBOutlet UIView *separatorBottom;
@property (weak, nonatomic) IBOutlet UIView *separatorRight;

@property (nonatomic, strong) DashboardItem *item;

@end
