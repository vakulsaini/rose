//
//  HistoryCell.m
//  Seera
//
//  Created by Vakul Saini on 10/01/19.
//  Copyright © 2019 TheTiger. All rights reserved.
//

#import "HistoryCell.h"

@implementation HistoryCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setReward:(Reward *)reward {
    _reward = reward;
    self.lblTitle.text = [NSString stringWithFormat:@"You checked in to %@", reward.event_name];
    self.lblPoint.text = [NSString stringWithFormat:@"+%@", reward.point];
}

@end
