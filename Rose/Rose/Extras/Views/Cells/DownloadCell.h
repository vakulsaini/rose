//
//  DownloadCell.h
//  Seera
//
//  Created by Vakul Saini on 22/01/19.
//  Copyright © 2019 TheTiger. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DownloadCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIImageView *iVIcon;
@property (nonatomic, weak) IBOutlet UILabel *lblFileName;
@property (nonatomic, weak) IBOutlet UILabel *lblDate;
@property (nonatomic, weak) IBOutlet UILabel *lblSize;
@property (nonatomic, weak) IBOutlet UIButton *btnDelete;

@end
