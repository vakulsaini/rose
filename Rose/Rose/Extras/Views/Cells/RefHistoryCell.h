//
//  RefHistoryCell.h
//  Seera
//
//  Created by Vakul Saini on 29/01/19.
//  Copyright © 2019 TheTiger. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RefHistory.h"

@interface RefHistoryCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblEmail;
@property (weak, nonatomic) IBOutlet UILabel *lblContactNo;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;

@property (nonatomic, strong) RefHistory *history;

@end
