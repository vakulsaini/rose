//
//  SideMenuCell.h
//  Seera
//
//  Created by Vakul Saini on 29/11/18.
//  Copyright © 2018 TheTiger. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SideMenuCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *lblName;
@property (strong, nonatomic) IBOutlet UIImageView *iVIcon;

@end
