//
//  EventCell.h
//  Seera
//
//  Created by Vakul Saini on 18/11/18.
//  Copyright © 2018 TheTiger. All rights reserved.
//

#import <UIKit/UIKit.h>

#define kEventPlaceholder [UIImage imageNamed:@"hotel"]

@interface EventCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *lblTitle;
@property (nonatomic, weak) IBOutlet UIImageView *iVIcon;
@property (nonatomic, weak) IBOutlet UILabel *lblDesc;
@property (nonatomic, weak) IBOutlet UILabel *lblDate;

@property (nonatomic, strong) Event *event;

@end
