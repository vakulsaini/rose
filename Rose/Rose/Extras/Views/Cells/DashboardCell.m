//
//  DashboardCell.m
//  Seera
//
//  Created by Vakul Saini on 17/11/18.
//  Copyright © 2018 TheTiger. All rights reserved.
//

#import "DashboardCell.h"

@implementation DashboardCell

- (void)setItem:(DashboardItem *)item {
    _item = item;
    self.lblTitle.text = item.title;
    self.iVIcon.image = [UIImage imageNamed:item.imageUrl];
}

@end
