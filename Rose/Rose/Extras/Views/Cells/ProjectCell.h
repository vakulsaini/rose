//
//  ProjectCell.h
//  Seera
//
//  Created by Vakul Saini on 17/11/18.
//  Copyright © 2018 TheTiger. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProjectCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *iVIcon;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblStatus;
@property (weak, nonatomic) IBOutlet UILabel *lblDesc;
@property (weak, nonatomic) IBOutlet UILabel *lblAccessStatus;

@property (nonatomic, strong) Project *project;

@end
