//
//  DealCell.m
//  Seera
//
//  Created by Vakul Saini on 09/12/18.
//  Copyright © 2018 TheTiger. All rights reserved.
//

#import "DealCell.h"

@implementation DealCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setDeal:(Deal *)deal {
    _deal = deal;
    self.lblTitle.text = deal.title;
    self.lblDesc.text = deal.desc;
    //  self.iVIcon.image = [UIImage imageNamed:@""];
}


@end
