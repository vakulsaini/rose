//
//  DealCell.h
//  Seera
//
//  Created by Vakul Saini on 09/12/18.
//  Copyright © 2018 TheTiger. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DealCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *lblTitle;
@property (nonatomic, weak) IBOutlet UIImageView *iVIcon;
@property (nonatomic, weak) IBOutlet UILabel *lblDesc;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *layoutConstraintImageViewHeight;

@property (nonatomic, strong) Deal *deal;

@end
