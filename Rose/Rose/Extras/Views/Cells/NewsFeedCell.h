//
//  NewsFeedCell.h
//  Seera
//
//  Created by Vakul Saini on 18/11/18.
//  Copyright © 2018 TheTiger. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsFeedCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblSubTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;

@property (nonatomic, strong) NewsFeed *newsFeed;

@end
