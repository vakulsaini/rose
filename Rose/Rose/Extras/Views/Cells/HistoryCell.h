//
//  HistoryCell.h
//  Seera
//
//  Created by Vakul Saini on 10/01/19.
//  Copyright © 2019 TheTiger. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reward.h"

@interface HistoryCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblDay;
@property (weak, nonatomic) IBOutlet UILabel *lblMonth;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblPoint;

@property (nonatomic, strong) Reward *reward;

@end
