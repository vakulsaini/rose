//
//  NotificationCell.m
//  Seera
//
//  Created by Vakul Saini on 06/12/18.
//  Copyright © 2018 TheTiger. All rights reserved.
//

#import "NotificationCell.h"

@implementation NotificationCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

- (void)setNotification:(Notification *)notification {
    _notification = notification;
    self.lblTitle.text = notification.title;
    
    NSString *dateStr = [[Helper shared] convertDateString:notification.created_at fromFormat:kDateFormat_yyyy_MM_dd_HH_mm_ss toFormat:@"MMM dd"];
    NSString *timeStr = [[Helper shared] convertDateString:notification.created_at fromFormat:kDateFormat_yyyy_MM_dd_HH_mm_ss toFormat:@"h:mm a"].lowercaseString;
    NSString *fullStr = [NSString stringWithFormat:@"%@ at %@", dateStr, timeStr];

    self.lblDate.text = fullStr;
    self.lblDesc.text = notification.desc;
  //  self.iVIcon.image = [UIImage imageNamed:@""];
    
    self.bgView.hidden = ([notification.status integerValue] == 0);
}

@end
