//
//  NewsFeedCell.m
//  Seera
//
//  Created by Vakul Saini on 18/11/18.
//  Copyright © 2018 TheTiger. All rights reserved.
//

#import "NewsFeedCell.h"

@implementation NewsFeedCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    // self.lblTitle.superview.layer.borderWidth = 1.0;
    // self.lblTitle.superview.layer.borderColor = [UIColor darkGrayColor].CGColor;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

- (void)setNewsFeed:(NewsFeed *)newsFeed {
    _newsFeed = newsFeed;
    self.lblTitle.text = newsFeed.newsfeed_name;
    self.lblSubTitle.text = newsFeed.newsfeed_description;
    self.lblDate.text = newsFeed.newsfeed_date;
}


@end
