//
//  RefHistoryCell.m
//  Seera
//
//  Created by Vakul Saini on 29/01/19.
//  Copyright © 2019 TheTiger. All rights reserved.
//

#import "RefHistoryCell.h"

@implementation RefHistoryCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}


- (void)setHistory:(RefHistory *)history {
    _history = history;
    self.lblTitle.text = history.agent_name;
    self.lblEmail.text = history.agent_email;
    self.lblContactNo.text = history.contact_no;
    self.lblDate.text = history.date_time;
}

@end
