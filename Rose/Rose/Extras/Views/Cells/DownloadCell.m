//
//  DownloadCell.m
//  Seera
//
//  Created by Vakul Saini on 22/01/19.
//  Copyright © 2019 TheTiger. All rights reserved.
//

#import "DownloadCell.h"

@interface DownloadCell()

@end

@implementation DownloadCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
