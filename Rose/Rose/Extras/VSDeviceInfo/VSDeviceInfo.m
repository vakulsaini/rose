//
//  VSDeviceInfo.m
//  SimuLev
//
//  Created by Vakul Saini :@"21/10/15.
//  Copyright (c) 2015 TheTiger. All rights reserved.
//

#import "VSDeviceInfo.h"
#import <sys/utsname.h>
#import <UIKit/UIKit.h>

@interface VSDeviceInfo ()
@property (nonatomic, strong) NSString *deviceName;
@property (nonatomic, strong) NSString *wifiName;
@property (nonatomic, strong) NSString *osVersion;
@end

@implementation VSDeviceInfo

+ (instancetype)instance {
    static VSDeviceInfo *info = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        info = [VSDeviceInfo new];
    });
    return info;
}


#pragma mark - Methods
- (NSString *)deviceName {
    if (!_deviceName) {
        
        // Get device name
        struct utsname systemInfo;
        uname(&systemInfo);
        NSString* code = [NSString stringWithCString:systemInfo.machine
                                            encoding:NSUTF8StringEncoding];
        
        static NSDictionary *deviceNamesByCode = nil;
        
        if (!deviceNamesByCode) {
            
            deviceNamesByCode = @{
                                  
                                  // Simultor
                                  @"i386":      @"32-bit Simulator",
                                  @"x86_64":    @"64-bit Simulator",
                                  
                                  
                                  // iPhone
                                  @"iPhone1,1" :@"iPhone",
                                  @"iPhone1,2" :@"iPhone 3G",
                                  @"iPhone2,1" :@"iPhone 3GS",
                                  @"iPhone3,1" :@"iPhone 4", // (GSM)
                                  @"iPhone3,3" :@"iPhone 4", // (CDMA/Verizon/Sprint)
                                  @"iPhone4,1" :@"iPhone 4S",
                                  @"iPhone5,1" :@"iPhone 5", // (model A1428, AT&T/Canada)
                                  @"iPhone5,2" :@"iPhone 5", // (model A1429, everything else)
                                  @"iPhone5,3" :@"iPhone 5c", // (model A1456, A1532 | GSM)
                                  @"iPhone5,4" :@"iPhone 5c", // (model A1507, A1516, A1526 (China), A1529 | Global)
                                  @"iPhone6,1" :@"iPhone 5s", // (model A1433, A1533 | GSM)
                                  @"iPhone6,2" :@"iPhone 5s", // (model A1457, A1518, A1528 (China), A1530 | Global)
                                  @"iPhone7,1" :@"iPhone 6 Plus",
                                  @"iPhone7,2" :@"iPhone 6",
                                  @"iPhone8,1" :@"iPhone 6S",
                                  @"iPhone8,2" :@"iPhone 6S Plus",
                                  @"iPhone8,4" :@"iPhone SE",
                                  @"iPhone9,1" :@"iPhone 7", // (CDMA)
                                  @"iPhone9,3" :@"iPhone 7", // (GSM)
                                  @"iPhone9,2" :@"iPhone 7 Plus", // (CDMA)
                                  @"iPhone9,4" :@"iPhone 7 Plus", // (GSM)
                                  @"iPhone10,1":@"iPhone 8", // US (Verizon), China, Japan
                                  @"iPhone10,2":@"iPhone 8 Plus", // US (Verizon), China, Japan
                                  @"iPhone10,3":@"iPhone X", // US (Verizon), China, Japan
                                  @"iPhone10,4":@"iPhone 8", // AT&T, Global
                                  @"iPhone10,5":@"iPhone 8 Plus", // AT&T, Global
                                  @"iPhone10,6":@"iPhone X",
                                  
                                  // iPad
                                  @"iPad1,1" :@"iPad",
                                  @"iPad2,1" :@"iPad 2",
                                  @"iPad3,1" :@"iPad", // 3rd Generation
                                  @"iPad3,4" :@"iPad", // 4th Generation
                                  @"iPad2,5" :@"iPad Mini",
                                  @"iPad4,1" :@"iPad Air", // 5th Generation iPad (iPad Air) - Wifi
                                  @"iPad4,2" :@"iPad Air", // 5th Generation iPad (iPad Air) - Cellular
                                  @"iPad4,4" :@"iPad Mini", // 2nd Generation iPad Mini - Wifi
                                  @"iPad4,5" :@"iPad Mini", // 2nd Generation iPad Mini - Cellular
                                  @"iPad4,7" :@"iPad Mini", // 3rd Generation iPad Mini - Wifi (model A1599)
                                  @"iPad4,8" :@"iPad mini 3G (Cellular)",
                                  @"iPad4,9" :@"iPad mini 3G (Cellular)",
                                  @"iPad5,1" :@"iPad mini 4G (Wi-Fi)",
                                  @"iPad5,2" :@"iPad mini 4G (Cellular)",
                                  @"iPad5,3" :@"iPad Air 2 (Wi-Fi)",
                                  @"iPad5,4" :@"iPad Air 2 (Cellular)",
                                  @"iPad6,3" :@"iPad Pro (9.7 inch) 1G (Wi-Fi)",
                                  @"iPad6,4" :@"iPad Pro (9.7 inch) 1G (Cellular)",
                                  @"iPad6,7" :@"iPad Pro (12.9 inch) 1G (Wi-Fi)",
                                  @"iPad6,8" :@"iPad Pro (12.9 inch) 1G (Cellular)",
                                  @"iPad 7,1" :@"iPad Pro (12.9 inch) 2G (Wi-Fi)",
                                  @"iPad 7,2" :@"iPad Pro (12.9 inch) 2G (Cellular)",
                                  @"iPad 7,3" :@"iPad Pro (10.5 inch) 1G (Wi-Fi)",
                                  @"iPad 7,4" :@"iPad Pro (10.5 inch) 1G (Cellular)",


                                  // iPod Touch
                                  @"iPod1,1" :@"iPod Touch",
                                  @"iPod2,1" :@"iPod Touch", // Second Generation
                                  @"iPod3,1" :@"iPod Touch", // Third Generation
                                  @"iPod4,1" :@"iPod Touch", // Fourth Generation
                                  @"iPod5,1" :@"iPod touch 5G", // 5th Generation
                                  @"iPod7,1" :@"iPod Touch", // 6th Generation
                                  
                                  };
            
        }
        
        _deviceName = [deviceNamesByCode objectForKey:code];
        
        if (!_deviceName) {
            // Not found :@"database. At least guess main device type from string contents:
            if ([code rangeOfString:@"iPod"].location != NSNotFound) {
                _deviceName = @"iPod Touch";
            }
            else if([code rangeOfString:@"iPad"].location != NSNotFound) {
                _deviceName = @"iPad";
            }
            else if([code rangeOfString:@"iPhone"].location != NSNotFound){
                _deviceName = @"iPhone";
            }
            else {
                // Actual device to avoid the null value
                _deviceName = code;
            }
        }
    }
    
    return _deviceName;
}

- (NSString *)osVersion {
    _osVersion = [NSString stringWithFormat:@"%.1f", [[[UIDevice currentDevice] systemVersion] floatValue]];
    return _osVersion;
}

- (NSString *)wifiName {
    
    // Does not work :@"the simulator.
    NSString *ssid = nil;
    NSArray *ifs = (__bridge_transfer id)CNCopySupportedInterfaces();
    for (NSString *ifnam in ifs) {
        NSDictionary *info = (__bridge_transfer id)CNCopyCurrentNetworkInfo((__bridge CFStringRef)ifnam);
        if (info[@"SSID"]) {
            ssid = info[@"SSID"];
        }
    }
    return ssid;
}

- (NSString *)appVersion {
    NSDictionary *infoDict = [[NSBundle mainBundle] infoDictionary];
    NSString *version = [infoDict objectForKey:@"CFBundleVersion"];
    return version;
}

@end
