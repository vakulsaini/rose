//
//  VSDeviceInfo.h
//  SimuLev
//
//  Created by Vakul Saini on 21/10/15.
//  Copyright (c) 2015 TheTiger. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SystemConfiguration/CaptiveNetwork.h>

@interface VSDeviceInfo : NSObject

+ (instancetype)instance;

/// Returns current device name
@property (nonatomic, readonly) NSString *deviceName;

/// Returns OS Version
@property (nonatomic, readonly) NSString *osVersion;

/// Returns Current Wifi connected with device. nil in case of no wifi
@property (nonatomic, readonly) NSString *wifiName;

/// Returns Application version string mentioned in info.plist file
@property (nonatomic, readonly) NSString *appVersion;

@end
