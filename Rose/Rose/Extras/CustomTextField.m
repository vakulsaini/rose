//
//  CustomTextField.m
//  Rose
//
//  Created by Virender on 02/01/20.
//  Copyright © 2020 QualiteSoft. All rights reserved.
//

#import "CustomTextField.h"
#import "UIFont+Custom.h"

@implementation CustomTextField


-(void)awakeFromNib{
  [super awakeFromNib];
   self.font = [UIFont fontOfSize:15.0f];
}

@end
