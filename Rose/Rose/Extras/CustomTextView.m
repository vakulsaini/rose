//
//  CustomTextView.m
//  Rose
//
//  Created by Virender on 02/01/20.
//  Copyright © 2020 QualiteSoft. All rights reserved.
//

#import "CustomTextView.h"
#import "UIFont+Custom.h"

@implementation CustomTextView
 
-(void)awakeFromNib{
   [super awakeFromNib];
    self.font = [UIFont fontOfSize:15.0f];
 }

@end
