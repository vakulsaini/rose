//
//  Helper.m
//
//  Created by Vakul Saini on 14/12/18.
//  Copyright © 2018 TheTiger. All rights reserved.
//

#import "Helper.h"

@interface Helper ()
@property (nonatomic, strong) NSDateFormatter *dateFormatter;
@property (nonatomic, strong) NSString *appDirectory;
@end

@implementation Helper

+ (instancetype)shared {
    static Helper *helper = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        helper = [[Helper alloc] init];
        helper.dateFormatter = [[NSDateFormatter alloc] init];
        helper.appDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    });
    return helper;
}

- (NSString *)convertDateString:(NSString *)dateStr
                     fromFormat:(NSString *)fromFormat
                       toFormat:(NSString *)toFormat {
    self.dateFormatter.dateFormat = fromFormat;
    NSDate *date = [self.dateFormatter dateFromString:dateStr];
    self.dateFormatter.dateFormat = toFormat;
    return [self.dateFormatter stringFromDate:date];
}

@end
