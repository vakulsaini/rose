//
//  NavButtons.m
//  Rose
//
//  Created by Vakul Saini on 28/01/20.
//  Copyright © 2020 QualiteSoft. All rights reserved.
//

#import "NavButtons.h"

@implementation NavButtons

- (void)awakeFromNib {
    [super awakeFromNib];
    self.tintColor = APP_GRAY_COLOR;
}

@end
