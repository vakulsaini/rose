//
//  UIFont+Custom.h
//  Rose
//
//  Created by Vakul Saini on 13/02/20.
//  Copyright © 2020 QualiteSoft. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIFont (Custom)

+ (instancetype)fontOfSize:(CGFloat)size;
+ (instancetype)fontSemiBoldOfSize:(CGFloat)size;

@end

NS_ASSUME_NONNULL_END
