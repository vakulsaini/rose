//
//  UIFont+Custom.m
//  Rose
//
//  Created by Vakul Saini on 13/02/20.
//  Copyright © 2020 QualiteSoft. All rights reserved.
//

#import "UIFont+Custom.h"

@implementation UIFont (Custom)

+ (instancetype)fontOfSize:(CGFloat)size {
    return [UIFont fontWithName:@"Roboto-Regular" size:size + 1];
    // return [UIFont fontWithName:@"HelveticaNeue" size:size + 2];
    // return [UIFont fontWithName:@"Sailec-Light" size:size];
}

+ (instancetype)fontSemiBoldOfSize:(CGFloat)size {
    return [UIFont fontWithName:@"Roboto-Medium" size:size + 1];
    // return [UIFont fontWithName:@"HelveticaNeue-Medium" size:size];
    // return [UIFont fontWithName:@"Sailec-Medium" size:size];
}


@end
