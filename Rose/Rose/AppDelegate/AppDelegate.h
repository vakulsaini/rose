//
//  AppDelegate.h
//  Rose
//
//  Created by Vakul Saini on 23/12/19.
//  Copyright © 2019 QualiteSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RUser.h"
#import "Reachability.h"
#import <CoreLocation/CoreLocation.h>
#import <Sinch/Sinch.h>
#import "ConsultUser.h"
#import "CallViewController.h"

@class TabBarController;
@class RBaseViewController;
@class SINCallKitProvider;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

/**
 Application window.
 */
@property (strong, nonatomic) UIWindow *window;

/**
 Reachability's instance.
 */
@property (strong, nonatomic) Reachability *reachability;

/**
 Current location of device/user.
 */
@property (strong, nonatomic) CLLocation *currentLocation;

/**
 Current logged in user.
 */
@property (strong, nonatomic) RUser *currentUser;

/**
 Application tabbar controller.
 */
@property (strong, nonatomic) TabBarController *tabBarController;


/**
 Singleton instance of AppDelegate.
 
 @return returns AppDelegate's instance.
 */
+ (AppDelegate *)sharedInstance;

/**
 Will show Authenticate screen. Can be use after logout.
 
 @param animated is a BOOL value if true auth screen will be show with animation otherwise without any animation.
 */
- (void)showAuthScreen:(BOOL)animated;

/**
 Will show Authenticate screen.
 
 @param animated is a BOOL value if true main screen will be show with animation otherwise without any animation.
 */
- (void)showMainScreen:(BOOL)animated;


/**
 Device token.
 
 @return returns device token else nil.
 */
- (NSString *)getDeviceToken;

/**
 FCM token to send the push notification with the help of Firebase Cloud Messaging.
 
 @return returns FCM token else nil.
 */
- (NSString *)getFCMToken;


/**
 Will save user detail to user defaults.
 */
- (void)saveUserLocally;


/// Will clear the user detail from user defaults.
- (void)clearUserData;


- (void)goToSettingsFrom:(RBaseViewController *)controller;
- (void)openSearchFrom:(RBaseViewController *)controller;
- (void)goToNotificationsFrom:(RBaseViewController *)controller;

@property (nonatomic, strong) NSDateFormatter *dateFormatter;
@property (nonatomic, strong) id<SINClient> client;
@property (strong, nonatomic, readonly) SINCallKitProvider *callKitProvider;
@property (strong, nonatomic) NSMutableArray <ConsultUser *> *consultUsers;
@property (strong, nonatomic) NSMutableArray <RUser *> *contacts;
@property (strong, nonatomic) NSNumber *unreadCount;

- (void)uploadFCMTokenToServer;
- (void)deleteFCMTokenFromServer;
- (void)refreshUnreadNotifications:(void (^) (void))completion;

- (id<SINCall>)createCallInstanceForUserId:(NSString *)userId;
- (void)showCall:(id<SINCall>)call from:(UIViewController *)controller delegate:(id <CallViewControllerEventDelegate>)delegate;
- (void)loadConsultants:(void (^) (NSError *error))completion;
- (void)enabledOnTopAndDisabledOnBottom:(NSMutableArray *)array;
- (void)loadContacts:(void (^) (void))completion;
- (id<SINClient>)sinchClient;
- (id<SINCallClient>)callClient;
- (NSString *)status:(id <SINCall>)call;

- (void)dialCall:(NSString *)callId
        callerId:(NSString *)callerId
      receiverId:(NSString *)receiverId
      completion:(void (^) (void))completion;

- (void)updateCallStatus:(NSString *)callId
              callStatus:(NSString *)callStatus
            callDuration:(NSNumber *)callDuration
              completion:(void (^) (void))completion;

- (void)showOldAlert:(NSString *)title message:(NSString *)message;

@end

