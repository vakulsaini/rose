//
//  AppDelegate.m
//  Rose
//
//  Created by Vakul Saini on 23/12/19.
//  Copyright © 2019 QualiteSoft. All rights reserved.
//

#import "AppDelegate.h"
#import "LoginViewController.h"
#import "HomeViewController.h"
#import <UserNotifications/UserNotifications.h>
#import "TabBarController.h"
#import "RBaseViewController.h"
#import "SettingsViewController.h"
#import "NotificationsViewController.h"

#import "SINCallKitProvider.h"
#import "CallViewController.h"
#import "UIFont+Custom.h"

@import Firebase;

@interface AppDelegate () <UNUserNotificationCenterDelegate, SINClientDelegate, SINCallClientDelegate, SINManagedPushDelegate>

@property (strong, nonatomic) CLLocationManager *locationManager;

@property (nonatomic, readwrite, strong) id<SINManagedPush> push;
@property (nonatomic, readwrite, strong) SINCallKitProvider *callKitProvider;

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
   // NSLog(@"%@", [UIFont fontSemiBoldOfSize:15]);
   // NSLog(@"%@", [UIFont fontOfSize:15]);
    
    // Firebase
    [FIRApp configure];
    
    // Crashlytics
    [Fabric with:@[[Crashlytics class]]];
    
    /**
     Configure Sinch SDK
     */
    [self configureSinchSDK];
    
    /**
     Configuring Reachability
     */
    self.reachability = [Reachability reachabilityForInternetConnection];
    [self.reachability startNotifier];

    /// Register for remote notifications
    [self registerForPushNotifications];
    
    /// User's online/offline norifications
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userDidLogin) name:NOTIFICATION_USER_DID_LOGIN object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userDidLogout) name:NOTIFICATION_USER_DID_LOGOUT object:nil];
    
    
    /// Check if user is already logged in
    [self syncUser];
    if (self.currentUser) {
        [self showMainScreen:YES];
        // dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_USER_DID_LOGIN object:nil];
        //});
    }
    else {
        [self showAuthScreen:YES];
    }
    
    self.dateFormatter = [[NSDateFormatter alloc] init];
    
    return YES;
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
 
    id<SINCall> call = [_callKitProvider currentEstablishedCall];
    // If there is one established call, show the callView of the current call when
    // the App is brought to foreground. This is mainly to handle the UI transition
    // when clicking the App icon on the lockscreen CallKit UI.
    if (call) {
        
        UIViewController *top = self.window.rootViewController;
        while (top.presentedViewController) {
            top = top.presentedViewController;
        }
        
        // When entering the application via the App button on the CallKit lockscreen,
        // and unlocking the device by PIN code/Touch ID, applicationWillEnterForeground:
        // will be invoked twice, and "top" will be CallViewController already after
        // the first invocation.
        if (![top isMemberOfClass:[CallViewController class]]) {
             [self showCall:call from:top delegate:nil];
        }
    }
}

#pragma mark - Methods
+ (AppDelegate *)sharedInstance {
    return (AppDelegate *)[UIApplication sharedApplication].delegate;
}

- (void)showAuthScreen:(BOOL)animated {
    if (animated) {
        [self.window fadeTransition:0.2];
    }
    
    LoginViewController *authVC = [LoginViewController instance];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:authVC];
    self.window.rootViewController = nav;
}

- (void)showMainScreen:(BOOL)animated {
    if (animated) {
        [self.window fadeTransition:0.2];
    }
        
    TabBarController *tabVC = [TabBarController instance];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:tabVC];
    self.window.rootViewController = nav;
    
//    HomeViewController *tabVC = [HomeViewController instance];
//    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:tabVC];
//    self.window.rootViewController = nav;
}

- (void)saveUserLocally {
    NSDictionary *dict = [self.currentUser dictionary];
    if (dict) {
        [[NSUserDefaults standardUserDefaults] setObject:dict forKey:@"k_Current_User_Info"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    else {
        [self clearUserData];
    }
}

- (void)clearUserData {
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"k_Current_User_Info"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)syncUser {
    NSDictionary *dict = [[NSUserDefaults standardUserDefaults] objectForKey:@"k_Current_User_Info"];
    if (dict) {
        RUser *user = [[RUser alloc] init];
        [user setUpDictionary:dict];
        APP_DELEGATE.currentUser = user;
    }
}

- (id<SINCall>)createCallInstanceForUserId:(NSString *)userId {
    id<SINCall> call = [self.callClient callUserVideoWithId:userId];
    return call;
}

- (void)showCall:(id<SINCall>)call from:(UIViewController *)controller delegate:(id <CallViewControllerEventDelegate>)delegate {
   // dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        CallViewController *callViewController = [SINCH_STORYBOARD instantiateViewControllerWithIdentifier:@"CallViewController"];
        callViewController.call = call;
        callViewController.eventDelegate = delegate;
        callViewController.view.backgroundColor = [UIColor whiteColor];
        callViewController.modalPresentationStyle = UIModalPresentationFullScreen;
        [controller presentViewController:callViewController animated:YES completion:nil];
   // });
}

#pragma mark - Events
- (void)userDidLogin {
    /// Sync user detail
    [self.currentUser syncWithCompletionBlock:^{
        [APP_DELEGATE saveUserLocally];
    }];
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self initSinchClientWithUserId:[NSString stringWithFormat:@"%@", self.currentUser.Id]];
    });
    
    [self loadConsultants:^(NSError *error) { }];
    
    [self uploadFCMTokenToServer];
}

- (void)userDidLogout {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        // Vanish Sinch client
        id<SINClient> client = self.client;
        if ([client isStarted]) {
            [client unregisterPushNotificationDeviceToken];
            [client terminateGracefully];
        }
        
        self.client = nil;
    });
    
    [self deleteFCMTokenFromServer];
}

- (void)uploadFCMTokenToServer {
    NSNumber *userId = self.currentUser.Id;
    NSString *fcmToken = [self getFCMToken];
    if (userId && fcmToken) {
        NSDictionary *params = @{@"UserID": userId,
                                 @"DeviceID": fcmToken};
        
        NSLog(@"XYZZZZZZZZZZZZ\n%@", params);
        [[ApiHandler shared] sendHTTPRequest:APIRequestTypePOST url:WEB_SERVICE_FCM_TOKEN params:params withCompletion:^(id response, NSError *error) {

        }];
    }
}

- (void)deleteFCMTokenFromServer {
    NSNumber *userId = self.currentUser.Id;
    if (userId) {
        NSString *url = [NSString stringWithFormat:@"%@/%@", WEB_SERVICE_FCM_TOKEN, userId];
        [[ApiHandler shared] sendHTTPRequest:APIRequestTypeDELETE url:url params:nil withCompletion:^(id response, NSError *error) {

        }];
    }
}


- (void)loadConsultants:(void (^) (NSError *error))completion {
    
    /// Checking for internet connection
    if (!IsInternetAvailable) {
        return;
    }
    
    if (!self.consultUsers) {
        self.consultUsers = [[NSMutableArray alloc] init];
    }
    
    NSString *consultantsAPIUrl = [NSString stringWithFormat:@"%@/User_type/2/%@", WEB_SERVICE_NEW_SIGNUP, APP_DELEGATE.currentUser.Id];
    [[ApiHandler shared] sendHTTPRequest:APIRequestTypeGET url:consultantsAPIUrl params:nil withCompletion:^(id response, NSError *error) {
        HIDE_LOADER;
        if (error) {
            completion(error);
            return;
        }
        
        NSArray *users = response[@"User"];
        if (users && [users isKindOfClass:[NSArray class]]) {
            [self.consultUsers removeAllObjects];
            for (NSDictionary *u in users) {
                ConsultUser *user = [[ConsultUser alloc] init];
                [user setUpDictionary:u];
                [self.consultUsers addObject:user];
            }
        }
        
        [self enabledOnTopAndDisabledOnBottom:self.consultUsers];
        
        completion(nil);
    }];
}

- (void)enabledOnTopAndDisabledOnBottom:(NSMutableArray *)array {
    NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"SELF.Enable == %@", @"1"];
    NSArray *enabledConsultants = [array filteredArrayUsingPredicate:predicate1];
    
    NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"SELF.Enable == %@", @"0"];
    NSArray *disabledConsultants = [array filteredArrayUsingPredicate:predicate2];
    
    [array removeAllObjects];
    [array addObjectsFromArray:enabledConsultants];
    [array addObjectsFromArray:disabledConsultants];
}


- (void)loadContacts:(void (^) (void))completion {
    
    /// Checking for internet connection
    if (!IsInternetAvailable) {
        return;
    }
    
    if (!self.contacts) {
        self.contacts = [[NSMutableArray alloc] init];
    }
    
    NSString *consultantsAPIUrl = [NSString stringWithFormat:@"%@/%@", WEB_SERVICE_NEW_SIGNUP, APP_DELEGATE.currentUser.Id];
    [[ApiHandler shared] sendHTTPRequest:APIRequestTypeGET url:consultantsAPIUrl params:nil withCompletion:^(id response, NSError *error) {
        HIDE_LOADER;
        if (error) {
            completion();
            return;
        }
        
        NSArray *users = response[@"User"];
        if (users && [users isKindOfClass:[NSArray class]]) {
            [self.contacts removeAllObjects];
            for (NSDictionary *u in users) {
                RUser *user = [[RUser alloc] init];
                [user setUpDictionary:u];
                [self.contacts addObject:user];
            }
        }
            
        completion();
    }];
}

- (void)refreshUnreadNotifications:(void (^) (void))completion {
    NSString *countAPIUrl = [NSString stringWithFormat:@"%@/%@", WEB_SERVICE_UNREAD_NOTIFICATIONS_COUNT, APP_DELEGATE.currentUser.Id];
    [[ApiHandler shared] sendHTTPRequest:APIRequestTypeGET url:countAPIUrl params:nil withCompletion:^(id response, NSError *error) {
        if (error) {
            completion();
            return;
        }
        
        BOOL success = [response[@"status"] boolValue];
        if (success) {
            self.unreadCount = response[@"Counter"];
        }
        completion();
    }];
}


- (id<SINClient>)sinchClient {
    return [(AppDelegate *)[[UIApplication sharedApplication] delegate] client];
}

- (id<SINCallClient>)callClient {
  return [[self sinchClient] callClient];
}

- (NSString *)status:(id <SINCall>)call {
    
    if (call.details.endCause == SINCallEndCauseTimeout) {
        return @"TIMEOUT";
    }
    else if (call.details.endCause == SINCallEndCauseDenied) {
        return @"DENIED";
    }
    else if (call.details.endCause == SINCallEndCauseNoAnswer) {
        return @"NO_ANSWER";
    }
    else if (call.details.endCause == SINCallEndCauseError) {
        return @"FALIURE";
    }
    else if (call.details.endCause == SINCallEndCauseHungUp) {
        return @"HUNG_UP";
    }
    else if (call.details.endCause == SINCallEndCauseCanceled) {
        return @"CANCELED";
    }
    else if (call.details.endCause == SINCallEndCauseOtherDeviceAnswered) {
        return @"OTHER_DEVICE_ANSWERED";
    }
    else {
        return @"NONE";
    }
}


- (void)dialCall:(NSString *)callId
        callerId:(NSString *)callerId
      receiverId:(NSString *)receiverId
      completion:(void (^) (void))completion {
    
    NSDictionary *params = @{@"CallerID": callerId,
                             @"ReceiverID": receiverId,
                             @"CallStatus": @"Dial",
                             @"SinchID": callId};
    [[ApiHandler shared] sendHTTPRequest:APIRequestTypePOST url:WEB_SERVICE_CALL_LOG params:params withCompletion:^(id response, NSError *error) {

        if (error) {
            completion();
            return;
        }
        
        
        completion();
    }];
}

- (void)updateCallStatus:(NSString *)callId
              callStatus:(NSString *)callStatus
            callDuration:(NSNumber *)callDuration
              completion:(void (^) (void))completion {
    
    NSMutableDictionary *params = @{@"CallStatus": callStatus,
                                    @"_method": @"PUT",
                                    @"SinchID": callId}.mutableCopy;
    
    if (callDuration) {
        params[@"CallDuration"] = callDuration;
    } else {
        params[@"CallDuration"] = @0;
    }
    
    [[ApiHandler shared] sendHTTPRequest:APIRequestTypePOST url:WEB_SERVICE_CALL_LOG params:params withCompletion:^(id response, NSError *error) {
        
        if (error) {
            completion();
            return;
        }
        
        completion();
    }];
}

#pragma mark - Push Notification
/**
 Registering for push notification.
 */
- (void)registerForPushNotifications {
    
    UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
    center.delegate = self;
    [center requestAuthorizationWithOptions:UNAuthorizationOptionAlert | UNAuthorizationOptionBadge | UNAuthorizationOptionSound completionHandler:^(BOOL granted, NSError * _Nullable error) {
        if (granted) {
            DLog(@"Notification permission granted");
        }
        else {
            DLog(@"Notification permission not granted. Error: %@", error.localizedDescription);
        }
    }];
    
    [[UIApplication sharedApplication] registerForRemoteNotifications];
}


/**
 Will save the device token in NSUserDefaults by "DEVICE_TOKEN" key for future ussage.
 
 @param token is a device token in NSString format.
 */
- (void)saveDeviceToken:(NSString *)token {
    [[NSUserDefaults standardUserDefaults] setObject:token forKey:@"DEVICE_TOKEN"];
}

/**
 Function to get the saved device token in NSUserDefaults.
 
 @return returns a device token in NSString format.
 */
- (NSString *)getDeviceToken {
    NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:@"DEVICE_TOKEN"];
    return token;
}

/**
 Will save the device token in NSUserDefaults by "DEVICE_TOKEN" key for future ussage.
 
 @param token is a device token in NSString format.
 */
- (void)saveFCMToken:(NSString *)token {
    [[NSUserDefaults standardUserDefaults] setObject:token forKey:@"FCM_TOKEN"];
}

/**
 Function to get the saved device token in NSUserDefaults.
 
 @return returns a device token in NSString format.
 */
- (NSString *)getFCMToken {
    NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:@"FCM_TOKEN"];
    return token;
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    
   // [FIRMessaging messaging].APNSToken = deviceToken;
    const unsigned *deviceTokenBytes = [deviceToken bytes];
    
    NSString *tokenString = [NSString stringWithFormat:@"%08x%08x%08x%08x%08x%08x%08x%08x",
                             ntohl(deviceTokenBytes[0]), ntohl(deviceTokenBytes[1]), ntohl(deviceTokenBytes[2]),
                             ntohl(deviceTokenBytes[3]), ntohl(deviceTokenBytes[4]), ntohl(deviceTokenBytes[5]),
                             ntohl(deviceTokenBytes[6]), ntohl(deviceTokenBytes[7])];
    
    // Save device token to userDefaults
    [self saveDeviceToken:tokenString];
}

- (void)messaging:(FIRMessaging *)messaging didReceiveRegistrationToken:(NSString *)fcmToken {
    NSLog(@"FCM registration token: %@", fcmToken);
    // Notify about received token.
    //NSDictionary *dataDict = [NSDictionary dictionaryWithObject:fcmToken forKey:@"token"];
    //[[NSNotificationCenter defaultCenter] postNotificationName:@"FCMToken" object:nil userInfo:dataDict];
    
    // TODO: If necessary send token to application server.
    // Note: This callback is fired at each app startup and whenever a new token is generated.
    [self saveFCMToken:fcmToken];
    [self uploadFCMTokenToServer];
}


- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    
    //DLog(@"%@", userInfo);
    // NSDictionary *aps = userInfo[@"aps"];
    //DLog(@"%@", aps);
    
    if (application.applicationState == UIApplicationStateActive) {
        
        // Check id we have valid message text
        // NSString *alertMessage = [aps objectForKey:@"alert"];
        
        // Also check if we have center view controller
    }
    
    completionHandler(UIBackgroundFetchResultNewData);
}

// Receive displayed notifications for iOS 10 devices.
// Handle incoming notification messages while app is in the foreground.
- (void)userNotificationCenter:(UNUserNotificationCenter *)center
       willPresentNotification:(UNNotification *)notification
         withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler {
    NSDictionary *userInfo = notification.request.content.userInfo;
    
    // With swizzling disabled you must let Messaging know about the message, for Analytics
    // [[FIRMessaging messaging] appDidReceiveMessage:userInfo];

    // Print full message.
    NSLog(@"%@", userInfo);
    
    // Change this to your preferred presentation option
    completionHandler(UNNotificationPresentationOptionAlert);
}

// Handle notification messages after display notification is tapped by the user.
- (void)userNotificationCenter:(UNUserNotificationCenter *)center
didReceiveNotificationResponse:(UNNotificationResponse *)response
         withCompletionHandler:(void(^)(void))completionHandler {
    NSDictionary *userInfo = response.notification.request.content.userInfo;
    // Print full message.
    NSLog(@"%@", userInfo);
    
    completionHandler();
}

#pragma mark - Location Manager
- (void)startLocationManager {
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    [self.locationManager requestWhenInUseAuthorization]; // Add This Line
    self.locationManager.distanceFilter = kCLDistanceFilterNone; //whenever we move
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [self.locationManager startUpdatingLocation];
}

#pragma mark - CLLocationManagerDelegate
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations {
    self.currentLocation = locations.lastObject;
}

- (CLLocation *)currentLocation {
    if (!_currentLocation) {
        return DEFAULT_LOCATION;
    }
    return _currentLocation;
}


- (void)agreeToTermsAndConditions:(BOOL)agreed
                           byUser:(NSNumber *)userId
                   withCompletion:(void (^) (NSString *error, BOOL success))block {
    NSDictionary *params = @{@"user_id": userId, @"agree_status": @(agreed)};
//    [[ApiHandler shared] sendHTTPRequest:APIRequestTypePOST url:WEB_SERVICE_AGREE_TO_POLICY params:params withCompletion:^(id response, NSError *error) {
//        if (error) {
//            block(error.localizedDescription, NO);
//        }
//        else {
//            if ([response[@"result"] boolValue]) {
//                block(nil, YES);
//            }
//            else {
//                block(response[@"message"], YES);
//            }
//        }
//    }];
}

#pragma mark - Extra Methods
- (void)goToSettingsFrom:(RBaseViewController *)controller {
    SettingsViewController *settingsVC = [SettingsViewController instance];
    [controller.navigationController pushViewController:settingsVC animated:YES];
}

- (void)openSearchFrom:(RBaseViewController *)controller {
    
}

- (void)goToNotificationsFrom:(RBaseViewController *)controller {
    NotificationsViewController *notificationsVC = [NotificationsViewController instance];
    [controller.navigationController pushViewController:notificationsVC animated:YES];
}

#pragma mark - Sinch
- (void)configureSinchSDK {
    
    // Sinch SDK Configuration
    [Sinch setLogCallback:^(SINLogSeverity severity, NSString *area, NSString *message, NSDate *timestamp) {
        NSLog(@"[%@] %@", area, message);
    }];
    
    self.push = [Sinch managedPushWithAPSEnvironment:SINAPSEnvironmentProduction];
    NSLog(@"didFinishLaunchingWithOptions:");
    
//        PKPushRegistry *pushRegistry = [[PKPushRegistry alloc] initWithQueue:dispatch_get_main_queue()];
//        pushRegistry.delegate = self;
//        pushRegistry.desiredPushTypes = [NSSet setWithObject:PKPushTypeVoIP];
    
    self.push.delegate = self;
    [self.push setDesiredPushType:SINPushTypeVoIP];
    
    self.callKitProvider = [[SINCallKitProvider alloc] init];
}


- (void)initSinchClientWithUserId:(NSString *)userId {
    if (!_client) {
        
        _client = [Sinch clientWithApplicationKey:@"0fa0bb40-f925-411c-a0aa-79be9a591a52"
                                applicationSecret:@"7ARFeqHZuUmQO0bwnER3Xw=="
                                  environmentHost:@"clientapi.sinch.com"
                                           userId:userId];
        
        _client.delegate = self;
        _client.callClient.delegate = self;
        
        [_client setSupportCalling:YES];
        [_client enableManagedPushNotifications];
        
        _callKitProvider.client = _client;
        [_client start];
    }
}

- (void)handleRemoteNotification:(NSDictionary *)userInfo {
    if (!_client) {
        NSString *userId = [[NSUserDefaults standardUserDefaults] objectForKey:@"userId"];
        if (userId) {
            [self initSinchClientWithUserId:userId];
        }
    }
    
    [self.client relayRemotePushNotification:userInfo];
}

#pragma mark - SINManagedPushDelegate

- (void)managedPush:(id<SINManagedPush>)managedPush didReceiveIncomingPushWithPayload:(NSDictionary *)payload forType:(NSString *)pushType {
    NSLog(@"didReceiveIncomingPushWithPayload: %@", payload.description);
    
    // Since iOS 13 the application must report an incoming call to CallKit if a VoIP push notification was used, and
    // this must be done within the same run loop as the push is received (i.e. GCD async dispatch must not be used). See
    // https://developer.apple.com/documentation/pushkit/pkpushregistrydelegate/2875784-pushregistry for details.
    [self.callKitProvider didReceivePushWithPayload:payload];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self handleRemoteNotification:payload];
        [self.push didCompleteProcessingPushPayload:payload];
    });
}


//- (void)pushRegistry:(PKPushRegistry *)registry didUpdatePushCredentials:(PKPushCredentials *)credentials forType:(NSString *)type{
//    if([credentials.token length] == 0) {
//        NSLog(@"voip token NULL");
//        return;
//    }
//
//    NSLog(@"PushCredentials: %@", credentials.token);
//}
//
//- (void)pushRegistry:(PKPushRegistry *)registry didReceiveIncomingPushWithPayload:(PKPushPayload *)payload forType:(PKPushType)type withCompletionHandler:(void (^)(void))completion {
//    NSLog(@"didReceiveIncomingPushWithPayload");
//    [self.callKitProvider didReceivePushWithPayload:payload.dictionaryPayload];
//
//     dispatch_async(dispatch_get_main_queue(), ^{
//       [self handleRemoteNotification:payload.dictionaryPayload];
//       [self.push didCompleteProcessingPushPayload:payload.dictionaryPayload];
//     });
//    completion();
//}


#pragma mark - SINCallClientDelegate
- (void)client:(id<SINCallClient>)client didReceiveIncomingCall:(id<SINCall>)call {
    // Find MainViewController and present CallViewController from it.
    UIViewController *top = self.window.rootViewController;
    while (top.presentedViewController) {
        top = top.presentedViewController;
    }
    
    [self showCall:call from:top delegate:nil];
}

- (void)client:(id<SINClient>)client willReceiveIncomingCall:(id<SINCall>)call {
    [self.callKitProvider willReceiveIncomingCall:call];
}

- (void)presentMissedCallNotificationWithRemoteUserId:(NSString *)remoteUserId {
    UIApplication *application = [UIApplication sharedApplication];
    if ([application applicationState] == UIApplicationStateBackground) {
        UILocalNotification *note = [[UILocalNotification alloc] init];
        note.alertBody = [NSString stringWithFormat:@"Missed call from %@", remoteUserId];
        note.alertTitle = @"Missed call";
        [application presentLocalNotificationNow:note];
    }
}

#pragma mark - SINClientDelegate
- (void)clientDidStart:(id<SINClient>)client {
    NSLog(@"Sinch client started successfully (version: %@)", [Sinch version]);
}

- (void)clientDidFail:(id<SINClient>)client error:(NSError *)error {
    NSLog(@"Sinch client error: %@", [error localizedDescription]);
}

@end
