//
//  AddContactView.h
//  Rose
//
//  Created by Virender on 30/12/19.
//  Copyright © 2019 QualiteSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomHeadingLabel.h"
#import "CustomTextField.h"
#import "CustomButton.h"
#import "CustomTitleLabel.h"
#import "CustomTextView.h"
NS_ASSUME_NONNULL_BEGIN
@class AddContactView;

@protocol AddContactViewDelegate <NSObject>
- (void)contactViewDidClose:(AddContactView *)contactView;
- (void)contactViewDidSave:(AddContactView *)contactView; 
@end

@interface AddContactView : UIView 
@property (nonatomic, weak) IBOutlet CustomTextField *tfPatientName;
@property (nonatomic, weak) IBOutlet CustomTextField *tfPatientHealthNumber;
@property (nonatomic, weak) IBOutlet CustomTextField *tfPatientMSPNumber;
@property (nonatomic, weak) IBOutlet CustomTextField *tfUserPhoneNumber;
@property (nonatomic, weak) IBOutlet CustomTextField *tfPatientDOB;
@property (nonatomic, weak) IBOutlet CustomTextView *tVPatientMessage;
@property (nonatomic, weak) IBOutlet CustomButton *btnCreate;
@property (nonatomic, weak) IBOutlet UIButton *btnDOB;
@property (nonatomic, strong) IBOutlet UIDatePicker *datePicker;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *bottomConstraint;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *centerYConstraint;
@property (nonatomic, weak) IBOutlet CustomTitleLabel *lblNotePlaceholder;

@property (nonatomic, assign) id <AddContactViewDelegate> delegate;

- (void)showOnView:(UIView *)view animated:(BOOL)animated;
- (void)hide:(BOOL)animated;
+ (instancetype)view;
- (void)clearText;


@end

NS_ASSUME_NONNULL_END
