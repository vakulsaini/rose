//
//  CallInfoView.m
//  Rose
//
//  Created by Vakul Saini on 27/01/20.
//  Copyright © 2020 QualiteSoft. All rights reserved.
//

#import "CallInfoView.h"

@interface CallInfoView ()
@property (nonatomic, weak) IBOutlet UIView *viewMain;
@property (nonatomic, weak) IBOutlet CustomTitleLabel *lblNameTitle;
@property (nonatomic, weak) IBOutlet CustomTitleLabel *lblLocationTitle;
@property (nonatomic, weak) IBOutlet CustomTitleLabel *lblSpecialityTitle;
@property (nonatomic, weak) IBOutlet CustomTitleLabel *lblCallStatusTitle;
@end


@implementation CallInfoView

+ (instancetype)view {
    CallInfoView *v = [CallInfoView loadFromNib];
    v.iVProfile.layer.cornerRadius = v.iVProfile.bounds.size.width/2.0;
    [v.viewMain makeRoundWithRadius:6];
    [v.btnOk makeRoundWithRadius:2.0];
    
    v.lblName.textColor = APP_LIGHT_GRAY_COLOR;
    v.lblLocation.textColor = APP_LIGHT_GRAY_COLOR;
    v.lblSpeciality.textColor = APP_LIGHT_GRAY_COLOR;
    v.lblCallStatus.textColor = APP_LIGHT_GRAY_COLOR;
    
    v.lblNameTitle.textColor = APP_BLACK_COLOR;
    v.lblLocationTitle.textColor = APP_BLACK_COLOR;
    v.lblSpecialityTitle.textColor = APP_BLACK_COLOR;
    v.lblCallStatusTitle.textColor = APP_BLACK_COLOR;
    
    v.lblNameTitle.isBoldFont = YES;
    v.lblLocationTitle.isBoldFont = YES;
    v.lblSpecialityTitle.isBoldFont = YES;
    v.lblCallStatusTitle.isBoldFont = YES;
    
    
    return v;
}

- (void)showOnView:(UIView *)view animated:(BOOL)animated {
    self.alpha = 0.0;
    self.frame = view.bounds;
    [view addSubview:self];
    [self showWithAnimation:YES];
 }

- (void)hide:(BOOL)animated {
    [self hideWithAnimation:animated];
}

- (IBAction)okAction:(id)sender {
    [self hide:YES];
}

- (IBAction)closeAction:(id)sender {
    [self hideWithAnimation:YES];
}

- (void)setCallHistory:(CallHistory *)callHistory {
    _callHistory = callHistory;
    self.lblName.text = callHistory.name;
    self.lblLocation.text = callHistory.location;
    self.lblSpeciality.text = callHistory.speciality;
    self.lblCallStatus.text = callHistory.CallStatus;

    [self.iVProfile sd_setImageWithURL:[NSURL URLWithString:callHistory.profilePic] placeholderImage:kUserProfilePlaceholder];
}

@end
