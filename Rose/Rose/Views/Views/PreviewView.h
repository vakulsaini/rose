//
//  PreviewView.h
//
//  Copyright © 2019 TheTiger. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>

@class PreviewView;

typedef void(^PreviewViewPrintButtonBlock)(UIButton *sender, PreviewView *view);


@interface PreviewView : UIView

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet WKWebView *webView;
@property (weak, nonatomic) IBOutlet UIView *viewPrintOption;

@property (nonatomic, strong) NSString *text;
@property (nonatomic, strong) NSString *url;

@property (nonatomic, strong) _Nullable PreviewViewPrintButtonBlock printButtonblock;


+ (instancetype)view;
- (void)showInView:(UIView *)view;
- (void)hide;

@end
