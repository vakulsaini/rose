//
//  CallInfoView.h
//  Rose
//
//  Created by Vakul Saini on 27/01/20.
//  Copyright © 2020 QualiteSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomTitleLabel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CallInfoView : UIView

@property (nonatomic, weak) IBOutlet UIImageView *iVProfile;
@property (nonatomic, weak) IBOutlet CustomTitleLabel *lblName;
@property (nonatomic, weak) IBOutlet CustomTitleLabel *lblLocation;
@property (nonatomic, weak) IBOutlet CustomTitleLabel *lblSpeciality;
@property (nonatomic, weak) IBOutlet CustomTitleLabel *lblCallStatus;
@property (nonatomic, strong) CallHistory *callHistory;
@property (nonatomic, weak) IBOutlet UIButton *btnOk;

- (void)showOnView:(UIView *)view animated:(BOOL)animated;
- (void)hide:(BOOL)animated;
+ (instancetype)view;


@end

NS_ASSUME_NONNULL_END
