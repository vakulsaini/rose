//
//  LoadingView.m
//  Virtual Staging
//
//  Created by Vakul on 02/03/17.
//  Copyright © 2017 TheTiger. All rights reserved.
//

#import "LoadingView.h"

@interface LoadingView()
@property (nonatomic, weak) IBOutlet UIImageView *iVCenter;
@property (nonatomic, weak) IBOutlet UIImageView *iVCircle;
@property (nonatomic, weak) IBOutlet UIView *viewCircleBG;
@end

@implementation LoadingView

#pragma mark - LifeCycle
+ (instancetype)sharedInstance {
    static LoadingView *v = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        v = [LoadingView view];
        [v setUp];
    });
    return v;
}

+ (instancetype)view {
    return [LoadingView loadFromNib];
}

- (void)setUp {
    CGFloat strokeWidth = 3.0;
    CGFloat radius = self.viewCircleBG.frame.size.width/2.0;
    CGPoint center = CGPointMake(self.viewCircleBG.frame.size.width/2.0, self.viewCircleBG.frame.size.height/2.0);
    UIBezierPath *bezierPath = [UIBezierPath bezierPathWithArcCenter:center radius:radius startAngle:(M_PI * 3.0/2.0)  endAngle:(M_PI) clockwise:YES];
    
    CAShapeLayer *layer = [CAShapeLayer layer];
    layer.strokeColor = [UIColor whiteColor].CGColor;
    layer.lineWidth = strokeWidth;
    layer.lineCap = kCALineCapRound;
    layer.path = bezierPath.CGPath;
    layer.fillColor = [UIColor clearColor].CGColor;
    [self.viewCircleBG.layer addSublayer:layer];
}

- (void)show {
    [self showInView:APP_DELEGATE.window];
}

- (void)showInView:(UIView *)view {
    
    self.frame = APP_DELEGATE.window.bounds;
    self.alpha = 0.0;
    [view addSubview:self];
    [UIView animateWithDuration:0.3 animations:^{
        self.alpha = 1.0;
    }];
    
    // Start rotating
    [self startZRotationForDuration:1 repeatCount:CGFLOAT_MAX clockWise:YES];
}

- (void)hide {
    // Stop rotating
    [self stopZRotation];
    [UIView animateWithDuration:0.3 animations:^{
        self.alpha = 0.0;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}


#pragma mark - Methods
- (void)startZRotationForDuration:(CFTimeInterval)duration repeatCount:(CGFloat)repeatCount  clockWise:(BOOL)clockWise {
    
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    CGFloat direction = clockWise ? 1.0 : -1.0;
    animation.toValue = @(M_PI * 2 * direction);
    animation.duration = duration;
    animation.cumulative = YES;
    animation.repeatCount = repeatCount;
    [self.viewCircleBG.layer addAnimation:animation forKey:@"transform.rotation.z"];
}

- (void)stopZRotation {
    [self.viewCircleBG.layer removeAnimationForKey:@"transform.rotation.z"];
}

@end
