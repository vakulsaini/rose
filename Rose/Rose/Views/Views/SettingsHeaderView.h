//
//  SettingsHeaderView.h
//  Rose
//
//  Created by Vakul Saini on 07/01/20.
//  Copyright © 2020 QualiteSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomTitleLabel.h"

NS_ASSUME_NONNULL_BEGIN

@interface SettingsHeaderView : UIView

@property (nonatomic, weak) IBOutlet CustomTitleLabel *lblTitle;
@property (nonatomic, weak) IBOutlet UIButton *btn;

+ (instancetype)view;

@end

NS_ASSUME_NONNULL_END
