//
//  DatePickerView.h
//  Rose
//
//  Created by Vakul Saini on 06/01/20.
//  Copyright © 2020 QualiteSoft. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class DatePickerView;

@protocol DatePickerViewDelegate <NSObject>
@optional
- (void)datePickerDidPickDate:(DatePickerView *)view date:(NSDate *)date;
- (void)datePickerDidCancel:(DatePickerView *)view;

@end

@interface DatePickerView : UIView

@property (nonatomic, weak) IBOutlet UIButton *btnDone;
@property (nonatomic, weak) IBOutlet UIButton *btnCancel;
@property (nonatomic, weak) IBOutlet UIDatePicker *datePicker;
@property (nonatomic, assign) id <DatePickerViewDelegate> delegate;

- (void)showOnView:(UIView *)view animated:(BOOL)animated;
- (void)hide:(BOOL)animated;
+ (instancetype)view;

@end

NS_ASSUME_NONNULL_END
