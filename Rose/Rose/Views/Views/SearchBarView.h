//
//  SearchBarView.h
//  Rose
//
//  Created by Vakul Saini on 31/12/19.
//  Copyright © 2019 QualiteSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomTextField.h"

NS_ASSUME_NONNULL_BEGIN

@class SearchBarView;

@protocol SearchBarViewDelegate <NSObject>
@optional
- (void)searchBarDidEditingChange:(SearchBarView *)view;
- (void)searchBarDidClose:(SearchBarView *)view;

@end

@interface SearchBarView : UIView

@property (nonatomic, weak) IBOutlet CustomTextField *textField;
@property (nonatomic, weak) IBOutlet UIButton *btnCancel;
@property (nonatomic, assign) id <SearchBarViewDelegate> delegate;

- (void)showOnView:(UIView *)view animated:(BOOL)animated;
- (void)hide:(BOOL)animated;
+ (instancetype)view;
- (void)clearText;

@end

NS_ASSUME_NONNULL_END
