//
//  DropdownView.m
//
//  Copyright © 2018 TheTiger. All rights reserved.
//

#import "DropdownView.h"

@interface DropdownView() <UIPickerViewDelegate, UIPickerViewDataSource>
@property (strong, nonatomic) NSArray *data;
@property (strong, nonatomic) UIView *overlayView;
@end

@implementation DropdownView

#pragma mark - LifeCycle
+ (instancetype)view {
    DropdownView *instance = [DropdownView loadFromNib];
    instance.pickerView.delegate = instance;
    instance.pickerView.dataSource = instance;
    [instance loadDataWithCompletion:^{
        [instance reloadData];
    }];
    instance.overlayView = [[UIView alloc] init];
    instance.overlayView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.4];
    
    return instance;
}

- (IBAction)doneAction:(id)sender {
    [self hide];
}

#pragma mark - Methods
- (void)showInView:(UIView *)view {
    
    self.overlayView.alpha = 0.0;
    self.overlayView.frame = view.bounds;
    [view addSubview:self.overlayView];
    
    CGRect rect = self.frame;
    rect.origin.y = view.frame.size.height;
    rect.origin.x = 0.0;
    rect.size.width = view.frame.size.width;
    self.frame = rect;
    [view addSubview:self];
    
    rect.origin.y = view.frame.size.height - self.frame.size.height;
    [UIView animateWithDuration:0.3 animations:^{
        self.frame = rect;
        self.overlayView.alpha = 1.0;
    } completion:^(BOOL finished) {
        
        if ([self.delegate respondsToSelector:@selector(dropDown:didSelectItem:)]) {
            NSInteger row = [self.pickerView selectedRowInComponent:0];
            [self.delegate dropDown:self didSelectItem:self.data[row][@"name"]];
        }
    }];
    
    if ([self.data count] == 0) {
        [self loadDataWithCompletion:^{
            [self reloadData];
        }];
    }
}

- (void)hide {
    CGRect rect = self.frame;
    rect.origin.y += self.frame.size.height;
    [UIView animateWithDuration:0.3 animations:^{
        self.frame = rect;
        self.overlayView.alpha = 0.0;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
        [self.overlayView removeFromSuperview];
    }];
}

- (void)loadDataWithCompletion:(void (^) (void))completion {
    
//    [[ApiHandler shared] sendHTTPRequest:APIRequestTypeGET url:WEB_SERVICE_BROCKRAGE_LIST params:nil withCompletion:^(id response, NSError *error) {
//        
//        if (error) {
//            completion();
//            return;
//        }
//        
//        if ([response[@"result"] boolValue]) {
//            self.data = response[@"data"];
//        }
//        
//        completion();
//    }];
}

- (void)reloadData {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.pickerView reloadAllComponents];
    });
}

#pragma mark - PickerView
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return [self.data count];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return self.data[row][@"name"];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    if ([self.delegate respondsToSelector:@selector(dropDown:didSelectItem:)]) {
        [self.delegate dropDown:self didSelectItem:self.data[row][@"name"]];
    }
}

@end
