//
//  AddContactView.m
//  Rose
//
//  Created by Virender on 30/12/19.
//  Copyright © 2019 QualiteSoft. All rights reserved.
//

#import "AddContactView.h"
#import "CustomHeadingLabel.h"
#import "CustomTextField.h"
#import "CustomButton.h"
#import "UIFont+Custom.h"

@interface AddContactView () <UITextViewDelegate, UITextFieldDelegate>
@property (nonatomic, weak) IBOutlet UIView *viewMain;
@end


@implementation AddContactView

+ (instancetype)view {
    AddContactView *v = [AddContactView loadFromNib];
    v.tfPatientName.delegate = v;
    v.tfPatientMSPNumber.delegate = v;
    v.tfPatientHealthNumber.delegate = v;
    v.tfPatientDOB.delegate = v;
    v.tfUserPhoneNumber.delegate = v;
    v.tfUserPhoneNumber.delegate = v;
    v.tVPatientMessage.delegate = v;
    v.tVPatientMessage.font = [UIFont fontOfSize:15];
    [v.viewMain makeRoundWithRadius:6];
    return v;
}

- (void)observeKeyboardNotifications {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
}

- (void)removeKeyboardNotifications {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWillShow:(NSNotification *)notification {
    [self.btnDOB setSelected:NO];
    [self hideDatePicker:YES];
    self.centerYConstraint.constant = - 100;
    [UIView animateWithDuration:0.2 animations:^{
        [self layoutIfNeeded];
    }];
}

- (void)keyboardWillHide:(NSNotification *)notification {
    self.centerYConstraint.constant = 0;
    [UIView animateWithDuration:0.2 animations:^{
        [self layoutIfNeeded];
    }];
}

- (void)showOnView:(UIView *)view animated:(BOOL)animated {
    [self observeKeyboardNotifications];
    self.alpha = 0.0;
    self.frame = view.bounds;
    [view addSubview:self];
    [self showWithAnimation:YES];
 }

- (void)hide:(BOOL)animated {
    [self removeKeyboardNotifications];
    [self hideWithAnimation:animated];
}

#pragma mark - Action
- (IBAction)createAction:(id)sender {
    if ([self.delegate respondsToSelector:@selector(contactViewDidSave:)]) {
        [self.delegate contactViewDidSave:self];
    }
}

- (IBAction)dobAction:(id)sender {
    
    [self resignAll];
    
    if (![sender isSelected]) {
        [sender setSelected:YES];
        [self showDatePicker:YES];
    }
    else {
        [sender setSelected:NO];
        [self hideDatePicker:YES];
    }
 }

- (IBAction)cancelAction:(id)sender {
    [self.btnDOB setSelected:NO];
    [self hideDatePicker:YES];
}

- (IBAction)doneAction:(id)sender {
    [self.btnDOB setSelected:NO];
    [self hideDatePicker:YES];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd-MM-yyyy"];
    self.tfPatientDOB.text = [formatter stringFromDate:self.datePicker.date];
}


- (IBAction)closeAction:(id)sender {
    [self resignAll];
    [self hideWithAnimation:YES];
}

- (void)clearText {
    self.tfPatientName.text = @"";
    self.tfPatientHealthNumber.text = @"";
    self.tfPatientMSPNumber.text = @"";
    self.tVPatientMessage.text = @"";
    self.tfUserPhoneNumber.text = @"";
    [self.btnDOB setSelected:NO];
    [self hideDatePicker:YES];
}

- (void)resignAll {
    [self.tfPatientName resignFirstResponder];
    [self.tfPatientHealthNumber resignFirstResponder];
    [self.tfPatientMSPNumber resignFirstResponder];
    [self.tVPatientMessage resignFirstResponder];
    [self.tfUserPhoneNumber resignFirstResponder];
}

- (void)showDatePicker:(BOOL)animated {
    [self readonly:YES];
    self.bottomConstraint.constant = 20.0;
    if (animated) {
        [UIView animateWithDuration:0.3 animations:^{
             [self layoutIfNeeded];
        }];
    }
    else {
        [self layoutIfNeeded];
    }
}

- (void)hideDatePicker:(BOOL)animated {
    [self readonly:NO];
    self.bottomConstraint.constant = -560.0;
    if (animated) {
        [UIView animateWithDuration:0.3 animations:^{
             [self layoutIfNeeded];
        }];
    }
    else {
        [self layoutIfNeeded];
    }
}

- (void)readonly:(BOOL)disable {
    self.tfPatientName.userInteractionEnabled = !disable;
    self.tfPatientHealthNumber.userInteractionEnabled = !disable;
    self.tVPatientMessage.userInteractionEnabled = !disable;
    self.tfUserPhoneNumber.userInteractionEnabled = !disable;
    
    self.tfPatientMSPNumber.userInteractionEnabled = NO;
}

// MARK:- TextView Delegate
- (void)textViewDidBeginEditing:(UITextView *)textView{
    
}

- (void)textViewDidChange:(UITextView *)textView {
    if (![textView.text isEmpty]) {
        [self.lblNotePlaceholder setHidden:YES];
    }
    else {
        [self.lblNotePlaceholder setHidden:NO];
    }
}

// MARK:- TextField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if (textField == self.tfPatientMSPNumber) {
        return !([newString length] > 5);
    }
    else if (textField == self.tfPatientHealthNumber) {
        return !([newString length] > 10);
    }
    else if (textField == self.tfUserPhoneNumber) {
        return !([newString length] > 10);
    }
    else {
        return YES;
    }
}

@end
