//
//  SettingsHeaderView.m
//  Rose
//
//  Created by Vakul Saini on 07/01/20.
//  Copyright © 2020 QualiteSoft. All rights reserved.
//

#import "SettingsHeaderView.h"

@implementation SettingsHeaderView

+ (instancetype)view {
    SettingsHeaderView *v = [SettingsHeaderView loadFromNib];
    return v;
}

@end
