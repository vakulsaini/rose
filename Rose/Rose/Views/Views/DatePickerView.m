//
//  DatePickerView.m
//  Rose
//
//  Created by Vakul Saini on 06/01/20.
//  Copyright © 2020 QualiteSoft. All rights reserved.
//

#import "DatePickerView.h"

@interface DatePickerView ()
@property (nonatomic, strong) UIView *overlayView;
@end

@implementation DatePickerView

+ (instancetype)view {
    DatePickerView *v = [DatePickerView loadFromNib];
    [v setUpDefault];
    return v;
}

- (void)showOnView:(UIView *)view animated:(BOOL)animated {
    
    CGRect frame = self.frame;
    frame.origin.y = view.frame.size.height;
    frame.size.width = view.frame.size.width;
    self.frame = frame;
    
    if (!self.overlayView) {
        self.overlayView = [[UIView alloc] init];
        self.overlayView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.4];
    }
    self.overlayView.frame = view.bounds;
    self.overlayView.alpha = 0.0;
    
    [view addSubview: self.overlayView];
    [view addSubview:self];
    
    frame.origin.y -= frame.size.height;
    
    if (animated) {
        [UIView animateWithDuration:0.1 animations:^{
            self.overlayView.alpha = 1.0;
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:0.2 animations:^{
                self.frame = frame;
            }];
        }];
    } else {
        self.overlayView.alpha = 1.0;
        self.frame = frame;
    }
}

- (void)hide:(BOOL)animated {
    CGRect frame = self.frame;
    frame.origin.y += frame.size.height;
    
    if (animated) {
        [UIView animateWithDuration:0.1 animations:^{
            self.overlayView.alpha = 0.0;
        } completion:^(BOOL finished) {
            [self.overlayView removeFromSuperview];
            [UIView animateWithDuration:0.2 animations:^{
                self.frame = frame;
            } completion:^(BOOL finished) {
                [self removeFromSuperview];
            }];
        }];
        
    } else {
        self.overlayView.alpha = 0.0;
        self.frame = frame;
    }
}

- (void)setUpDefault {
    
}

#pragma mark - Actions
- (IBAction)doneAction:(id)sender {
    [self hide:YES];
    if ([self.delegate respondsToSelector:@selector(datePickerDidPickDate:date:)]) {
        [self.delegate datePickerDidPickDate:self date:self.datePicker.date];
    }
}

- (IBAction)cancelAction:(id)sender {
    [self hide:YES];
    if ([self.delegate respondsToSelector:@selector(datePickerDidCancel:)]) {
        [self.delegate datePickerDidCancel:self];
    }
}

@end
