//
//  DropdownView.h
//
//  Copyright © 2018 TheTiger. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DropdownView;
@class DropdownViewDelegate;

@protocol DropdownViewDelegate <NSObject>
- (void)dropDown:(DropdownView *)dropDown didSelectItem:(NSString *)item;
@end

@interface DropdownView : UIView

@property (nonatomic, weak) IBOutlet UIPickerView *pickerView;
@property (nonatomic, weak) IBOutlet UIButton *btnDone;
@property (nonatomic, assign) id <DropdownViewDelegate> delegate;

+ (instancetype)view;

- (void)showInView:(UIView *)view;
- (void)hide;

@end
