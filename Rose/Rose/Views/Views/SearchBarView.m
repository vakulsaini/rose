//
//  SearchBarView.m
//  Rose
//
//  Created by Vakul Saini on 31/12/19.
//  Copyright © 2019 QualiteSoft. All rights reserved.
//

#import "SearchBarView.h"

@interface SearchBarView () <UITextFieldDelegate>

@end

@implementation SearchBarView

+ (instancetype)view {
    SearchBarView *v = [SearchBarView loadFromNib];
    [v setUpDefault];
    return v;
}

- (void)showOnView:(UIView *)view animated:(BOOL)animated {
    [self clearText];
    self.alpha = 0.0;
    self.frame = view.bounds;
    [view addSubview:self];
    [self showWithAnimation:YES];
    [self.textField becomeFirstResponder];
}

- (void)hide:(BOOL)animated {
    [self hideWithAnimation:animated];
}

- (void)setUpDefault {
    self.backgroundColor = APP_WHITE_COLOR;
    
    self.textField.delegate = self;
    [self.textField addTarget:self action:@selector(textFieldDidEditingChange:) forControlEvents:UIControlEventEditingChanged];
    
    UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 32.0, 22.0)];
    self.textField.leftView = leftView;
    self.textField.leftViewMode = UITextFieldViewModeAlways;
}

- (void)clearText {
    self.textField.text = @"";
}

#pragma mark - Actions
- (IBAction)cancelAction:(id)sender {
    [self.textField resignFirstResponder];
    if ([self.delegate respondsToSelector:@selector(searchBarDidClose:)]) {
        [self.delegate searchBarDidClose:self];
    }
}

#pragma mark - TextField Delegates
- (void)textFieldDidEditingChange:(UITextField *)textField {
    if ([self.delegate respondsToSelector:@selector(searchBarDidEditingChange:)]) {
        [self.delegate searchBarDidEditingChange:self];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

@end
