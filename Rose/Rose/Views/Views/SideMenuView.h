//
//  SideMenuView.h
//
//  Copyright © 2018 TheTiger. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SideMenuView;
@class SideMenuDelegate;

@protocol SideMenuDelegate <NSObject>
- (void)sideMenu:(SideMenuView *)sideMenu clickedAtIndex:(NSIndexPath *)indexPath;
@end

@interface SideMenuView : UIView

@property (nonatomic, strong) UIView *container;
@property (nonatomic, assign) id <SideMenuDelegate> delegate;

+ (instancetype)view;
- (void)toggle;

@end
