//
//  SettingsCell.h
//  Rose
//
//  Created by Vakul Saini on 31/12/19.
//  Copyright © 2019 QualiteSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomTitleLabel.h"

NS_ASSUME_NONNULL_BEGIN
@class SettingsCell;

typedef void(^SettingsCellToggleButtonBlock)(UISwitch *swtch, SettingsCell *cell);

@interface SettingsCell : UITableViewCell

@property (nonatomic, weak) IBOutlet CustomTitleLabel *lblTitle;
@property (nonatomic, weak) IBOutlet UIButton *btnImage;
@property (nonatomic, weak) IBOutlet UISwitch *swtch;

@property (nonatomic, getter=isToggleButtonCell) BOOL toggleButtonCell;
@property (nonatomic, strong) _Nullable SettingsCellToggleButtonBlock toggleButtonblock;

@end

NS_ASSUME_NONNULL_END
