//
//  TransPatientInfoCell.h
//  Rose
//
//  Created by Vakul Saini on 01/02/20.
//  Copyright © 2020 QualiteSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomHeadingLabel.h"
#import "CustomTitleLabel.h"
#import "CustomTitleSubLabel.h"
#import "Patient.h"

NS_ASSUME_NONNULL_BEGIN

@interface TransPatientInfoCell : UITableViewCell

@property (nonatomic, weak) IBOutlet CustomHeadingLabel *lblHeadingMain;
@property (nonatomic, weak) IBOutlet CustomTitleLabel *lblHeading;

@property (nonatomic, weak) IBOutlet CustomTitleSubLabel *lblPatientNameHeading;
@property (nonatomic, weak) IBOutlet CustomTitleSubLabel *lblPatientName;

@property (nonatomic, weak) IBOutlet CustomTitleSubLabel *lblDOBHeading;
@property (nonatomic, weak) IBOutlet CustomTitleSubLabel *lblDOB;

@property (nonatomic, weak) IBOutlet CustomTitleSubLabel *lblPHNHeading;
@property (nonatomic, weak) IBOutlet CustomTitleSubLabel *lblPHN;

@property (nonatomic, weak) IBOutlet UIView *viewBottomLine;

@property (nonatomic, strong) Patient *patient;

@end

NS_ASSUME_NONNULL_END
