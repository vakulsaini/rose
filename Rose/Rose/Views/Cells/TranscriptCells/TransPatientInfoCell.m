//
//  TransPatientInfoCell.m
//  Rose
//
//  Created by Vakul Saini on 01/02/20.
//  Copyright © 2020 QualiteSoft. All rights reserved.
//

#import "TransPatientInfoCell.h"

@implementation TransPatientInfoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.lblHeadingMain.isBoldFont = YES;
    self.lblHeading.isBoldFont = YES;
    self.lblDOBHeading.isBoldFont = YES;
    self.lblPHNHeading.isBoldFont = YES;
    self.lblPatientNameHeading.isBoldFont = YES;
    
    self.lblHeadingMain.textColor = APP_BLACK_COLOR;
    self.lblHeading.textColor = APP_BLACK_COLOR;
    self.lblPHNHeading.textColor = APP_BLACK_COLOR;
    self.lblDOBHeading.textColor = APP_BLACK_COLOR;
    self.lblPatientNameHeading.textColor = APP_BLACK_COLOR;
    
    self.lblPatientName.textColor = APP_LIGHT_GRAY_COLOR;
    self.lblDOB.textColor = APP_LIGHT_GRAY_COLOR;
    self.lblPHN.textColor = APP_LIGHT_GRAY_COLOR;
    self.lblDOB.textColor = APP_LIGHT_GRAY_COLOR;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (void)setPatient:(Patient *)patient {
    _patient = patient;
    self.lblPatientName.text = [patient.PatientName showThisIfEmpty:@"-"];
    self.lblDOB.text = [patient.PatientDOB showThisIfEmpty:@"-"];
    self.lblPHN.text = [patient.PatientPHN showThisIfEmpty:@"-"];
}

@end
