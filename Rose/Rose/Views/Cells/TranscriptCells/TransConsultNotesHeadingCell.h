//
//  TransConsultNotesHeadingCell.h
//  Rose
//
//  Created by Vakul Saini on 01/02/20.
//  Copyright © 2020 QualiteSoft. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TransConsultNotesHeadingCell : UITableViewCell
@property (nonatomic, weak) IBOutlet UILabel *lblHeading;
@end

NS_ASSUME_NONNULL_END
