//
//  TransConsultNotesCell.m
//  Rose
//
//  Created by Vakul Saini on 01/02/20.
//  Copyright © 2020 QualiteSoft. All rights reserved.
//

#import "TransConsultNotesCell.h"

@implementation TransConsultNotesCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (void)setConsultNotes:(ConsultNotes *)consultNotes {
    _consultNotes = consultNotes;
    
    if (consultNotes.userType == UserTypePhysician) {
        self.lblSignHeading.text = @"Physician Signature:";
        self.lblPhysicianHeading.text = @"Physician Name:";
    }
    else {
        self.lblSignHeading.text = @"Consultant Signature:";
        self.lblPhysicianHeading.text = @"Consultant Name:";
    }
    
    self.lblPhysician.text = [consultNotes.ConsultantName showThisIfEmpty:@"-"];
    self.lblNotes.text = [consultNotes.ConsultationNote showThisIfEmpty:@"-"];
    [self.iVSign sd_setImageWithURL:[NSURL URLWithString:consultNotes.Signature]];
    
    self.lblDateTime.text = [[[[consultNotes.ConsultationDate dateWithFormat:kDateFormatFullServerSide] toLocal] stringWithFormat:kDateFormatLocalClientSide] lowercaseString];
}

@end
