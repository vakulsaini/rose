//
//  TransNotesSignCell.m
//  Rose
//
//  Created by Vakul Saini on 01/02/20.
//  Copyright © 2020 QualiteSoft. All rights reserved.
//

#import "TransNotesSignCell.h"

@interface TransNotesSignCell() <UITextViewDelegate>
@property (nonatomic, strong) UILabel *lblPlaceHolder;
@end

@implementation TransNotesSignCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.tVNotes.delegate = self;
    self.signContainer.layer.borderWidth = 2.0;
    self.signContainer.layer.borderColor = APP_PINK_COLOR.CGColor;
    self.signContainer.layer.cornerRadius = 5.0;
    self.lblPlaceHolder = [[UILabel alloc] init];
    [self setUpPlaceholder];
    
    [self.btnSubmit makeRoundWithRadius:2.0];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)clearAction:(id)sender {
    [self.signatureView erase];
}

- (IBAction)submitAction:(id)sender {
    if (self.submitButtonblock) {
        self.submitButtonblock(sender, self);
    }
}

#pragma mark - Methods
- (void)setUpPlaceholder {
    
    self.lblPlaceHolder.text = @"Notes";
    self.lblPlaceHolder.textColor = [UIColor grayColor];
    self.lblPlaceHolder.font = self.tVNotes.font;
    [self.lblPlaceHolder sizeToFit];
    
    CGRect rect = self.lblPlaceHolder.frame;
    rect.origin.x = 4.0;
    rect.origin.y = 8.0;
    self.lblPlaceHolder.frame = rect;
    [self.tVNotes addSubview:self.lblPlaceHolder];
}

- (void)showHidePlaceholder {
    NSString *text = self.tVNotes.text;
    self.lblPlaceHolder.hidden = text.length > 0;
}

#pragma mark - UITextView Delegates
- (void)textViewDidChange:(UITextView *)textView {
    [self showHidePlaceholder];
}

@end
