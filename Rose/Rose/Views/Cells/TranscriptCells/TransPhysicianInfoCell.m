//
//  TransPhysicianInfoCell.m
//  Rose
//
//  Created by Vakul Saini on 01/02/20.
//  Copyright © 2020 QualiteSoft. All rights reserved.
//

#import "TransPhysicianInfoCell.h"

@implementation TransPhysicianInfoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setPatient:(Patient *)patient {
    _patient = patient;
    if (patient.userType == UserTypePhysician) {
        self.lblHeading.text = @"Referring Physician Information";
    }
    else {
        self.lblHeading.text = @"Referring Consultant Information";
    }
    self.lblPhysician.text = [patient.ReferralName showThisIfEmpty:@"-"];
    self.lblNote.text = [patient.Note showThisIfEmpty:@"-"];
    self.lblMSP.text = [patient.MSP showThisIfEmpty:@"-"];
    self.lblPhone.text = [patient.UserPhone showThisIfEmpty:@"-"];
}

@end
