//
//  TransConsultNotesCell.h
//  Rose
//
//  Created by Vakul Saini on 01/02/20.
//  Copyright © 2020 QualiteSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomTitleSubLabel.h"
#import "ConsultNotes.h"

NS_ASSUME_NONNULL_BEGIN

@interface TransConsultNotesCell : UITableViewCell

@property (nonatomic, weak) IBOutlet CustomTitleSubLabel *lblPhysicianHeading;
@property (nonatomic, weak) IBOutlet CustomTitleSubLabel *lblPhysician;

@property (nonatomic, weak) IBOutlet CustomTitleSubLabel *lblDateTimeHeading;
@property (nonatomic, weak) IBOutlet CustomTitleSubLabel *lblDateTime;

@property (nonatomic, weak) IBOutlet CustomTitleSubLabel *lblNotesHeading;
@property (nonatomic, weak) IBOutlet CustomTitleSubLabel *lblNotes;

@property (nonatomic, weak) IBOutlet CustomTitleSubLabel *lblMSPHeading;
@property (nonatomic, weak) IBOutlet CustomTitleSubLabel *lblMSP;

@property (nonatomic, weak) IBOutlet CustomTitleSubLabel *lblSignHeading;
@property (nonatomic, weak) IBOutlet UIImageView *iVSign;

@property (nonatomic, strong) ConsultNotes *consultNotes;

@end

NS_ASSUME_NONNULL_END
