//
//  TransPhysicianInfoCell.h
//  Rose
//
//  Created by Vakul Saini on 01/02/20.
//  Copyright © 2020 QualiteSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomTitleLabel.h"
#import "CustomTitleSubLabel.h"
#import "Patient.h"

NS_ASSUME_NONNULL_BEGIN

@interface TransPhysicianInfoCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *lblHeading;

@property (nonatomic, weak) IBOutlet CustomTitleSubLabel *lblPhysicianHeading;
@property (nonatomic, weak) IBOutlet CustomTitleSubLabel *lblPhysician;
@property (nonatomic, weak) IBOutlet CustomTitleSubLabel *lblNote;
@property (nonatomic, weak) IBOutlet CustomTitleSubLabel *lblMSP;
@property (nonatomic, weak) IBOutlet CustomTitleSubLabel *lblPhoneHeading;
@property (nonatomic, weak) IBOutlet CustomTitleSubLabel *lblPhone;
@property (nonatomic, weak) IBOutlet UIView *viewBottomLine;

@property (nonatomic, strong) Patient *patient;

@end

NS_ASSUME_NONNULL_END
