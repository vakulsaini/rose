//
//  TransNotesSignCell.h
//  Rose
//
//  Created by Vakul Saini on 01/02/20.
//  Copyright © 2020 QualiteSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UviSignatureView.h"

NS_ASSUME_NONNULL_BEGIN

@class TransNotesSignCell;

typedef void(^TransNotesSignCellSubmitButtonBlock)(UIButton *sender, TransNotesSignCell *cell);

@interface TransNotesSignCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UITextView *tVNotes;
@property (nonatomic, weak) IBOutlet UIView *signContainer;
@property (nonatomic, weak) IBOutlet UIButton *btnSubmit;

@property (nonatomic, weak) IBOutlet UIButton *btnClear;
@property (weak, nonatomic) IBOutlet UviSignatureView *signatureView;
@property (nonatomic, strong) _Nullable TransNotesSignCellSubmitButtonBlock submitButtonblock;
- (void)showHidePlaceholder;

@end

NS_ASSUME_NONNULL_END
