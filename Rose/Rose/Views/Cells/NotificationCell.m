//
//  NotificationCell.m
//
//  Created by Vakul Saini on 06/12/18.
//  Copyright © 2018 TheTiger. All rights reserved.
//

#import "NotificationCell.h"

@implementation NotificationCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self.bgView makeRoundWithRadius:15.0];
    [self.iVIcon makeRound];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

- (void)setNotification:(Notification *)notification {
    _notification = notification;
//    self.lblTitle.text = notification.title;

    self.lblDate.text = [[[[notification.CallDate dateWithFormat:kDateFormatFullServerSide] toLocal] stringWithFormat:kDateFormatLocalClientSide] lowercaseString];;
    self.lblDesc.text = notification.combinedMessage;
    self.bgView.backgroundColor = APP_WHITE_COLOR;
    [self.iVIcon sd_setImageWithURL:[NSURL URLWithString:notification.CallerProfilepic] placeholderImage:kUserProfilePlaceholder];
    
  //  self.iVIcon.image = [UIImage imageNamed:@""];
//    if ([notification.ReadbyMe integerValue] == 0) {
//        self.bgView.backgroundColor = [APP_PINK_COLOR colorWithAlphaComponent:0.6];
//    }
//    else {
//        self.bgView.backgroundColor = APP_WHITE_COLOR;
//    }
}

@end
