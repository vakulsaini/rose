//
//  ContactsCell.h
//  Rose
//
//  Created by Virender on 25/12/19.
//  Copyright © 2019 QualiteSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomTitleLabel.h"
#import "CustomTitleSubLabel.h"

NS_ASSUME_NONNULL_BEGIN

@class ContactsCell;

typedef void(^ContactsCellPhoneButtonBlock)(UIButton *sender, ContactsCell *cell);


@interface ContactsCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *iVProfile;
@property (weak, nonatomic) IBOutlet UIImageView *iVCall;
@property (weak, nonatomic) IBOutlet CustomTitleLabel *lblName;
@property (weak, nonatomic) IBOutlet UIView *viewContainer;
@property (weak, nonatomic) IBOutlet CustomTitleSubLabel *lblLocation;
@property (weak, nonatomic) IBOutlet CustomTitleSubLabel *lblEmail;
@property (weak, nonatomic) IBOutlet CustomTitleSubLabel *lblPhone;
@property (nonatomic, strong) RUser *user;

@property (nonatomic, strong) _Nullable ContactsCellPhoneButtonBlock phoneButtonblock;


@end

NS_ASSUME_NONNULL_END
