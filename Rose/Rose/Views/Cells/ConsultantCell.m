//
//  ConsultantCell.m
//  Rose
//
//  Created by Virender on 30/12/19.
//  Copyright © 2019 QualiteSoft. All rights reserved.
//

#import "ConsultantCell.h"

@implementation ConsultantCell

 
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self.viewContainer makeRoundWithRadius:15.0];
    self.lblName.textColor = APP_BLACK_COLOR;
    self.lblPhone.textColor = APP_LIGHT_GRAY_COLOR;
    [self.iVProfile makeRound];
    
    self.iVCall.image = [[UIImage imageNamed:@"call_filled"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.iVCall.tintColor = APP_PINK_COLOR;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setUser:(ConsultUser *)user {
    _user = user;

    self.lblName.text = [user fullName];
    self.lblName.isBoldFont = YES;
    self.lblPhone.text = [NSString stringWithFormat:@"%@ %@", user.Phone, @"Consultant"];
    [self.iVProfile sd_setImageWithURL:[NSURL URLWithString:user.ProfilePic] placeholderImage:kUserProfilePlaceholder];
    
   // if (![APP_DELEGATE.currentUser isPhysician]) {
        if ([user isEnabled]) {
            self.viewContainer.alpha = 1.0;
            self.viewContainer.backgroundColor = [UIColor whiteColor];
            // self.viewCall.userInteractionEnabled = YES;
        }
        else {
            self.viewContainer.alpha = 0.5;
            self.viewContainer.backgroundColor = COLOR_FROM_RGBA(@"220,220,220,1");
            // self.viewCall.userInteractionEnabled = NO;
        }
   // }
}

#pragma mark - Actions
- (IBAction)phoneAction:(id)sender {
    if (self.phoneButtonblock) {
        self.phoneButtonblock(sender, self);
    }
}

@end
