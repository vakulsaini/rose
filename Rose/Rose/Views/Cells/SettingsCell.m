//
//  SettingsCell.m
//  Rose
//
//  Created by Vakul Saini on 31/12/19.
//  Copyright © 2019 QualiteSoft. All rights reserved.
//

#import "SettingsCell.h"

@implementation SettingsCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.btnImage.userInteractionEnabled = false;
    self.swtch.onTintColor = APP_PINK_COLOR;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setToggleButtonCell:(BOOL)toggleButtonCell {
    _toggleButtonCell = toggleButtonCell;
    if (toggleButtonCell) {
        self.btnImage.hidden = YES;
        self.swtch.hidden = NO;
    }
    else {
        self.btnImage.hidden = NO;
        self.swtch.hidden = YES;
    }
}

- (IBAction)toggleAction:(id)sender {
    if (self.toggleButtonblock) {
        self.toggleButtonblock(sender, self);
    }
}

@end
