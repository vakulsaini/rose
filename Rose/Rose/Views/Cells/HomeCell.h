//
//  HomeCell.h
//  Rose
//
//  Created by Virender on 25/12/19.
//  Copyright © 2019 QualiteSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomTitleLabel.h"
#import "CustomTitleSubLabel.h"

NS_ASSUME_NONNULL_BEGIN

@class HomeCell;

typedef void(^HomeCellInfoButtonBlock)(UIButton *sender, HomeCell *cell);

@interface HomeCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *iVProfile;
@property (weak, nonatomic) IBOutlet UIImageView *iVInfo;
@property (weak, nonatomic) IBOutlet CustomTitleLabel *lblName;
@property (weak, nonatomic) IBOutlet UIView *viewContainer;
@property (weak, nonatomic) IBOutlet CustomTitleSubLabel *lblhospitalName;
@property (weak, nonatomic) IBOutlet CustomTitleSubLabel *lblDate;
@property (nonatomic, strong) CallHistory *callHistory;
@property (nonatomic, strong) _Nullable HomeCellInfoButtonBlock infoButtonblock;

+ (instancetype)cell;

@end

NS_ASSUME_NONNULL_END
