//
//  HomeCell.m
//  Rose
//
//  Created by Virender on 25/12/19.
//  Copyright © 2019 QualiteSoft. All rights reserved.
//

#import "HomeCell.h"

@implementation HomeCell

+ (instancetype)cell {
    HomeCell *_cell = [self loadFromNib];
    _cell.selectionStyle = UITableViewCellSelectionStyleNone;
     return _cell;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self.viewContainer makeRoundWithRadius:15.0];
    [self.iVProfile makeRound];
    self.lblhospitalName.textColor = APP_LIGHT_GRAY_COLOR;
    self.lblDate.textColor = APP_LIGHT_GRAY_COLOR;
    
    self.iVInfo.image = [[UIImage imageNamed:@"info"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.iVInfo.tintColor = APP_PINK_COLOR;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)infoAction:(id)sender {
    if (self.infoButtonblock) {
        self.infoButtonblock(sender, self);
    }
}

- (void)setCallHistory:(CallHistory *)callHistory {
    _callHistory = callHistory;
    self.lblName.isBoldFont = YES;
    
    self.lblDate.text = [[[[callHistory.CallPlaceDate dateWithFormat:kDateFormatFullServerSide] toLocal] stringWithFormat:kDateFormatLocalClientSide] lowercaseString];
    
    self.lblhospitalName.text = callHistory.location;
    [self.iVProfile sd_setImageWithURL:[NSURL URLWithString:callHistory.profilePic] placeholderImage:kUserProfilePlaceholder];
    
    if (callHistory.callType == CallTypeMissedCall) {
        self.lblName.text = [NSString stringWithFormat:@"%@ (Missed Call)", callHistory.name];
        self.lblName.textColor = APP_ORANGE_COLOR;
    }
    else if (callHistory.callType == CallTypeDialed) {
        self.lblName.text = [NSString stringWithFormat:@"%@ (Outgoing)", callHistory.name];
        self.lblName.textColor = APP_BLACK_COLOR;
    }
    else {
        // Received
        self.lblName.textColor = APP_BLACK_COLOR;
        self.lblName.text = [NSString stringWithFormat:@"%@ (Incoming)", callHistory.name];
    }
}


@end
