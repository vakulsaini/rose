//
//  ProfileCell.m
//  Rose
//
//  Created by Virender on 30/12/19.
//  Copyright © 2019 QualiteSoft. All rights reserved.
//

#import "ProfileCell.h"


@implementation ProfileCell

+ (instancetype)cell {
    ProfileCell *_cell = [self loadFromNib];
    _cell.selectionStyle = UITableViewCellSelectionStyleNone;
     return _cell;
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self.viewContainer makeRoundWithRadius:15.0];
    self.lblhospitalName.textColor = APP_GRAY_COLOR;
    self.lblDate.textColor = APP_GRAY_COLOR;
    self.lblName.textColor = APP_BLACK_COLOR;
    
    self.iVSim.image = [[UIImage imageNamed:@"sim"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.iVSim.tintColor = APP_PINK_COLOR;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setPatient:(Patient *)patient {
    _patient = patient;
    self.lblName.text = patient.PatientName;
    self.lblName.isBoldFont = YES;
    self.lblhospitalName.text = patient.PatientPHN;
    
    self.lblDate.text = [[[[self.patient.PatientCreateDate dateWithFormat:kDateFormatFullServerSide] toLocal] stringWithFormat:kDateFormatLocalClientSide] lowercaseString];
}

@end
