//
//  ContactsCell.m
//  Rose
//
//  Created by Virender on 25/12/19.
//  Copyright © 2019 QualiteSoft. All rights reserved.
//

#import "ContactsCell.h"

@implementation ContactsCell
 

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self.iVProfile makeRound];
    self.lblName.textColor = APP_BLACK_COLOR;
    self.lblLocation.textColor = APP_LIGHT_GRAY_COLOR;
    self.lblPhone.textColor = APP_LIGHT_GRAY_COLOR;
    self.lblEmail.textColor = APP_LIGHT_GRAY_COLOR;
    
    self.iVCall.image = [[UIImage imageNamed:@"call"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.iVCall.tintColor = APP_PINK_COLOR;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setUser:(RUser *)user {
    _user = user;
    
    self.lblName.text = user.fullName;
    self.lblName.isBoldFont = YES;

    self.lblLocation.text = user.Location;
    self.lblPhone.text = user.Phone;
    self.lblEmail.text = user.Email;
    [self.iVProfile sd_setImageWithURL:[NSURL URLWithString:user.ProfilePic] placeholderImage:kUserProfilePlaceholder];
}

#pragma mark - Actions
- (IBAction)phoneAction:(id)sender {
    if (self.phoneButtonblock) {
        self.phoneButtonblock(sender, self);
    }
}


@end
