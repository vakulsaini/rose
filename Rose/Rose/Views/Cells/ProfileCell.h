//
//  ProfileCell.h
//  Rose
//
//  Created by Virender on 30/12/19.
//  Copyright © 2019 QualiteSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProfileUser.h"
#import "Patient.h"
#import "CustomTitleLabel.h"
#import "CustomTitleSubLabel.h"


NS_ASSUME_NONNULL_BEGIN

@interface ProfileCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *iVProfile;
@property (weak, nonatomic) IBOutlet UIImageView *iVSim;
@property (weak, nonatomic) IBOutlet CustomTitleLabel *lblName;
@property (weak, nonatomic) IBOutlet CustomTitleSubLabel *lblhospitalName;
@property (weak, nonatomic) IBOutlet CustomTitleSubLabel *lblDate;

@property (weak, nonatomic) IBOutlet UIView *viewContainer;
@property (nonatomic, strong) Patient *patient;


+ (instancetype)cell;

@end

NS_ASSUME_NONNULL_END
