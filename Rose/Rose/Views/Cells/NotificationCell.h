//
//  NotificationCell.h
//
//  Created by Vakul Saini on 06/12/18.
//  Copyright © 2018 TheTiger. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomTitleLabel.h"
#import "CustomTitleSubLabel.h"
#import "Notification.h"

@interface NotificationCell : UITableViewCell

@property (nonatomic, weak) IBOutlet CustomTitleLabel *lblTitle;
@property (nonatomic, weak) IBOutlet UIImageView *iVIcon;
@property (nonatomic, weak) IBOutlet CustomTitleSubLabel *lblDesc;
@property (nonatomic, weak) IBOutlet CustomTitleSubLabel *lblDate;
@property (nonatomic, weak) IBOutlet UIView *bgView;

@property (nonatomic, strong) Notification *notification;

@end
