//
//  ConsultantCell.h
//  Rose
//
//  Created by Virender on 30/12/19.
//  Copyright © 2019 QualiteSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ConsultUser.h"
#import "CustomTitleLabel.h"
#import "CustomTitleSubLabel.h"

NS_ASSUME_NONNULL_BEGIN

@class ConsultantCell;

typedef void(^ConsultantCellPhoneButtonBlock)(UIButton *sender, ConsultantCell *cell);

@interface ConsultantCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *iVProfile;
@property (weak, nonatomic) IBOutlet UIImageView *iVCall;
@property (weak, nonatomic) IBOutlet UIView *viewContainer;
@property (weak, nonatomic) IBOutlet UIView *viewCall;
@property (weak, nonatomic) IBOutlet CustomTitleLabel *lblName;
@property (weak, nonatomic) IBOutlet CustomTitleSubLabel *lblPhone;
@property (nonatomic, strong) ConsultUser *user;
@property (nonatomic, strong) _Nullable ConsultantCellPhoneButtonBlock phoneButtonblock;

@end

NS_ASSUME_NONNULL_END
