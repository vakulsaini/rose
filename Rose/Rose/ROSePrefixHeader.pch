//
//  ROSePrefixHeader.pch
//  Rose
//
//  Created by Vakul Saini on 23/12/19.
//  Copyright © 2019 QualiteSoft. All rights reserved.
//

#ifndef ROSePrefixHeader_pch
#define ROSePrefixHeader_pch

// Include any system framework and library headers here that should be included in all compilation units.
// You will also need to set the Prefix Header build setting of one or more of your targets to reference this file.

#ifdef DEBUG
#define DLog(fmt, ...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);
#else
#   define DLog(...)
#endif

/**
 A macro to check if internet is available or not.
 
 @return returns a BOOL value.
 */
// #define IsInternetAvailable ([AppDelegate sharedInstance].reachability.currentReachabilityStatus != NotReachable)
#define IsInternetAvailable [[AppDelegate sharedInstance].reachability isReachable]

/**
 Static internet alert title.
 */
#define kNoInternetTitle @"Internet Error!"

/**
 Static internet alert message.
 */
#define kNoInternetMessage @"You're currently not connected with internet. Please check your internet connection and try again."


#import "AppDelegate.h"
#import "UIColor+Custom.h"
#import "UIView+Custom.h"
#import "UITextField+Custom.h"
#import "NSObject+ObjectMapping.h"
#import "NSDate+Utility.h"
#import "NSString+Custom.h"
#import "UIViewController+Custom.h"
#import "ApiHandler.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "Helper.h"

#import "LoadingView.h"
#import "VSDeviceInfo.h"

#import "RUser.h"
#import "CallHistory.h"
#import "Patient.h"


#define DEVICE_HEIGHT   [UIScreen mainScreen].bounds.size.height
#define DEVICE_WIDTH    [UIScreen mainScreen].bounds.size.width
#define IS_IPHONE_X             (MAX(DEVICE_WIDTH, DEVICE_HEIGHT) == 812.0)
#define IS_IPHONE_4or5orSE      (MIN(DEVICE_WIDTH, DEVICE_HEIGHT) == 320.0)
#define IS_IPHONE_6or7or8       (MIN(DEVICE_WIDTH, DEVICE_HEIGHT) == 375.0)
#define IS_IPHONE_6Por7Por8P    (MIN(DEVICE_WIDTH, DEVICE_HEIGHT) == 414.0)

#define APP_DELEGATE ((AppDelegate *)[[UIApplication sharedApplication] delegate])
#define DEFAULT_LOCATION [[CLLocation alloc] initWithLatitude:28.5191278 longitude:-81.36557319999997]
#define K_GLOBAL_DATE_FORMAT @"MM/dd/yy"

#define NOTIFICATION_USER_DID_LOGIN     @"NOTIFICATION_USER_DID_LOGIN"
#define NOTIFICATION_USER_DID_LOGOUT    @"NOTIFICATION_USER_DID_LOGOUT"


/// ----------------------- Colors -----------------------
#define APP_BLACK_COLOR     [UIColor blackColor]
#define APP_ORANGE_COLOR    [UIColor orangeColor]

#define APP_WHITE_COLOR     [UIColor whiteColor]
#define APP_BLUE_COLOR      COLOR_FROM_RGBA(@"29,49,170,1")
#define APP_PINK_COLOR      COLOR_FROM_RGBA(@"243,162,169,1")
#define APP_GRAY_COLOR      COLOR_FROM_RGBA(@"78,79,83,1")
#define APP_LIGHT_GRAY_COLOR [APP_GRAY_COLOR colorWithAlphaComponent:0.78]
#define APP_OFFWHITE_COLOR  COLOR_FROM_RGBA(@"235,235,235,1")

/// ----------------------- Storyboards -----------------------
#define AUTH_STORYBOARD     [UIStoryboard storyboardWithName:@"Auth" bundle:nil]
#define MAIN_STORYBOARD     [UIStoryboard storyboardWithName:@"Main" bundle:nil]
#define SINCH_STORYBOARD    [UIStoryboard storyboardWithName:@"MainStoryBoard" bundle:nil]


/*
 * Loader
 */
#define SHOW_LOADER dispatch_async(dispatch_get_main_queue(), ^{[[LoadingView sharedInstance] show];});
#define HIDE_LOADER dispatch_async(dispatch_get_main_queue(), ^{[[LoadingView sharedInstance] hide];});


#define kImagePlaceholder [UIImage imageNamed:@"image_placeholder"]
#define kUserProfilePlaceholder [UIImage imageNamed:@"profile_pic"]
#define kAttachmentPlaceholder [UIImage imageNamed:@"paper-clip"]


#endif /* ROSePrefixHeader_pch */
