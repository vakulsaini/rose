//
//  MyProfileViewController.m
//  Rose
//
//  Created by Virender on 26/12/19.
//  Copyright © 2019 QualiteSoft. All rights reserved.
//

#import "MyProfileViewController.h"
#import "ProfileCell.h"
#import "M13BadgeView.h"
#import "ProfileUser.h"
#import "SearchBarView.h"
#import "AddContactView.h"
#import "CustomTitleLabel.h"
#import "CustomTitleSubLabel.h"
#import "CustomButton.h"
#import "TranscriptionViewController.h"
#import "ConsultUser.h"
#import "TabBarController.h"

@interface MyProfileViewController () <SearchBarViewDelegate, AddContactViewDelegate, CallViewControllerEventDelegate> {
    NSArray *enabledConsultants;
    NSInteger currentIndex;
}
/**
 UITableView IBOutlet - Main table
 */
@property (weak, nonatomic) IBOutlet UITableView *tblView;
@property (weak, nonatomic) IBOutlet UIImageView *iVLogo;
@property (nonatomic, weak) IBOutlet UILabel *lblNoRecords;


@property (weak, nonatomic) IBOutlet CustomTitleLabel *lblName;
@property (weak, nonatomic) IBOutlet CustomTitleSubLabel *lblLocation;
@property (weak, nonatomic) IBOutlet CustomTitleSubLabel *lblPincode;
 
/**
 UIButton IBOutlet - addContact
 */
@property (nonatomic, weak) IBOutlet UIButton *btnAddContact;

@property (nonatomic, weak) IBOutlet UIButton *btnBell;

@property (nonatomic, strong) M13BadgeView *badgeView;

@property (nonatomic, strong) NSMutableArray *patients;
@property (nonatomic, strong) NSMutableArray *tableUsers;
@property (nonatomic, strong) SearchBarView *searchBarView;
@property (nonatomic, strong) AddContactView *addContactView;
@property (nonatomic, strong) Patient *currentPatient;


@end

@implementation MyProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.btnAddContact makeRound];
    //self.btnAddContact.backgroundColor = APP_PINK_COLOR;
    //self.btnAddContact.layer.borderColor = [UIColor whiteColor].CGColor;
    //self.btnAddContact.layer.borderWidth = 1.0;
    
    
    self.lblName.isBoldFont = YES;
    
    [self setUpBadge];
    self.tblView.backgroundColor = [UIColor clearColor];
    [self.tblView registerNib:[UINib nibWithNibName:@"ProfileCell" bundle:nil] forCellReuseIdentifier:@"ProfileCell"];
    self.tblView.contentInset = UIEdgeInsetsMake(0.0, 0.0, 80.0, 0.0);
    [self.iVLogo makeRound];

    self.patients = [[NSMutableArray alloc] init];
    self.tableUsers = [[NSMutableArray alloc] init];

    [self reloadTable];
    
    self.searchBarView = [SearchBarView view];
    self.searchBarView.delegate = self;
    
    currentIndex = 0;
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    [self adjustBadgeFrame];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self setUpUI];
    [self loadPatients:^{
        [self applyFilter];
        [self reloadTable];
    }];
    
    [APP_DELEGATE refreshUnreadNotifications:^{
        [self refreshBadgeValue];
    }];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];

}

#pragma mark - Actions
- (IBAction)bellAction:(id)sender {
    [APP_DELEGATE goToNotificationsFrom:self];
}

- (IBAction)addContactAction:(id)sender {
    
    [APP_DELEGATE.tabBarController showAddContactPopup];
    
    /*self.addContactView = [AddContactView view];
    self.addContactView.delegate = self;
     self.addContactView.tfPatientMSPNumber.text = APP_DELEGATE.currentUser.MSPNumber;
    [self.addContactView showOnView:APP_DELEGATE.window animated:YES];*/
}

- (IBAction)searchAction:(id)sender {
    [self.searchBarView showOnView:[sender superview] animated:YES];
}

- (IBAction)settingsAction:(id)sender {
    [APP_DELEGATE goToSettingsFrom:self];
}

#pragma mark - Methods
- (void)setUpUI {
    self.lblName.text = [APP_DELEGATE.currentUser fullName];
    NSURL *imageUrl = [NSURL URLWithString:APP_DELEGATE.currentUser.ProfilePic];
    [self.iVLogo sd_setImageWithURL:imageUrl placeholderImage:kUserProfilePlaceholder];
    
    self.lblLocation.text = [NSString stringWithFormat:@"Speciality %@", APP_DELEGATE.currentUser.Speciality];
    self.lblPincode.text = APP_DELEGATE.currentUser.Phone;
}

- (void)setUpBadge {
    self.badgeView = [[M13BadgeView alloc] initWithFrame:CGRectMake(0, 0, 24.0, 24.0)];
    self.badgeView.font = [UIFont fontWithName:@"Helvetica Neue" size:14.0];
    self.badgeView.text = @"";
    self.badgeView.horizontalAlignment = M13BadgeViewHorizontalAlignmentRight;
    self.badgeView.verticalAlignment = M13BadgeViewVerticalAlignmentTop;
    [self.btnBell addSubview:self.badgeView];
    self.badgeView.hidden = YES;
}

- (void)refreshBadgeValue {
    dispatch_async(dispatch_get_main_queue(), ^{
        if ([APP_DELEGATE.unreadCount integerValue] == 0) {
            self.badgeView.text = @"";
            self.badgeView.hidden = YES;
        }
        else {
            self.badgeView.text = [NSString stringWithFormat:@"%@", APP_DELEGATE.unreadCount];
            self.badgeView.hidden = NO;
        }
        [self adjustBadgeFrame];
    });
}

- (void)adjustBadgeFrame {
    [self.badgeView autoSetBadgeFrame];
    
    CGRect frame = self.badgeView.frame;
    frame.origin.x -= 10.0;
    frame.origin.y += 10.0;
    self.badgeView.frame = frame;
}

- (void)applyFilter {
    [self.tableUsers removeAllObjects];
    NSString *text = self.searchBarView.textField.text;
    if (text.length > 0) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.PatientName CONTAINS[cd] %@ || SELF.PatientPHN CONTAINS[cd] %@", text, text];
        NSArray *logs = [self.patients filteredArrayUsingPredicate:predicate];
        [self.tableUsers addObjectsFromArray:logs];
    }
    else {
        [self.tableUsers addObjectsFromArray:self.patients];
    }
    
    // Sort by created date
    /*APP_DELEGATE.dateFormatter.dateFormat = @"yyyy-MM-dd hh:mm:ss";
    [self.tableUsers sortUsingComparator:^NSComparisonResult(Patient *p1, Patient *p2) {
        NSDate *d1 = [APP_DELEGATE.dateFormatter dateFromString:p1.PatientCreateDate];
        NSDate *d2 = [APP_DELEGATE.dateFormatter dateFromString:p2.PatientCreateDate];
        if (d1 && d2) {
            return [d2 compare:d1];
        }
        return YES;
    }];*/
}

- (void)reloadTable {
    dispatch_async(dispatch_get_main_queue(), ^{
        if ([self.tableUsers count] > 0) {
            self.tblView.hidden = NO;
            self.lblNoRecords.hidden = YES;
        }
        else {
            self.tblView.hidden = YES;
            self.lblNoRecords.hidden = NO;
        }
        
        [self.tblView reloadData];
    });
}


- (void)loadPatients:(void (^) (void))completion {
    
    /// Checking for internet connection
    if (!IsInternetAvailable) {
        return;
    }
    
    if (!self.patients) {
        self.patients = [[NSMutableArray alloc] init];
    }
    
    if (self.patients.count == 0) {
        SHOW_LOADER;
    }
    
//    NSString *userTypeStr = [APP_DELEGATE.currentUser isPhysician] ? @"physician" : @"consultant";
    NSString *consultantsAPIUrl = [NSString stringWithFormat:@"%@/%@", WEB_SERVICE_USER_TRANSCRIPT, APP_DELEGATE.currentUser.Id];
    [[ApiHandler shared] sendHTTPRequest:APIRequestTypeGET url:consultantsAPIUrl params:nil withCompletion:^(id response, NSError *error) {
        HIDE_LOADER;
        if (error) {
            completion();
            return;
        }
        
        NSArray *users = response[@"patientList"];
        if (users && [users isKindOfClass:[NSArray class]]) {
            [self.patients removeAllObjects];
            for (NSDictionary *p in users) {
                Patient *patient = [[Patient alloc] initWithDicionary:p];
                [self.patients addObject:patient];
            }
        }
            
        completion();
    }];
}

#pragma mark  UITableView Delegate & Datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.tableUsers count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellId = @"ProfileCell";
    ProfileCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.patient = self.tableUsers[indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    TranscriptionViewController *transsriptionVC = [TranscriptionViewController instance];
    transsriptionVC.patient = self.tableUsers[indexPath.row];
    [self.navigationController pushViewController:transsriptionVC animated:YES];
}

#pragma mark - CreateContact Delegate
- (void)contactViewDidClose:(AddContactView *)contactView {
    
}

- (void)contactViewDidSave:(AddContactView *)contactView {
    
    BOOL success = [self validateFields];
    if (!success) {
        return;
    }
    
    /// Checking for internet connection
    if (!IsInternetAvailable) {
        [self showAlertWithTitle:kNoInternetTitle message:kNoInternetMessage okButtonTitle:@"OK"];
        return;
    }
    
    SHOW_LOADER;
    [APP_DELEGATE loadConsultants:^(NSError *error) {
        
        if (error) {
            [self showAlertWithTitle:@"" message:error.localizedDescription okButtonTitle:@"Ok"];
            HIDE_LOADER;
            return;
        }
        
        NSMutableArray *eConsultants = [[NSMutableArray alloc] init];
        for (ConsultUser *user in APP_DELEGATE.consultUsers) {
            // If enabled consultant and not me
            if ([user isEnabled] && [user.Id integerValue] != [APP_DELEGATE.currentUser.Id integerValue]) {
                [eConsultants addObject:user];
            }
        }
        
        self->enabledConsultants = eConsultants;
        
        if (self->enabledConsultants.count == 0) {
            [self showAlertWithTitle:@"" message:@"There is no consultant enabled." okButtonTitle:@"Ok"];
            HIDE_LOADER;
            return;
        }
        
        //NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"SELF.Enable == %@", @"1"];
        //NSMutableArray *eConsultants = [APP_DELEGATE.consultUsers filteredArrayUsingPredicate:predicate1] ? [[APP_DELEGATE.consultUsers filteredArrayUsingPredicate:predicate1] mutableCopy] : @[].mutableCopy;

        /// Proceed
        [self proceedToCreateTranscription];
    }];
}

- (BOOL)validateFields {
    
    NSString *name = self.addContactView.tfPatientName.text;
    if ([name isEmpty]) {
        [self showAlertWithTitle:@"" message:@"Please enter patient name." okButtonTitle:@"OK"];
        return NO;
    }
    
    NSString *healthNum = self.addContactView.tfPatientHealthNumber.text;
    if ([healthNum isEmpty]) {
        [self showAlertWithTitle:@"" message:@"Please enter patient health number." okButtonTitle:@"OK"];
        return NO;
    }
    
//    NSString *mspNumber = self.addContactView.tfPatientMSPNumber.text;
//    if ([mspNumber isEmpty]) {
//        [self showAlertWithTitle:@"" message:@"Please enter MSP number." okButtonTitle:@"OK"];
//        return NO;
//    }
    
    NSString *phoneNumber = self.addContactView.tfUserPhoneNumber.text;
    if ([phoneNumber isEmpty]) {
        [self showAlertWithTitle:@"" message:@"Please enter User phone number." okButtonTitle:@"OK"];
        return NO;
    }
    
    NSString *dob = self.addContactView.tfPatientDOB.text;
    if ([dob isEmpty]) {
        [self showAlertWithTitle:@"" message:@"Please select patient date of birth." okButtonTitle:@"OK"];
        return NO;
    }
    
    NSString *message = self.addContactView.tVPatientMessage.text;
    if ([message isEmpty]) {
        [self showAlertWithTitle:@"" message:@"Please enter a message." okButtonTitle:@"OK"];
        return NO;
    }
    
    return YES;
}

- (void)proceedToCreateTranscription {

    NSString *name = self.addContactView.tfPatientName.text;
    NSString *healthNum = self.addContactView.tfPatientHealthNumber.text;
    NSString *mspNumber = self.addContactView.tfPatientMSPNumber.text;
    NSString *userPhone = self.addContactView.tfUserPhoneNumber.text;
    NSString *dob = self.addContactView.tfPatientDOB.text;
    NSString *message = self.addContactView.tVPatientMessage.text;
    
    NSDictionary *params = @{@"PatientName": name,
                             @"PatientDOB": dob,
                             @"UserPhone": userPhone,
                             @"ReferringID": APP_DELEGATE.currentUser.Id,
                             @"Note": message,
                             @"PatientPHN": healthNum};
    
    // SHOW_LOADER;
    [[ApiHandler shared] sendHTTPRequest:APIRequestTypePOST url:WEB_SERVICE_PATIENT params:params withCompletion:^(id response, NSError *error) {
        HIDE_LOADER;
        if (error) {
            [self showAlertWithTitle:@"" message:error.localizedDescription okButtonTitle:@"OK"];
            return;
        }
        
        if ([response[@"status"] boolValue]) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.addContactView hide:YES];
            });
            
            NSDictionary *patientData = response[@"patient_data"];
            self.currentPatient = [[Patient alloc] initWithDicionary:patientData];

            self->currentIndex = 0;
            if (self->currentIndex < self->enabledConsultants.count) {
                [self call:self->enabledConsultants[self->currentIndex]];
            }
            
            // [self showAlertWithTitle:@"" message:response[@"message"] okButtonTitle:@"OK"];
        }
        else {
            [self showAlertWithTitle:@"" message:response[@"message"] okButtonTitle:@"OK"];
        }
    }];
}

- (void)proceedToShareTranscriptionForPatient:(NSString *)patientId consultId:(NSString *)consultId {
    
    NSDictionary *params = @{@"PatientID": patientId,
                             @"ConsultantID": consultId};
    
    SHOW_LOADER;
    [[ApiHandler shared] sendHTTPRequest:APIRequestTypePOST url:WEB_SERVICE_SHARE_TRANSCRIPT params:params withCompletion:^(id response, NSError *error) {
        HIDE_LOADER;
        if (error) {
//            [self showAlertWithTitle:@"" message:error.localizedDescription okButtonTitle:@"OK"];
            return;
        }
        
        if ([response[@"status"] boolValue]) {
            
        }
        else {
//            [self showAlertWithTitle:@"" message:response[@"message"] okButtonTitle:@"OK"];
        }
    }];
}


#pragma mark - SearchBarViewDelegates
- (void)searchBarDidEditingChange:(SearchBarView *)view {
    [self applyFilter];
    [self reloadTable];
}

- (void)searchBarDidClose:(SearchBarView *)view {
    [view hide:YES];
    [view clearText];
    [self applyFilter];
    [self reloadTable];
}

#pragma mark - SINCallNotification
- (void)call:(ConsultUser *)user {

    // We can pass the extra information in headers
    // id<SINCall> call = [APP_DELEGATE.callClient callUserVideoWithId:[NSString stringWithFormat:@"%@", user.Id]];
    id<SINCall> call = [APP_DELEGATE createCallInstanceForUserId:[NSString stringWithFormat:@"%@", user.Id]];
    
    [APP_DELEGATE showCall:call from:APP_DELEGATE.window.rootViewController delegate:self];
    
    // Call the API for call logs - > Dial API
    [APP_DELEGATE dialCall:call.callId callerId:[NSString stringWithFormat:@"%@", APP_DELEGATE.currentUser.Id] receiverId:[NSString stringWithFormat:@"%@", user.Id] completion:^{}];
    
    [self proceedToShareTranscriptionForPatient:self.currentPatient.PatientId consultId:call.remoteUserId];
}

- (void)callEvent:(CallEventType)eventType controller:(CallViewController *)controller call:(id<SINCall>)call {
    
    if (eventType == kCallEventTypeProgress) {
        
    }
    else if (eventType == kCallEventTypeEstablished) {
        [self sinCallDidEstablish:call];
    }
    else if (eventType == kCallEventTypeEnd) {
        [self sinCallDidEnd:call];
    }
}

- (void)sinCallDidEnd:(id<SINCall>)call {
    NSLog(@"%ld", call.details.endCause);
    if (call.details.endCause == SINCallEndCauseCanceled) {
        // Call canceled by caller
        // Update it's log on server
        [APP_DELEGATE updateCallStatus:call.callId callStatus:@"decline" callDuration:nil completion:^{}];
        return;
    }
    
    NSDate *callEstablishedTime = call.details.establishedTime;
    NSDate *callEndTime   = call.details.endedTime;
    BOOL callEstablished = NO;
    NSTimeInterval interval = 0;
    if (callEstablishedTime && callEndTime) {
        interval = fabs([callEstablishedTime timeIntervalSinceDate:callEndTime]);
        if (interval > 0) {
            callEstablished = YES;
        }
    }
    
    if (callEstablished) {
        // Call is surely established because call duration is greater than 0 seconds.
        currentIndex = 0;
        [APP_DELEGATE updateCallStatus:call.callId callStatus:@"RECEIVED" callDuration:@(interval) completion:^{}];
    }
    else {
        
        // Update it's log on server
        NSString *status = [APP_DELEGATE status:call];
        [APP_DELEGATE updateCallStatus:call.callId callStatus:status callDuration:nil completion:^{}];
        
        // Call did not establish. Try calling next consultant.
        currentIndex += 1;
        if (currentIndex >= enabledConsultants.count) {
             currentIndex = 0;
            // [self showAlertWithTitle:@"" message:@"No consultant picked your call." okButtonTitle: @"Ok"];
            // return;
        }
        [self call:enabledConsultants[currentIndex]];
    }
}

- (void)sinCallDidEstablish:(id<SINCall>)call {
    // [self proceedToShareTranscriptionForPatient:self.currentPatient.PatientId consultId:call.remoteUserId];
}


@end
