//
//  TabBarController.h
//
//  Copyright © 2018 enAct eServices. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RBaseViewController.h"

@interface TabBarController : UITabBarController

/**
 Creates an instance of this class from Storyboard.

 @return returns an instance of this class.
 */
+ (instancetype)instance;


- (void)showAddContactPopup;

@end
