//
//  EditProfileViewController.h
//  Rose
//
//  Created by Vakul Saini on 07/01/20.
//  Copyright © 2020 QualiteSoft. All rights reserved.
//

#import "RBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface EditProfileViewController : RBaseViewController

@end

NS_ASSUME_NONNULL_END
