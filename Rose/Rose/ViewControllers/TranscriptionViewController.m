//
//  TranscriptionViewController.m
//  Rose
//
//  Created by Vakul Saini on 09/01/20.
//  Copyright © 2020 QualiteSoft. All rights reserved.
//

#import "TranscriptionViewController.h"
#import "TransPatientInfoCell.h"
#import "TransPhysicianInfoCell.h"
#import "TransConsultNotesHeadingCell.h"
#import "TransConsultNotesCell.h"
#import "TransNotesSignCell.h"
#import "ConsultNotes.h"
#import "PreviewView.h"

@interface TranscriptionViewController () {
    BOOL printingIsGoingOn;
}
@property (nonatomic, strong) IBOutlet UITableView *tblView;
@property (nonatomic, strong) IBOutlet UIView *viewPreview;
@property (nonatomic, strong) NSMutableArray *consultNotes;
@property (nonatomic, strong) NSDictionary *patientInfo;
@end

@implementation TranscriptionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.viewPreview makeRound];
    self.viewPreview.layer.borderColor = [UIColor whiteColor].CGColor;
    self.viewPreview.layer.borderWidth = 1.0;
    
    // self.tblView.backgroundColor = [UIColor clearColor];
    self.tblView.contentInset = UIEdgeInsetsMake(10.0, 0.0, 60.0, 0.0);
    [self.tblView registerNib:[UINib nibWithNibName:@"TransPatientInfoCell" bundle:nil] forCellReuseIdentifier:@"TransPatientInfoCell"];
    [self.tblView registerNib:[UINib nibWithNibName:@"TransPhysicianInfoCell" bundle:nil] forCellReuseIdentifier:@"TransPhysicianInfoCell"];
    [self.tblView registerNib:[UINib nibWithNibName:@"TransConsultNotesHeadingCell" bundle:nil] forCellReuseIdentifier:@"TransConsultNotesHeadingCell"];
    [self.tblView registerNib:[UINib nibWithNibName:@"TransConsultNotesCell" bundle:nil] forCellReuseIdentifier:@"TransConsultNotesCell"];
    [self.tblView registerNib:[UINib nibWithNibName:@"TransNotesSignCell" bundle:nil] forCellReuseIdentifier:@"TransNotesSignCell"];
    
    [self loadTranscripts:^{
        [self reloadTable];
    }];
}

#pragma mark Actions
- (IBAction)backAction:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated: YES];
}

- (IBAction)previewAction:(id)sender {
    NSString *path = [self toPDF:@"temp.pdf"];
    PreviewView *preview = [PreviewView view];
    preview.url = path;
    [preview showInView:APP_DELEGATE.window];
    preview.printButtonblock = ^(UIButton *sender, PreviewView *view) {
        [self showPrinter:path];
    };
}

#pragma mark - Methods
- (void)loadTranscripts:(void (^) (void))completion {
    
    /// Checking for internet connection
    if (!IsInternetAvailable) {
        return;
    }
    
    if (!self.consultNotes) {
        self.consultNotes = [[NSMutableArray alloc] init];
    }
    
    SHOW_LOADER;
    NSString *consultantsAPIUrl = [NSString stringWithFormat:@"%@/%@", WEB_SERVICE_PATIENT_TRANSCRIPT, self.patient.PatientId];
    [[ApiHandler shared] sendHTTPRequest:APIRequestTypeGET url:consultantsAPIUrl params:nil withCompletion:^(id response, NSError *error) {
        HIDE_LOADER;
        if (error) {
            completion();
            return;
        }
        
        if ([response[@"status"] boolValue]) {
            
            self.patientInfo = response[@"Patient_info"];
            if (![self.patientInfo isKindOfClass:[NSDictionary class]]) {
                self.patientInfo = @{};
            }
            
            NSArray *noets = response[@"consultant_Notes"];
            if (noets && [noets isKindOfClass:[NSArray class]]) {
                [self.consultNotes removeAllObjects];
                for (NSDictionary *n in noets) {
                    ConsultNotes *note = [[ConsultNotes alloc] initWithDicionary:n];
                    [self.consultNotes addObject:note];
                }
            }
        
        }
        else {
            [self showAlertWithTitle:@"" message:response[@"message"] okButtonTitle:@"Ok"];
        }
            
        completion();
    }];
}

- (void)reloadTable {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tblView reloadData];
    });
}

- (void)proceedToSaveNotes:(NSString *)notes sign:(UIImage *)sign cell:(TransNotesSignCell *)cell {
    if ([notes isEmpty]) {
        [self showAlertWithTitle:@"" message:@"Please add a notes." okButtonTitle:@"Ok"];
        return;
    }
    
    if (!sign) {
        [self showAlertWithTitle:@"" message:@"Please sign to submit." okButtonTitle:@"Ok"];
        return;
    }
    
    NSDictionary *params = @{@"PatientID": self.patient.PatientId,
                             @"ConsultantID": APP_DELEGATE.currentUser.Id,
                             @"ConsultationNote": notes};
    
        
    SHOW_LOADER;
    [[ApiHandler shared] uploadImage:sign withName:@"Signature" atURL:WEB_SERVICE_TRANSCRIPT withParamas:params withProgressBlock:^(NSProgress *uploadProgress) {
        
    } andCompletionBlock:^(id response, NSError *error) {
        
        HIDE_LOADER;
        if (error) {
            [self showAlertWithTitle:@"" message:error.localizedDescription okButtonTitle:@"OK"];
            return;
        }
    
        if ([response[@"status"] boolValue]) {
            NSDictionary *patient_Information = response[@"patient_Information"];
            if ([patient_Information isKindOfClass:[NSDictionary class]]) {
                id consultation = patient_Information[@"consultation"];
                if ([consultation isKindOfClass:[NSDictionary class]]) {
                    ConsultNotes *n = [[ConsultNotes alloc] initWithDicionary:consultation];
                    [self.consultNotes addObject:n];
                    [self reloadTable];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        cell.tVNotes.text = @"";
                        [cell showHidePlaceholder];
                        [cell.signatureView erase];
                    });
                }
                else if ([consultation isKindOfClass:[NSArray class]] &&
                         [[consultation lastObject] isKindOfClass:[NSDictionary class]]) {
                    
                    ConsultNotes *n = [[ConsultNotes alloc] initWithDicionary:[consultation lastObject]];
                    [self.consultNotes addObject:n];
                    [self reloadTable];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        cell.tVNotes.text = @"";
                        [cell showHidePlaceholder];
                        [cell.signatureView erase];
                    });
                }
            }
        }
        
        [self showAlertWithTitle:@"" message:response[@"message"] okButtonTitle:@"OK"];
        
    }];
}

#pragma mark - Printer Methods
- (NSString *)toPDF:(NSString *)fileName {
    
    printingIsGoingOn = YES;

    CGPoint currentOffset = self.tblView.contentOffset;
    
    // Don't include scroll indicators in file
    self.tblView.showsVerticalScrollIndicator = NO;
    
    // Creates a mutable data object for updating with binary data, like a byte array
    NSMutableData *pdfData = [[NSMutableData alloc] init];
    
    UIGraphicsBeginPDFContextToData(pdfData, CGRectMake(0, 0, self.tblView.contentSize.width, self.tblView.contentSize.height), nil);
    UIGraphicsBeginPDFPage();
    [self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
    [self.tblView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
    [self.tblView.layer renderInContext:UIGraphicsGetCurrentContext()];

    CGFloat screensInTable = self.tblView.contentSize.height / self.tblView.height;

    for (int i = 1; i < screensInTable; i++) {
        CGPoint contentOffset = CGPointMake(0, (i * self.tblView.height) - 10);
        [self.tblView setContentOffset:contentOffset animated:NO];
        [self.tblView.layer renderInContext:UIGraphicsGetCurrentContext()];
    }

    UIGraphicsEndPDFContext();
    
    // Change the frame size to include all data
    /*CGRect originalFrame = self.tblView.frame;
    self.tblView.frame = CGRectMake(self.tblView.frame.origin.x, self.tblView.frame.origin.y, self.tblView.contentSize.width, self.tblView.contentSize.height);
    
    // Points the pdf converter to the mutable data object and to the UIView to be converted
    UIGraphicsBeginPDFContextToData(pdfData, self.tblView.bounds, nil);
    UIGraphicsBeginPDFPage();
    CGContextRef pdfContext = UIGraphicsGetCurrentContext();
    
    // Draws rect to the view and thus this is captured by UIGraphicsBeginPDFContextToData
    [self.tblView.layer renderInContext:pdfContext];
    
    // Remove PDF rendering context
    UIGraphicsEndPDFContext();*/
    
    // Retrieves the document directories from the iOS device
    NSArray *documentDirectories = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = documentDirectories[0];
    NSString *documentDirectoryFilename = [documentDirectory stringByAppendingPathComponent:fileName];
    
    // Instructs the mutable data object to write its context to a file on disk
    [pdfData writeToFile:documentDirectoryFilename atomically:YES];
    
    // Back to normal size
    // self.tblView.frame = originalFrame;

    // Put back the scroll indicator
    self.tblView.showsVerticalScrollIndicator = YES;
    self.tblView.contentOffset = currentOffset;
    printingIsGoingOn = NO;
    
    return documentDirectoryFilename;
    
}


- (void)showPrinter:(NSString *)path {
    UIPrintInteractionController *pc = [UIPrintInteractionController
                                        sharedPrintController];

    
    UIPrintInfo *printInfo = [UIPrintInfo printInfo];
    printInfo.outputType = UIPrintInfoOutputGeneral;
    printInfo.orientation = UIPrintInfoOrientationPortrait;
    printInfo.jobName = @"ROSe Consult Transcription";
    
    pc.printInfo = printInfo;
    pc.printingItem = [NSData dataWithContentsOfURL:[NSURL fileURLWithPath:path]];
    // You can use here image or any data type to print.
    
    
    UIPrintInteractionCompletionHandler completionHandler =
    ^(UIPrintInteractionController *printController, BOOL completed,
      NSError *error) {
        if(!completed && error){
            NSLog(@"Print failed - domain: %@ error code %ld", error.domain,
                  (long)error.code);
        }
    };
    
    [pc presentFromRect:CGRectMake(0, 0, 300, 300) inView:self.view animated:YES completionHandler:completionHandler];
}


#pragma mark  UITableView Delegate & Datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 5;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (section == 0) {
        // Patient Information
        return 1;
    }
    
    if (section == 1) {
        // Physician Information
        return 1;
    }
    
    if (section == 2) {
        // Consult Notes Heading
        return 1;
    }
    
    if (section == 3) {
        // Consult Notes
        return [self.consultNotes count];
    }
    
    // Notes and Sign
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return UITableViewAutomaticDimension;
    }
    else if (indexPath.section == 1) {
        return UITableViewAutomaticDimension;
    }
    else if (indexPath.section == 2) {
        return 51;
    }
    else if (indexPath.section == 3) {
        return UITableViewAutomaticDimension;
    }
    else {
        return 324;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0) {
        // Patient Information
        static NSString *cellId = @"TransPatientInfoCell";
        TransPatientInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
        cell.backgroundColor = [UIColor whiteColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.patient = self.patient;
        return cell;
    }
    else if (indexPath.section == 1) {
        // Physician Information
        static NSString *cellId = @"TransPhysicianInfoCell";
        TransPhysicianInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
        cell.backgroundColor = [UIColor whiteColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.patient = self.patient;
        return cell;
    }
    else if (indexPath.section == 2) {
        // Consult Notes Heading
        static NSString *cellId = @"TransConsultNotesHeadingCell";
        TransConsultNotesHeadingCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
        cell.backgroundColor = [UIColor whiteColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    else if (indexPath.section == 3) {
        // Consult Notes
        static NSString *cellId = @"TransConsultNotesCell";
        TransConsultNotesCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
        cell.backgroundColor = [UIColor whiteColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.consultNotes = self.consultNotes[indexPath.row];
        return cell;
    }
    else {
        // Notes and Sign
        static NSString *cellId = @"TransNotesSignCell";
        TransNotesSignCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
        cell.backgroundColor = [UIColor whiteColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.submitButtonblock = ^(UIButton * _Nonnull sender, TransNotesSignCell * _Nonnull cell) {
            UIImage *sign = [cell.signatureView signatureImage];
            NSString *notes = cell.tVNotes.text;
            [self proceedToSaveNotes:notes sign:sign cell:cell];
        };
        cell.hidden = printingIsGoingOn;
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

@end
