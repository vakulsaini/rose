//
//  ConsultsViewController.m
//  Rose
//
//  Created by Virender on 25/12/19.
//  Copyright © 2019 QualiteSoft. All rights reserved.
//

#import "ConsultsViewController.h"
#import "SearchBarView.h"
#import "ConsultUser.h"
#import "ConsultantCell.h"
#import "M13BadgeView.h"
#import "CallViewController.h"
#import "TabBarController.h"

@interface ConsultsViewController ()<SearchBarViewDelegate, CallViewControllerEventDelegate>
/**
 UITableView IBOutlet - Main table
 */
@property (nonatomic, weak) IBOutlet UITableView *tblView;
@property (nonatomic, weak) IBOutlet UILabel *lblNoRecords;
@property (nonatomic, strong) NSMutableArray <ConsultUser *> *tableUsers;
@property (nonatomic, strong) SearchBarView *searchBarView;
@property (nonatomic, strong) M13BadgeView *badgeView;
@property (nonatomic, weak) IBOutlet UIButton *btnBell;

@end

@implementation ConsultsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.tblView.backgroundColor = [UIColor clearColor];
    [self.tblView registerNib:[UINib nibWithNibName:@"ConsultantCell" bundle:nil] forCellReuseIdentifier:@"ConsultantCell"];
    self.tblView.contentInset = UIEdgeInsetsMake(0.0, 0.0, 80.0, 0.0);

    self.tableUsers = [[NSMutableArray alloc] init];
    
    [self applyFilter];
    [self reloadTable];
    [self setUpBadge];
    
    self.searchBarView = [SearchBarView view];
    self.searchBarView.delegate = self;
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    [self adjustBadgeFrame];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [APP_DELEGATE loadConsultants:^(NSError *error) {
        
        if (error && [APP_DELEGATE.consultUsers count] == 0) {
            [self showAlertWithTitle:@"" message:error.localizedDescription okButtonTitle:@"Ok"];
            return;
        }
        
        [self applyFilter];
        [self reloadTable];
    }];
    
    [APP_DELEGATE refreshUnreadNotifications:^{
        [self refreshBadgeValue];
    }];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
  
}

#pragma mark - Actions
- (IBAction)bellAction:(id)sender {
    [APP_DELEGATE goToNotificationsFrom:self];
}

- (IBAction)searchAction:(id)sender {
    [self.searchBarView showOnView:[sender superview] animated:YES];
}

- (IBAction)settingsAction:(id)sender {
    [APP_DELEGATE goToSettingsFrom:self];
}

- (IBAction)addContactAction:(id)sender {
    [APP_DELEGATE.tabBarController showAddContactPopup];
}

#pragma mark - Methods
- (void)setUpBadge {
    self.badgeView = [[M13BadgeView alloc] initWithFrame:CGRectMake(0, 0, 24.0, 24.0)];
    self.badgeView.font = [UIFont fontWithName:@"Helvetica Neue" size:14.0];
    self.badgeView.text = @"";
    self.badgeView.horizontalAlignment = M13BadgeViewHorizontalAlignmentRight;
    self.badgeView.verticalAlignment = M13BadgeViewVerticalAlignmentTop;
    [self.btnBell addSubview:self.badgeView];
    self.badgeView.hidden = YES;
}

- (void)refreshBadgeValue {
    dispatch_async(dispatch_get_main_queue(), ^{
        if ([APP_DELEGATE.unreadCount integerValue] == 0) {
            self.badgeView.text = @"";
            self.badgeView.hidden = YES;
        }
        else {
            self.badgeView.text = [NSString stringWithFormat:@"%@", APP_DELEGATE.unreadCount];
            self.badgeView.hidden = NO;
        }
        [self adjustBadgeFrame];
    });
}

- (void)adjustBadgeFrame {
    [self.badgeView autoSetBadgeFrame];
    
    CGRect frame = self.badgeView.frame;
    frame.origin.x -= 10.0;
    frame.origin.y += 10.0;
    self.badgeView.frame = frame;
}

- (void)applyFilter {
    [self.tableUsers removeAllObjects];
    NSString *text = self.searchBarView.textField.text;
    if (text.length > 0) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.fullName CONTAINS[cd] %@ || SELF.Phone CONTAINS[cd] %@", text, text];
        NSArray *logs = [APP_DELEGATE.consultUsers filteredArrayUsingPredicate:predicate];
        [self.tableUsers addObjectsFromArray:logs];
    }
    else {
        [self.tableUsers addObjectsFromArray:APP_DELEGATE.consultUsers];
    }
}

- (void)reloadTable {
    dispatch_async(dispatch_get_main_queue(), ^{
        if ([self.tableUsers count] > 0) {
            self.tblView.hidden = NO;
            self.lblNoRecords.hidden = YES;
        }
        else {
            self.tblView.hidden = YES;
            self.lblNoRecords.hidden = NO;
        }
        
        [self.tblView reloadData];
    });
}

- (void)enableConsultant:(ConsultUser *)user enable:(BOOL)isEnable {
    
    /// Checking for internet connection
    if (!IsInternetAvailable) {
        [self showAlertWithTitle:kNoInternetTitle message:kNoInternetMessage okButtonTitle:@"OK"];
        return;
    }
    
    NSDictionary *params = @{@"UserID": APP_DELEGATE.currentUser.Id,
                             @"EnableUserID": user.Id,
                             @"Type": isEnable ? @"1" : @"0"};
    
    [[ApiHandler shared] sendHTTPRequest:APIRequestTypePOST url:WEB_SERVICE_ENABLE_LIST params:params withCompletion:^(id response, NSError *error) {
        HIDE_LOADER;
        if (error) {
            [self showAlertWithTitle:@"" message:error.localizedDescription okButtonTitle:@"OK"];
            return;
        }
        
        if ([response[@"status"] boolValue]) {
            /*NSArray *users = response[@"User"];
            if ([users isKindOfClass:[NSArray class]]) {
                for (NSDictionary *u in users) {
                    if ([u[@"Id"] integerValue] == [user.Id integerValue]) {
                        [user setUpDictionary:u];
                        break;
                    }
                }
            }
            else {
                user.Enable = isEnable ? @"1" : @"0";
            }*/
            
            user.Enable = isEnable ? @"1" : @"0";
            [APP_DELEGATE enabledOnTopAndDisabledOnBottom:APP_DELEGATE.consultUsers];
            [self applyFilter];
            [self reloadTable];
        }
        else {
            [self showAlertWithTitle:@"" message:response[@"message"] okButtonTitle:@"OK"];
        }
    }];
}

#pragma mark  UITableView Delegate & Datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.tableUsers count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 100;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellId = @"ConsultantCell";
    ConsultantCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.user = self.tableUsers[indexPath.row];
    cell.phoneButtonblock = ^(UIButton * _Nonnull sender, ConsultantCell * _Nonnull cell) {
        
        if (!cell.user.isEnabled) {
            return;
        }
        
        [self call:cell.user];
        
        // Check if Consultant, Then he can call to every user whether it is enabled or disabled
        /*if (![APP_DELEGATE.currentUser isPhysician]) {
            [self call:cell.user];
        }
        else {
            // It is Physician and can call to enabled user only
            if (!cell.user.isEnabled) {
                return;
            }
            [self call:cell.user];
        }*/
    };
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (![APP_DELEGATE.currentUser isPhysician]) {
        ConsultUser *user = self.tableUsers[indexPath.row];
        NSString *msg = [NSString stringWithFormat:@"Are you sure you want to %@ %@?", [user isEnabled] ? @"disable" : @"enable", [user fullName]];
        [self showAlertWithTitle:@"" message:msg okButtonTitle:@"Yes" cancelButtonTitle:@"No" withCompletionBlock:^(NSString *title, UIAlertController *alert) {
            if ([title isEqualToString:@"Yes"]) {
                [self enableConsultant:user enable:![user isEnabled]];
            }
        }];
    }
}


#pragma mark - SearchBarViewDelegates
- (void)searchBarDidEditingChange:(SearchBarView *)view {
    [self applyFilter];
    [self reloadTable];
}

- (void)searchBarDidClose:(SearchBarView *)view {
    [view hide:YES];
    [view clearText];
    [self applyFilter];
    [self reloadTable];
}

#pragma mark - SINCallNotification

- (void)call:(ConsultUser *)user {
        
    // We can pass the extra information in headers
    // id<SINCall> call = [APP_DELEGATE.callClient callUserVideoWithId:[NSString stringWithFormat:@"%@", user.Id]];
    id<SINCall> call = [APP_DELEGATE createCallInstanceForUserId:[NSString stringWithFormat:@"%@", user.Id]];
    [APP_DELEGATE showCall:call from:APP_DELEGATE.window.rootViewController delegate:self];
    
    // Call the API for call logs - > Dial API
    [APP_DELEGATE dialCall:call.callId callerId:[NSString stringWithFormat:@"%@", APP_DELEGATE.currentUser.Id] receiverId:[NSString stringWithFormat:@"%@", user.Id] completion:^{}];
}

- (void)callEvent:(CallEventType)eventType controller:(CallViewController *)controller call:(id<SINCall>)call {
    
    if (eventType == kCallEventTypeProgress) {
        
    }
    else if (eventType == kCallEventTypeEstablished) {
        [self sinCallDidEstablish:call];
    }
    else if (eventType == kCallEventTypeEnd) {
        [self sinCallDidEnd:call];
    }
}

- (void)sinCallDidEnd:(id <SINCall>)call {
    NSLog(@"%ld", call.details.endCause);
    if (call.details.endCause == SINCallEndCauseCanceled) {
        // Call canceled by caller
        // Update it's log on server
        [APP_DELEGATE updateCallStatus:call.callId callStatus:@"decline" callDuration:nil completion:^{}];
        return;
    }
    
    NSDate *callEstablishedTime = call.details.establishedTime;
    NSDate *callEndTime   = call.details.endedTime;
    BOOL callEstablished = NO;
    NSTimeInterval interval = 0;
    if (callEstablishedTime && callEndTime) {
        interval = fabs([callEstablishedTime timeIntervalSinceDate:callEndTime]);
        if (interval > 0) {
            callEstablished = YES;
        }
    }
    
    if (callEstablished) {
        // Call is surely established because call duration is greater than 0 seconds.
        [APP_DELEGATE updateCallStatus:call.callId callStatus:@"RECEIVED" callDuration:@(interval) completion:^{}];
    }
    else {
        
        // Update it's log on server
        NSString *status = [APP_DELEGATE status:call];
        [APP_DELEGATE updateCallStatus:call.callId callStatus:status callDuration:nil completion:^{}];
    }
}

- (void)sinCallDidEstablish:(id <SINCall>)call {
    
}



@end
