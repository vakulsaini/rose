//
//  TranscriptionViewController.h
//  Rose
//
//  Created by Vakul Saini on 09/01/20.
//  Copyright © 2020 QualiteSoft. All rights reserved.
//

#import "RBaseViewController.h"
#import "Patient.h"

NS_ASSUME_NONNULL_BEGIN

@interface TranscriptionViewController : RBaseViewController
@property (nonatomic, strong) Patient *patient;
@end

NS_ASSUME_NONNULL_END
