//
//  ForgotPasswordViewController.m
//  Seera
//
//  Created by Vakul Saini on 17/11/18.
//  Copyright © 2018 TheTiger. All rights reserved.
//

#import "ForgotPasswordViewController.h"
#import "CustomTextField.h"
#import "CustomButton.h"

@interface ForgotPasswordViewController () <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet CustomTextField *tfEmail;
@property (weak, nonatomic) IBOutlet CustomButton *btnSubmit;
@property (weak, nonatomic) IBOutlet UIImageView *iVLogo;

@end

@implementation ForgotPasswordViewController

#pragma mark - LifeCycle
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setUpUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions
- (IBAction)submitAction:(id)sender {
    BOOL success = [self validateFields];
    if (!success) {
        return;
    }
    
    /// Checking for internet connection
    if (!IsInternetAvailable) {
        [self showAlertWithTitle:kNoInternetTitle message:kNoInternetMessage okButtonTitle:@"OK"];
        return;
    }
    
    /// Proceed
    [self proceedToResetPassword];
}

- (IBAction)backAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Methods
- (void)setUpUI {
    [self.tfEmail.superview makeRoundWithRadius:2.0];
    [self.btnSubmit makeRoundWithRadius:2.0];
}

- (void)proceedToResetPassword {
    
    SHOW_LOADER;
    NSDictionary *params = @{@"Email": [self.tfEmail.text lowercaseString],
                             @"_method": @"PUT"};
    
    [[ApiHandler shared] sendHTTPRequest:APIRequestTypePOST url:WEB_SERVICE_FORGOT_PASSWORD params:params withCompletion:^(id response, NSError *error) {
        HIDE_LOADER;
        if (error) {
            [self showAlertWithTitle:@"" message:error.localizedDescription okButtonTitle:@"OK"];
            return;
        }
        
        if ([response[@"status"] boolValue]) {
            [self showAlertWithTitle:@"" message:response[@"message"] okButtonTitle:@"OK" withCompletionBlock:^(NSString *title, UIAlertController *alert) {
                if ([title isEqualToString:@"OK"]) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self backAction:nil];
                    });
                }
            }];
        }
        else {
            [self showAlertWithTitle:@"" message:response[@"message"] okButtonTitle:@"OK"];
        }
    }];
}

- (BOOL)validateFields {

    NSString *email = self.tfEmail.text;
    if ([email isEmpty]) {
        [self showAlertWithTitle:@"" message:@"Please enter your email address." okButtonTitle:@"OK"];
        return NO;
    }
    
    if (!email.isValidEmail) {
        [self showAlertWithTitle:@"" message:@"Please enter a valid email address." okButtonTitle:@"OK"];
        return NO;
    }
    
    return YES;
}

#pragma mark - TextField Delegates
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

@end
