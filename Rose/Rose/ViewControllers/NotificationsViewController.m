//
//  NotificationsViewController.m
//  Rose
//
//  Created by Vakul Saini on 06/02/20.
//  Copyright © 2020 QualiteSoft. All rights reserved.
//

#import "NotificationsViewController.h"

#import "NotificationsViewController.h"
#import "NotificationCell.h"

@interface NotificationsViewController ()<UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tblView;
@property (nonatomic, weak) IBOutlet UILabel *lblNoRecords;

@property (nonatomic, strong) NSMutableArray <Notification *>*notifications;

@end

@implementation NotificationsViewController

#pragma mark - LifeCycle
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.tblView.backgroundColor = [UIColor clearColor];
    self.tblView.rowHeight = UITableViewAutomaticDimension;
    self.tblView.estimatedRowHeight = 100;
    [self.tblView registerNib:[UINib nibWithNibName:@"NotificationCell" bundle:nil] forCellReuseIdentifier:@"NotificationCell"];
    self.tblView.contentInset = UIEdgeInsetsMake(-20.0, 0.0, 10.0, 0.0);
    
    self.notifications = [[NSMutableArray alloc] init];
    
    /// Checking for internet connection
    if (!IsInternetAvailable) {
        [self showAlertWithTitle:kNoInternetTitle message:kNoInternetMessage okButtonTitle:@"OK"];
        return;
    }
    
    [self loadData:^{
        [self reloadTable];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.tblView reloadData];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleDefault;
}

#pragma mark - Methods
- (void)loadData:(void (^) (void))completion {
    SHOW_LOADER;
    
    NSString *notificationAPIUrl = [NSString stringWithFormat:@"%@/%@", WEB_SERVICE_ALERT, APP_DELEGATE.currentUser.Id];
    [[ApiHandler shared] sendHTTPRequest:APIRequestTypeGET url:notificationAPIUrl params:nil withCompletion:^(id response, NSError *error) {
        HIDE_LOADER;
        if (error) {
            [self showAlertWithTitle:@"" message:error.localizedDescription okButtonTitle:@"Ok"];
            completion();
            return;
        }
        
        NSArray *nots = response[@"Notification"];
        if (nots && [nots isKindOfClass:[NSArray class]]) {
            [self.notifications removeAllObjects];
            for (NSDictionary *n in nots) {
                Notification *notificaion = [[Notification alloc] initWithInfo:n];
                [self.notifications addObject:notificaion];
            }
            
            // Mark all notifications as read once they loaded
            [self markAllAsRead:^{
                
            }];
        }
            
        completion();
    }];
}

- (void)markAllAsRead:(void (^) (void))completion {
    NSString *notificationAPIUrl = [NSString stringWithFormat:@"%@/%@", WEB_SERVICE_ALERT, APP_DELEGATE.currentUser.Id];
    [[ApiHandler shared] sendHTTPRequest:APIRequestTypePUT url:notificationAPIUrl params:nil withCompletion:^(id response, NSError *error) {
        completion();
    }];
}

- (void)reloadTable {
    dispatch_async(dispatch_get_main_queue(), ^{
        if ([self.notifications count] > 0) {
            self.tblView.hidden = NO;
            self.lblNoRecords.hidden = YES;
        }
        else {
            self.tblView.hidden = YES;
            self.lblNoRecords.hidden = NO;
        }
        
        [self.tblView reloadData];
    });
}

- (void)markNotificationAsRead:(Notification *)notification {
    
//    if ([notification.status integerValue] == 0) {
//        /// Already read
//        return;
//    }
//
//    notification.status = @(0);
//    NSDictionary *params = @{@"user_id": APP_DELEGATE.currentUser.Id,
//                             @"notification_id": notification.notification_id};
//    [[ApiHandler shared] sendHTTPRequest:APIRequestTypePOST url:WEB_SERVICE_CHANGE_NOTIFICATION_STATUS params:params withCompletion:^(id response, NSError *error) { }];
//
//    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"notification_id == %@", notification.notification_id];
//    NSArray *result = [APP_DELEGATE.unreadNotifications filteredArrayUsingPredicate:predicate];
//    if ([result count] > 0) {
//        [APP_DELEGATE.unreadNotifications removeObject:[result firstObject]];
//    }
}

#pragma mark - Actions
- (IBAction)backAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - TableView Delegates & DataSources
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.notifications count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellId = @"NotificationCell";
    NotificationCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.notification = self.notifications[indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    NewsFeedDetailViewController *newsfeedDetailVC = [NewsFeedDetailViewController instance];
//    newsfeedDetailVC.title = @"Notification Detail";
//    [self.navigationController pushViewController:newsfeedDetailVC animated:YES];
//    [self markNotificationAsRead:self.notifications[indexPath.row]];
}

@end
