//
//  LoginViewController.m
//  Seera
//
//  Created by Vakul Saini on 16/11/18.
//  Copyright © 2018 TheTiger. All rights reserved.
//

#import "LoginViewController.h"
#import "ForgotPasswordViewController.h"
#import "HomeViewController.h"
#import "SignUpViewController.h"
#import "TermsView.h"
#import "CustomHeadingLabel.h"
#import "CustomTextField.h"
#import "CustomButton.h"

@interface LoginViewController () <UITextFieldDelegate> {
    TermsView *termsView;
}

@property (weak, nonatomic) IBOutlet CustomTextField *tfEmail;
@property (weak, nonatomic) IBOutlet CustomTextField *tfPassword;
@property (weak, nonatomic) IBOutlet CustomButton *btnLogin;
@property (weak, nonatomic) IBOutlet UIImageView *iVLogo;
@property (weak, nonatomic) IBOutlet CustomHeadingLabel *lbll;

@end

@implementation LoginViewController

#pragma mark - LifeCycle
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setUpUI];
    termsView = [TermsView view];
    termsView.text = @"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?";
    
 
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions
- (IBAction)loginAction:(id)sender {
    BOOL success = [self validateFields];
    if (!success) {
        return;
    }
    
    /// Checking for internet connection
    if (!IsInternetAvailable) {
        [self showAlertWithTitle:kNoInternetTitle message:kNoInternetMessage okButtonTitle:@"OK"];
        return;
    }
    
    /// Proceed
    [self proceedToLogin];

}

- (IBAction)forgotPasswordAction:(id)sender {
    ForgotPasswordViewController *forgotPasswordVC = [ForgotPasswordViewController instance];
    [self.navigationController pushViewController:forgotPasswordVC animated:YES];
}

- (IBAction)signUpAction:(id)sender {
    SignUpViewController *signUpVC = [SignUpViewController instance];
    [self.navigationController pushViewController:signUpVC animated:YES];
}

- (IBAction)eyeAction:(id)sender {
    self.tfPassword.secureTextEntry = !self.tfPassword.secureTextEntry;
}

#pragma mark - Methods
- (void)setUpUI {
    [self.tfEmail.superview makeRoundWithRadius:2.0];
    [self.tfPassword.superview makeRoundWithRadius:2.0];
    [self.btnLogin makeRoundWithRadius:2.0];
}

- (void)proceedToLogin {

//    NSString *deviceToken = [APP_DELEGATE getDeviceToken];
//    if (!deviceToken) {
//        deviceToken = @"xyz";
//    }
    
    NSString *username = self.tfEmail.text;
    // NSString *deviceModel = [NSString stringWithFormat:@"%@ %@", [VSDeviceInfo instance].deviceName, [VSDeviceInfo instance].osVersion];
    
    NSDictionary *params = @{@"Email": username.lowercaseString,
                             @"Password": self.tfPassword.text};
    
    SHOW_LOADER;
    [[ApiHandler shared] sendHTTPRequest:APIRequestTypePOST url:WEB_SERVICE_LOGIN params:params withCompletion:^(id response, NSError *error) {
        HIDE_LOADER;
        if (error) {
            [self showAlertWithTitle:@"" message:error.localizedDescription okButtonTitle:@"OK"];
            return;
        }
        
        if ([response[@"status"] boolValue]) {
            
            RUser *user = [[RUser alloc] init];
            [user setUpDictionary:response[@"user_data"]];
            
            APP_DELEGATE.currentUser = user;
            [APP_DELEGATE saveUserLocally];
            [APP_DELEGATE showMainScreen:YES];
            [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_USER_DID_LOGIN object:nil];
        }
        else {
            [self showAlertWithTitle:@"" message:response[@"message"] okButtonTitle:@"OK"];
        }
    }];
}

- (BOOL)validateFields {
    
    NSString *email = self.tfEmail.text;
    if ([email isEmpty]) {
        [self showAlertWithTitle:@"" message:@"Please enter your email address." okButtonTitle:@"OK"];
        return NO;
    }
    
    if (!email.isValidEmail) {
        [self showAlertWithTitle:@"" message:@"Please enter a valid email address." okButtonTitle:@"OK"];
        return NO;
    }
    
    NSString *password = self.tfPassword.text;
    if (password.isEmpty) {
        [self showAlertWithTitle:@"" message:@"Please enter your password." okButtonTitle:@"OK"];
        return NO;
    }
    
    return YES;
}

#pragma mark - TextField Delegates
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

@end
