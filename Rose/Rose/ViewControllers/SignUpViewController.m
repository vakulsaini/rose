//
//  SignUpViewController.m
//  Seera
//
//  Created by Vakul Saini on 28/11/18.
//  Copyright © 2018 TheTiger. All rights reserved.
//

#import "SignUpViewController.h"
#import "HomeViewController.h"
#import "TermsView.h"
#import "CustomTextField.h"
#import "CustomButton.h"
#import "DatePickerView.h"
#import "VSImagePicker.h"
#import "PreviewView.h"

#define kImageRadioButtonUnselected [UIImage imageNamed: @"unchecked"]
#define kImageRadioButtonSelected [UIImage imageNamed: @"checked"]
#define kImageCheckboxUnselected [UIImage imageNamed: @"terms_unchecked"]
#define kImageCheckboxSelected [UIImage imageNamed: @"terms_checked"]

@interface SignUpViewController () <UITextFieldDelegate, DatePickerViewDelegate> {
    TermsView *termsView;
    DatePickerView *datePickerView;
    UserType selectedUserType;
    UIImage *selectedPic;
    
    BOOL isTermsAndConditionSelected;
    BOOL isPhysicianSelected;
}

@property (weak, nonatomic) IBOutlet CustomTextField *tfFirstName;
@property (weak, nonatomic) IBOutlet CustomTextField *tfLastName;
@property (weak, nonatomic) IBOutlet CustomTextField *tfEmail;
@property (weak, nonatomic) IBOutlet CustomTextField *tfPhone;
@property (weak, nonatomic) IBOutlet CustomTextField *tfDOB;
@property (weak, nonatomic) IBOutlet CustomTextField *tfCity;
@property (weak, nonatomic) IBOutlet CustomTextField *tfLocation;
@property (weak, nonatomic) IBOutlet CustomTextField *tfSpeciality;
@property (weak, nonatomic) IBOutlet CustomTextField *tfMSPNumber;
@property (weak, nonatomic) IBOutlet CustomTextField *tfPassword;
@property (weak, nonatomic) IBOutlet CustomTextField *tfAccessCode;
@property (weak, nonatomic) IBOutlet CustomButton *btnSignUp;
@property (weak, nonatomic) IBOutlet UIImageView *iVLogo;
@property (nonatomic, strong) IBOutlet UIButton *btnPhysician;
@property (nonatomic, strong) IBOutlet UIButton *btnConsultant;
@property (nonatomic, strong) IBOutlet UIButton *btnTerms;
@property (nonatomic, weak) IBOutlet UIButton *btnDOB;
@property (nonatomic, strong) VSImagePicker *imagePicker;

@end

@implementation SignUpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setUpUI];
    [self setUpDelegate];
        
    datePickerView = [DatePickerView view];
    datePickerView.delegate = self;
    [self selectAction:self.btnPhysician];
    
    self.imagePicker = [[VSImagePicker alloc] init];
    [self.iVLogo.superview makeRound];
 }

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions
- (IBAction)loginAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)signUpAction:(id)sender {
    BOOL success = [self validateFields];
    if (!success) {
        return;
    }
    
    /// Checking for internet connection
    if (!IsInternetAvailable) {
        [self showAlertWithTitle:kNoInternetTitle message:kNoInternetMessage okButtonTitle:@"OK"];
        return;
    }
    
    /// Proceed
    [self proceedToRegister];
}

- (IBAction)selectAction:(UIButton *)sender {

    // (sender == self.btnPhysician && ![self isPhysicianSelected])
    if (sender == self.btnPhysician) {
        [self.btnPhysician setImage:kImageRadioButtonSelected forState:UIControlStateNormal];
        [self.btnConsultant setImage:kImageRadioButtonUnselected forState:UIControlStateNormal];
        selectedUserType = UserTypePhysician;
    }
    
    // (sender == self.btnConsultant && [self isPhysicianSelected])
    else if (sender == self.btnConsultant) {
        selectedUserType = UserTypeConsultant;
        [self.btnConsultant setImage:kImageRadioButtonSelected forState:UIControlStateNormal];
        [self.btnPhysician setImage:kImageRadioButtonUnselected forState:UIControlStateNormal];
    }
    
    /*if (sender.tag == 1 && !sender.isSelected) {
        self.btnPhysician.selected = YES;
        self.btnConsultant.selected = NO;
        selectedUserType = UserTypePhysician;
    }
    else if (sender.tag == 2 && !sender.isSelected){
        self.btnPhysician.selected = NO;
        self.btnConsultant.selected = YES;
        selectedUserType = UserTypeConsultant;
    }*/
 }

- (IBAction)termsAction:(UIButton *)sender {
    
    if ([self isTermsAccepted]) {
        [self.btnTerms setImage:kImageCheckboxUnselected forState:UIControlStateNormal];
    }
    else {
        [self.btnTerms setImage:kImageCheckboxSelected forState:UIControlStateNormal];
    }
    isTermsAndConditionSelected = !isTermsAndConditionSelected;
    
    /*if ([self isTermsAccepted]) {
        [self.btnTerms setImage:kImageCheckboxUnselected forState:UIControlStateNormal];
    }
    else {
        [self.btnTerms setImage:kImageCheckboxSelected forState:UIControlStateNormal];
    }*/
    
    /*if (sender.selected) {
        sender.selected = NO;
     }
    else {
        sender.selected = YES;
    }*/
}

- (IBAction)openTermsConditionsAction:(UIButton *)sender {
   // NSString *path = [[NSBundle mainBundle] pathForResource:@"PrivacyPolicy" ofType:@"pdf"];
    NSString *path = @"http://99.79.23.145/uploads/SevasHealthIncPrivacyPolicy.pdf";
    PreviewView *preview = [PreviewView view];
    preview.url = path;
    preview.viewPrintOption.hidden = YES;
    preview.lblTitle.text = @"Privacy Policy";
    [preview showInView:APP_DELEGATE.window];
}

- (IBAction)dobAction:(id)sender {
    
    [self resignAll];
    
    // Before 18 years old
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:[NSDate date]];
    components.year -= 18;
    NSDate *date = [[NSCalendar currentCalendar] dateFromComponents:components];
    datePickerView.datePicker.maximumDate = date;
    [datePickerView showOnView:self.view animated:YES];
 }

- (IBAction)choosePicAction:(id)sender {
    [self.imagePicker showInController:self withCompletionBlock:^(UIImage *image) {
        self->selectedPic = image;
        self.iVLogo.image = image;
    }];
}

 
#pragma mark - Methods
- (void)setUpUI {
    [self.tfFirstName.superview makeRoundWithRadius:2.0];
    [self.tfLastName.superview makeRoundWithRadius:2.0];
    [self.tfEmail.superview makeRoundWithRadius:2.0];
    [self.tfPhone.superview makeRoundWithRadius:2.0];
    [self.tfDOB.superview makeRoundWithRadius:2.0];
    [self.tfCity.superview makeRoundWithRadius:2.0];
    [self.tfLocation.superview makeRoundWithRadius:2.0];
    [self.tfSpeciality.superview makeRoundWithRadius:2.0];
    [self.tfMSPNumber.superview makeRoundWithRadius:2.0];
    [self.tfPassword.superview makeRoundWithRadius:2.0];
    [self.tfAccessCode.superview makeRoundWithRadius:2.0];
    [self.btnSignUp makeRoundWithRadius:2.0];
}

- (void)setUpDelegate {
    self.tfFirstName.delegate = self;
    self.tfLastName.delegate = self;
    self.tfEmail.delegate = self;
    self.tfPhone.delegate = self;
    self.tfCity.delegate = self;
    self.tfLocation.delegate = self;
    self.tfSpeciality.delegate = self;
    self.tfMSPNumber.delegate = self;
    self.tfPassword.delegate = self;
    self.tfAccessCode.delegate = self;
}

- (void)proceedToRegister {
    
//    NSString *deviceToken = [APP_DELEGATE getDeviceToken];
//    if (!deviceToken) {
//        deviceToken = @"xyz";
//    }
 
    // NSString *deviceModel = [NSString stringWithFormat:@"%@ %@", [VSDeviceInfo instance].deviceName, [VSDeviceInfo instance].osVersion];
    
    // @"DOB": self.tfDOB.text,
    NSDictionary *params = @{@"FirstName": self.tfFirstName.text.capitalizedString,
                             @"LastName": self.tfLastName.text.capitalizedString,
                             @"Email": self.tfEmail.text.lowercaseString,
                             @"Password": self.tfPassword.text,
                             @"User_type": @(selectedUserType),
                             @"Phone": self.tfPhone.text,
                             @"City": self.tfCity.text,
                             @"Location": self.tfLocation.text,
                             @"Speciality": self.tfSpeciality.text,
                             @"MSPNumber": self.tfMSPNumber.text,
                             @"AccessCode": self.tfAccessCode.text,
                             @"Latitude": @0,
                             @"Longitude": @0};
    
    SHOW_LOADER;
    [[ApiHandler shared] uploadImage:selectedPic withName:@"ProfilePic" atURL:WEB_SERVICE_NEW_SIGNUP withParamas:params withProgressBlock:^(NSProgress *uploadProgress) {
        
    } andCompletionBlock:^(id response, NSError *error) {
        
        HIDE_LOADER;
        if (error) {
            [self showAlertWithTitle:@"" message:error.localizedDescription okButtonTitle:@"OK"];
            return;
        }
        
        if ([response[@"status"] boolValue]) {
            
            RUser *user = [[RUser alloc] init];
            [user setUpDictionary:response[@"user_data"]];
            
            APP_DELEGATE.currentUser = user;
            [APP_DELEGATE saveUserLocally];
            [APP_DELEGATE showMainScreen:YES];
            [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_USER_DID_LOGIN object:nil];
        }
        else {
            [self showAlertWithTitle:@"" message:response[@"message"] okButtonTitle:@"OK"];
        }
        
    }];
}

- (void)resignAll {
    [self.tfFirstName resignFirstResponder];
    [self.tfLastName resignFirstResponder];
    [self.tfEmail resignFirstResponder];
    [self.tfPhone resignFirstResponder];
    [self.tfDOB resignFirstResponder];
    [self.tfCity resignFirstResponder];
    [self.tfLocation resignFirstResponder];
    [self.tfSpeciality resignFirstResponder];
    [self.tfMSPNumber resignFirstResponder];
    [self.tfPassword resignFirstResponder];
    [self.tfAccessCode resignFirstResponder];
}

- (BOOL)isPhysicianSelected {
    return [self.btnPhysician imageForState:UIControlStateNormal] == kImageRadioButtonSelected;
}

- (BOOL)isTermsAccepted {
    return isTermsAndConditionSelected;
    //return [self.btnTerms imageForState:UIControlStateNormal] == kImageCheckboxSelected;
}

- (BOOL)validateFields {
    
    NSString *fName = self.tfFirstName.text;
    if (fName.isEmpty) {
        [self showAlertWithTitle:@"" message:@"Please enter your first name." okButtonTitle:@"OK"];
        return NO;
    }
    
    if (![fName isValidName]) {
        [self showAlertWithTitle:@"" message:@"Please enter a valid first name." okButtonTitle:@"OK"];
        return NO;
    }
    
    NSString *lName = self.tfLastName.text;
    if (lName.isEmpty) {
        [self showAlertWithTitle:@"" message:@"Please enter your last name." okButtonTitle:@"OK"];
        return NO;
    }
    
    if (![lName isValidName]) {
        [self showAlertWithTitle:@"" message:@"Please enter a valid last name." okButtonTitle:@"OK"];
        return NO;
    }
    
    NSString *email = self.tfEmail.text;
    if ([email isEmpty]) {
        [self showAlertWithTitle:@"" message:@"Please enter your email address." okButtonTitle:@"OK"];
        return NO;
    }
    
    if (!email.isValidEmail) {
        [self showAlertWithTitle:@"" message:@"Please enter a valid email address." okButtonTitle:@"OK"];
        return NO;
    }

    
    NSString *phone = self.tfPhone.text;
    if (phone.isEmpty || phone.length != 10) {
        [self showAlertWithTitle:@"" message:@"Please enter 10 digit valid phone number." okButtonTitle:@"OK"];
        return NO;
    }
    
    /*NSString *dob = self.tfDOB.text;
    if (dob.isEmpty) {
        [self showAlertWithTitle:@"" message:@"Please choose your date of birth." okButtonTitle:@"OK"];
        return NO;
    }*/
    
    NSString *city = self.tfCity.text;
    if (city.isEmpty) {
        [self showAlertWithTitle:@"" message:@"Please enter your city." okButtonTitle:@"OK"];
        return NO;
    }
    
    NSString *location = self.tfLocation.text;
    if (location.isEmpty) {
        [self showAlertWithTitle:@"" message:@"Please enter your location." okButtonTitle:@"OK"];
        return NO;
    }
    
    NSString *speciality = self.tfSpeciality.text;
    if (speciality.isEmpty) {
        [self showAlertWithTitle:@"" message:@"Please enter your speciality." okButtonTitle:@"OK"];
        return NO;
    }
    
    NSString *mspNumber = self.tfMSPNumber.text;
    if (mspNumber.isEmpty || mspNumber.length != 5) {
        [self showAlertWithTitle:@"" message:@"Please enter 5 digit valid MSP number." okButtonTitle:@"OK"];
        return NO;
    }
    
    NSString *password = self.tfPassword.text;
    if (password.isEmpty) {
        [self showAlertWithTitle:@"" message:@"Please enter your password." okButtonTitle:@"OK"];
        return NO;
    }
    
    NSString *accessCode = self.tfAccessCode.text;
    if (accessCode.isEmpty) {
        [self showAlertWithTitle:@"" message:@"Please enter access code." okButtonTitle:@"OK"];
        return NO;
    }
    
    if (![self isTermsAccepted]) {
        [self showAlertWithTitle:@"" message:@"Please accept the Terms and Conditions." okButtonTitle:@"OK"];
        return NO;
    }
    
    return YES;
}

#pragma mark - TextField Delegates
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if (textField == self.tfMSPNumber) {
        return !([newString length] > 5);
    }
    else if (textField == self.tfPhone) {
        return !([newString length] > 10);
    }
    else {
        return YES;
    }
}

#pragma mark:- DatePicker Delegates
- (void)datePickerDidPickDate:(DatePickerView *)view date:(NSDate *)date {
    APP_DELEGATE.dateFormatter.dateFormat = @"MM/dd/yyyy";
    self.tfDOB.text = [APP_DELEGATE.dateFormatter stringFromDate:date];
}

- (void)datePickerDidCancel:(DatePickerView *)view {
    
}

@end
