//
//  EditProfileViewController.m
//  Rose
//
//  Created by Vakul Saini on 07/01/20.
//  Copyright © 2020 QualiteSoft. All rights reserved.
//

#import "EditProfileViewController.h"
#import "CustomTextField.h"
#import "CustomButton.h"
#import "VSImagePicker.h"

@interface EditProfileViewController () {
    UIImage *selectedPic;
}
@property (weak, nonatomic) IBOutlet CustomTextField *tfFirstName;
@property (weak, nonatomic) IBOutlet CustomTextField *tfLastName;
@property (weak, nonatomic) IBOutlet CustomTextField *tfEmail;
@property (weak, nonatomic) IBOutlet CustomTextField *tfPhone;
@property (weak, nonatomic) IBOutlet CustomTextField *tfDOB;
@property (weak, nonatomic) IBOutlet CustomTextField *tfCity;
@property (weak, nonatomic) IBOutlet CustomTextField *tfLocation;
@property (weak, nonatomic) IBOutlet CustomTextField *tfSpeciality;
@property (weak, nonatomic) IBOutlet CustomTextField *tfMSPNumber;
@property (weak, nonatomic) IBOutlet CustomButton *btnUpdate;
@property (weak, nonatomic) IBOutlet UIImageView *iVLogo;
@property (nonatomic, weak) IBOutlet UIButton *btnDOB;
@property (nonatomic, strong) VSImagePicker *imagePicker;

@end

@implementation EditProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setUpUI];
    [self bindData];
   
    [self.iVLogo.superview makeRound];
    NSURL *imageUrl = [NSURL URLWithString:APP_DELEGATE.currentUser.ProfilePic];
    [self.iVLogo sd_setImageWithURL:imageUrl placeholderImage:kUserProfilePlaceholder];
    
    self.imagePicker = [[VSImagePicker alloc] init];
}

#pragma mark Actions
- (IBAction)backAction:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated: YES];
}

- (IBAction)updateAction:(UIButton *)sender {
    BOOL success = [self validateFields];
    if (!success) {
        return;
    }
    
    /// Checking for internet connection
    if (!IsInternetAvailable) {
        [self showAlertWithTitle:kNoInternetTitle message:kNoInternetMessage okButtonTitle:@"OK"];
        return;
    }
    
    /// Proceed
    [self proceedToUpdate];
}

- (IBAction)choosePicAction:(id)sender {
    [self.imagePicker showInController:self withCompletionBlock:^(UIImage *image) {
        self->selectedPic = image;
        self.iVLogo.image = image;
    }];
}

#pragma mark - Methods
- (void)setUpUI {
    [self.tfFirstName.superview makeRoundWithRadius:2.0];
    [self.tfLastName.superview makeRoundWithRadius:2.0];
    [self.tfEmail.superview makeRoundWithRadius:2.0];
    [self.tfPhone.superview makeRoundWithRadius:2.0];
    [self.tfDOB.superview makeRoundWithRadius:2.0];
    [self.tfCity.superview makeRoundWithRadius:2.0];
    [self.tfLocation.superview makeRoundWithRadius:2.0];
    [self.tfSpeciality.superview makeRoundWithRadius:2.0];
    [self.tfMSPNumber.superview makeRoundWithRadius:2.0];
    [self.btnUpdate makeRoundWithRadius:2.0];
    
    // Disable color
    self.tfEmail.textColor = [UIColor lightGrayColor];
    self.tfPhone.textColor = [UIColor lightGrayColor];
    self.tfDOB.textColor = [UIColor lightGrayColor];
}

- (void)bindData {
    self.tfFirstName.text = APP_DELEGATE.currentUser.FirstName;
    self.tfLastName.text = APP_DELEGATE.currentUser.LastName;
    self.tfEmail.text = APP_DELEGATE.currentUser.Email;
    self.tfPhone.text = APP_DELEGATE.currentUser.Phone;
    self.tfDOB.text = APP_DELEGATE.currentUser.DOB;
    self.tfCity.text = APP_DELEGATE.currentUser.City;
    self.tfLocation.text = APP_DELEGATE.currentUser.Location;
    self.tfSpeciality.text = APP_DELEGATE.currentUser.Speciality;
    self.tfMSPNumber.text = APP_DELEGATE.currentUser.MSPNumber;
}

- (void)proceedToUpdate {
    
//    NSString *deviceToken = [APP_DELEGATE getDeviceToken];
//    if (!deviceToken) {
//        deviceToken = @"xyz";
//    }
 
    // NSString *deviceModel = [NSString stringWithFormat:@"%@ %@", [VSDeviceInfo instance].deviceName, [VSDeviceInfo instance].osVersion];
 
    NSDictionary *params = @{@"FirstName": self.tfFirstName.text.capitalizedString,
                             @"LastName": self.tfLastName.text.capitalizedString,
                             @"City": self.tfCity.text,
                             @"Location": self.tfLocation.text,
                             @"Speciality": self.tfSpeciality.text,
                             @"MSPNumber": self.tfMSPNumber.text,
                             @"_method": @"PUT"
    };
    
    
    NSString *profileAPIUrl = [NSString stringWithFormat:@"%@/%@", WEB_SERVICE_NEW_SIGNUP, APP_DELEGATE.currentUser.Id];
    
    SHOW_LOADER;
    [[ApiHandler shared] uploadImage:selectedPic withName:@"ProfilePic" atURL:profileAPIUrl withParamas:params withProgressBlock:^(NSProgress *uploadProgress) {
        
    } andCompletionBlock:^(id response, NSError *error) {
        
        HIDE_LOADER;
        if (error) {
            [self showAlertWithTitle:@"" message:error.localizedDescription okButtonTitle:@"OK"];
            return;
        }
        
        if ([response[@"status"] boolValue]) {
            [APP_DELEGATE.currentUser setUpDictionary:response[@"user_data"]];
            [APP_DELEGATE saveUserLocally];
        }
        
        [self showAlertWithTitle:@"" message:response[@"message"] okButtonTitle:@"OK"];
        
    }];
}

- (void)resignAll {
    [self.tfFirstName resignFirstResponder];
    [self.tfLastName resignFirstResponder];
    [self.tfEmail resignFirstResponder];
    [self.tfPhone resignFirstResponder];
    [self.tfDOB resignFirstResponder];
    [self.tfCity resignFirstResponder];
    [self.tfLocation resignFirstResponder];
    [self.tfSpeciality resignFirstResponder];
    [self.tfMSPNumber resignFirstResponder];
}

- (BOOL)validateFields {
    
    NSString *fName = self.tfFirstName.text;
    if (fName.isEmpty) {
        [self showAlertWithTitle:@"" message:@"Please enter your first name." okButtonTitle:@"OK"];
        return NO;
    }
    
    if (![fName isValidName]) {
        [self showAlertWithTitle:@"" message:@"Please enter a valid first name." okButtonTitle:@"OK"];
        return NO;
    }
    
    NSString *lName = self.tfLastName.text;
    if (lName.isEmpty) {
        [self showAlertWithTitle:@"" message:@"Please enter your last name." okButtonTitle:@"OK"];
        return NO;
    }
    
    if (![lName isValidName]) {
        [self showAlertWithTitle:@"" message:@"Please enter a valid last name." okButtonTitle:@"OK"];
        return NO;
    }
    
    NSString *city = self.tfCity.text;
    if (city.isEmpty) {
        [self showAlertWithTitle:@"" message:@"Please enter your city." okButtonTitle:@"OK"];
        return NO;
    }
    
    NSString *location = self.tfLocation.text;
    if (location.isEmpty) {
        [self showAlertWithTitle:@"" message:@"Please enter your location." okButtonTitle:@"OK"];
        return NO;
    }
    
    NSString *speciality = self.tfSpeciality.text;
    if (speciality.isEmpty) {
        [self showAlertWithTitle:@"" message:@"Please enter your speciality." okButtonTitle:@"OK"];
        return NO;
    }
    
    NSString *mspNumber = self.tfMSPNumber.text;
    if (mspNumber.isEmpty || mspNumber.length != 5) {
        [self showAlertWithTitle:@"" message:@"Please enter 5 digit valid MSP number." okButtonTitle:@"OK"];
        return NO;
    }
    
    return YES;
}

#pragma mark - TextField Delegates
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if (textField == self.tfMSPNumber) {
        return !([newString length] > 5);
    }
    else if (textField == self.tfPhone) {
        return !([newString length] > 10);
    }
    else {
        return YES;
    }
}

@end
