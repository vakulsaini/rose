//
//  NotificationsViewController.h
//  Rose
//
//  Created by Vakul Saini on 06/02/20.
//  Copyright © 2020 QualiteSoft. All rights reserved.
//

#import "RBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface NotificationsViewController : RBaseViewController

@end

NS_ASSUME_NONNULL_END
