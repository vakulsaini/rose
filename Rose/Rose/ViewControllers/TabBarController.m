//
//  TabBarController.m
//
//  Copyright © 2018 enAct eServices. All rights reserved.
//

#import "TabBarController.h"
#import "HomeViewController.h"
#import "ConsultsViewController.h"
#import "ContactsViewController.h"
#import "AddContactView.h"
#import "ConsultUser.h"

@interface TabBarController () <UITabBarControllerDelegate, AddContactViewDelegate, CallViewControllerEventDelegate> {
    NSArray *enabledConsultants;
    NSInteger currentIndex;
}

@property (nonatomic, strong) AddContactView *addContactView;
@property (nonatomic, strong) Patient *currentPatient;

@end

@implementation TabBarController

#pragma mark - LifeCycle
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    APP_DELEGATE.tabBarController = self;
    self.navigationController.navigationBarHidden = YES;

//    [self.tabBar setTintColor:FColorRed];
//    [self.tabBar setUnselectedItemTintColor:FColorBlack];
    
    [self setSelectedIndex:1];
    self.delegate = self;
    
    
    // Create a new layer which is the width of the device and with a heigh
    // of 0.5px.
    CALayer *topBorder = [CALayer layer];
    topBorder.frame = CGRectMake(0.0f, 0.0f, self.view.frame.size.width, 0.5f);
    
    // Set the background colour of the new layer to the colour you wish to
    // use for the border.
    topBorder.backgroundColor = [[UIColor colorWithRed:233.0/255.0 green:233.0/255.0 blue:233.0/255.0 alpha:1.0] CGColor];
    topBorder.opacity = 0.2;
    // Add the later to the tab bar's existing layer
    [self.tabBar.layer addSublayer:topBorder];
    
    
    // Disable Contact List for User/Physician
    if ([APP_DELEGATE.currentUser isPhysician]) {
        self.tabBar.items[2].enabled = NO;
    }
    else {
        self.tabBar.items[2].enabled = YES;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

+ (instancetype)instance {
    id class = [self class];
    NSString *className = NSStringFromClass(class);
    return [MAIN_STORYBOARD instantiateViewControllerWithIdentifier:className];
}

#pragma mark - Actions


#pragma mark - Methods
- (void)showAddContactPopup {
    self.addContactView = [AddContactView view];
    self.addContactView.delegate = self;
    self.addContactView.tfPatientMSPNumber.text = APP_DELEGATE.currentUser.MSPNumber;
    [self.addContactView showOnView:self.view animated:YES];
}

#pragma mark - Creating Transcript + Calling in loop
- (void)proceedToCreateTranscription {

    NSString *name = self.addContactView.tfPatientName.text;
    NSString *healthNum = self.addContactView.tfPatientHealthNumber.text;
    NSString *mspNumber = self.addContactView.tfPatientMSPNumber.text;
    NSString *userPhone = self.addContactView.tfUserPhoneNumber.text;
    NSString *dob = self.addContactView.tfPatientDOB.text;
    NSString *message = self.addContactView.tVPatientMessage.text;
    
    NSDictionary *params = @{@"PatientName": name,
                             @"PatientDOB": dob,
                             @"UserPhone": userPhone,
                             @"ReferringID": APP_DELEGATE.currentUser.Id,
                             @"Note": message,
                             @"PatientPHN": healthNum};
    
    // SHOW_LOADER;
    [[ApiHandler shared] sendHTTPRequest:APIRequestTypePOST url:WEB_SERVICE_PATIENT params:params withCompletion:^(id response, NSError *error) {
        HIDE_LOADER;
        if (error) {
            [self showAlertWithTitle:@"" message:error.localizedDescription okButtonTitle:@"OK"];
            return;
        }
        
        if ([response[@"status"] boolValue]) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.addContactView hide:YES];
            });
            
            NSDictionary *patientData = response[@"patient_data"];
            self.currentPatient = [[Patient alloc] initWithDicionary:patientData];

            self->currentIndex = 0;
            if (self->currentIndex < self->enabledConsultants.count) {
                [self call:self->enabledConsultants[self->currentIndex]];
            }
            
            // [self showAlertWithTitle:@"" message:response[@"message"] okButtonTitle:@"OK"];
        }
        else {
            [self showAlertWithTitle:@"" message:response[@"message"] okButtonTitle:@"OK"];
        }
    }];
}

- (void)proceedToShareTranscriptionForPatient:(NSString *)patientId consultId:(NSString *)consultId {
    
    NSDictionary *params = @{@"PatientID": patientId,
                             @"ConsultantID": consultId};
    
    SHOW_LOADER;
    [[ApiHandler shared] sendHTTPRequest:APIRequestTypePOST url:WEB_SERVICE_SHARE_TRANSCRIPT params:params withCompletion:^(id response, NSError *error) {
        HIDE_LOADER;
        if (error) {
//            [self showAlertWithTitle:@"" message:error.localizedDescription okButtonTitle:@"OK"];
            return;
        }
        
        if ([response[@"status"] boolValue]) {
            
        }
        else {
//            [self showAlertWithTitle:@"" message:response[@"message"] okButtonTitle:@"OK"];
        }
    }];
}


#pragma mark - CreateContact Delegate
- (void)contactViewDidClose:(AddContactView *)contactView {
    
}

- (void)contactViewDidSave:(AddContactView *)contactView {
    
    BOOL success = [self validateFields];
    if (!success) {
        return;
    }
    
    /// Checking for internet connection
    if (!IsInternetAvailable) {
        [self showAlertWithTitle:kNoInternetTitle message:kNoInternetMessage okButtonTitle:@"OK"];
        return;
    }
    
    SHOW_LOADER;
    [APP_DELEGATE loadConsultants:^(NSError *error) {
        
        if (error) {
            [self showAlertWithTitle:@"" message:error.localizedDescription okButtonTitle:@"Ok"];
            HIDE_LOADER;
            return;
        }
        
        NSMutableArray *eConsultants = [[NSMutableArray alloc] init];
        for (ConsultUser *user in APP_DELEGATE.consultUsers) {
            // If enabled consultant and not me
            if ([user isEnabled] && [user.Id integerValue] != [APP_DELEGATE.currentUser.Id integerValue]) {
                [eConsultants addObject:user];
            }
        }
        
        self->enabledConsultants = eConsultants;
        
        if (self->enabledConsultants.count == 0) {
            [self showAlertWithTitle:@"" message:@"There is no consultant enabled." okButtonTitle:@"Ok"];
            HIDE_LOADER;
            return;
        }
        
        //NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"SELF.Enable == %@", @"1"];
        //NSMutableArray *eConsultants = [APP_DELEGATE.consultUsers filteredArrayUsingPredicate:predicate1] ? [[APP_DELEGATE.consultUsers filteredArrayUsingPredicate:predicate1] mutableCopy] : @[].mutableCopy;

        /// Proceed
        [self proceedToCreateTranscription];
    }];
}

- (BOOL)validateFields {
    
    NSString *name = self.addContactView.tfPatientName.text;
    if ([name isEmpty]) {
        [self showAlertWithTitle:@"" message:@"Please enter patient name." okButtonTitle:@"OK"];
        return NO;
    }
    
    NSString *healthNum = self.addContactView.tfPatientHealthNumber.text;
    if ([healthNum isEmpty] || healthNum.length != 10) {
        [self showAlertWithTitle:@"" message:@"Please enter 10 digit valid Personal Health Number." okButtonTitle:@"OK"];
        return NO;
    }
    
    /*NSString *mspNumber = self.addContactView.tfPatientMSPNumber.text;
    if ([mspNumber isEmpty]) {
        [self showAlertWithTitle:@"" message:@"Please enter MSP number." okButtonTitle:@"OK"];
        return NO;
    }*/
    
    NSString *phoneNumber = self.addContactView.tfUserPhoneNumber.text;
    if ([phoneNumber isEmpty] || phoneNumber.length != 10) {
        [self showAlertWithTitle:@"" message:@"Please enter 10 digit valid caller phone number." okButtonTitle:@"OK"];
        return NO;
    }
    
    NSString *dob = self.addContactView.tfPatientDOB.text;
    if ([dob isEmpty]) {
        [self showAlertWithTitle:@"" message:@"Please select patient's date of birth." okButtonTitle:@"OK"];
        return NO;
    }
    
    NSString *message = self.addContactView.tVPatientMessage.text;
    if ([message isEmpty]) {
        [self showAlertWithTitle:@"" message:@"Please enter note." okButtonTitle:@"OK"];
        return NO;
    }
    
    return YES;
}

#pragma mark - SINCallNotification
- (void)call:(ConsultUser *)user {

    // We can pass the extra information in headers
    // id<SINCall> call = [APP_DELEGATE.callClient callUserVideoWithId:[NSString stringWithFormat:@"%@", user.Id]];
    id<SINCall> call = [APP_DELEGATE createCallInstanceForUserId:[NSString stringWithFormat:@"%@", user.Id]];
    
    [APP_DELEGATE showCall:call from:APP_DELEGATE.window.rootViewController delegate:self];
    
    // Call the API for call logs - > Dial API
    [APP_DELEGATE dialCall:call.callId callerId:[NSString stringWithFormat:@"%@", APP_DELEGATE.currentUser.Id] receiverId:[NSString stringWithFormat:@"%@", user.Id] completion:^{}];
    
    [self proceedToShareTranscriptionForPatient:self.currentPatient.PatientId consultId:call.remoteUserId];
}

- (void)callEvent:(CallEventType)eventType controller:(CallViewController *)controller call:(id<SINCall>)call {
    
    if (eventType == kCallEventTypeProgress) {
        
    }
    else if (eventType == kCallEventTypeEstablished) {
        [self sinCallDidEstablish:call];
    }
    else if (eventType == kCallEventTypeEnd) {
        [self sinCallDidEnd:call];
    }
}

- (void)sinCallDidEnd:(id<SINCall>)call {
    NSLog(@"%ld", call.details.endCause);
    if (call.details.endCause == SINCallEndCauseCanceled) {
        // Call canceled by caller
        // Update it's log on server
        [APP_DELEGATE updateCallStatus:call.callId callStatus:@"decline" callDuration:nil completion:^{}];
        return;
    }
    
    NSDate *callEstablishedTime = call.details.establishedTime;
    NSDate *callEndTime   = call.details.endedTime;
    BOOL callEstablished = NO;
    NSTimeInterval interval = 0;
    if (callEstablishedTime && callEndTime) {
        interval = fabs([callEstablishedTime timeIntervalSinceDate:callEndTime]);
        if (interval > 0) {
            callEstablished = YES;
        }
    }
    
    if (callEstablished) {
        // Call is surely established because call duration is greater than 0 seconds.
        currentIndex = 0;
        [APP_DELEGATE updateCallStatus:call.callId callStatus:@"RECEIVED" callDuration:@(interval) completion:^{}];
    }
    else {
        
        // Update it's log on server
        NSString *status = [APP_DELEGATE status:call];
        [APP_DELEGATE updateCallStatus:call.callId callStatus:status callDuration:nil completion:^{}];
        
        // Call did not establish. Try calling next consultant.
        currentIndex += 1;
        if (currentIndex >= enabledConsultants.count) {
             currentIndex = 0;
            // [self showAlertWithTitle:@"" message:@"No consultant picked your call." okButtonTitle: @"Ok"];
            // return;
        }
        [self call:enabledConsultants[currentIndex]];
    }
}

- (void)sinCallDidEstablish:(id<SINCall>)call {
    // [self proceedToShareTranscriptionForPatient:self.currentPatient.PatientId consultId:call.remoteUserId];
}


@end
