//
//  RBaseViewController.m
//
//  Copyright © 2018 TheTiger. All rights reserved.
//

#import "RBaseViewController.h"
#import "LoginViewController.h"
#import "ForgotPasswordViewController.h"
#import "SignUpViewController.h"

@interface RBaseViewController ()

/**
 NSLayoutConstraint IBOutlet - Layout constraint for navigation bar height. Specially for iPhoneX.
 */
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *layoutConstraintsNavBarHeight;

@end

@implementation RBaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    // self.view.backgroundColor = APP_BLUE_BG_COLOR;
    [self adjustNavBarHeight];
    
    if (self.lblTitle) {
        self.lblTitle.textColor = APP_GRAY_COLOR;
        self.lblTitle.superview.backgroundColor = APP_WHITE_COLOR;
    }
    
    if (![self isAuth]) {
        self.view.backgroundColor = APP_OFFWHITE_COLOR;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleDefault;
}


/**
 Creates an instance of its child class. The identifier of ViewCotroller in storyboard should be same as name of the class.
 
 @return returns an instance of referenced class.
 */
+ (instancetype)instance {
    id class = [self class];
    NSString *className = NSStringFromClass(class);
    if (class == [LoginViewController class] ||
        class == [ForgotPasswordViewController class] ||
        class == [SignUpViewController class]) {
        return [AUTH_STORYBOARD instantiateViewControllerWithIdentifier:className];
    }
    
    return [MAIN_STORYBOARD instantiateViewControllerWithIdentifier:className];
}

- (BOOL)isAuth {
    id class = [self class];
    if (class == [LoginViewController class] ||
        class == [ForgotPasswordViewController class] ||
        class == [SignUpViewController class]) {
        return YES;
    }
    return NO;
}

#pragma mark - Methods
- (void)adjustNavBarHeight {
    if (self.layoutConstraintsNavBarHeight != nil) {
        CGFloat statusBarHeight = [[UIApplication sharedApplication] statusBarFrame].size.height;
        self.layoutConstraintsNavBarHeight.constant = MAX(44.0 + statusBarHeight, 60.0);
        if (@available (iOS 11.0, *)) {
            self.layoutConstraintsNavBarHeight.constant = MAX(44.0 + [AppDelegate sharedInstance].window.safeAreaInsets.top, 60.0);
        }
    }
}

@end
