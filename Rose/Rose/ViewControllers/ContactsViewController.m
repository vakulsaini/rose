//
//  ContactsViewController.m
//  Rose
//
//  Created by Virender on 25/12/19.
//  Copyright © 2019 QualiteSoft. All rights reserved.
//

#import "ContactsViewController.h"
#import "ContactsCell.h"
#import "M13BadgeView.h"
#import "SearchBarView.h"
#import "AddContactView.h"
#import "CallViewController.h"
#import "TabBarController.h"

@interface ContactsViewController () <SearchBarViewDelegate, CallViewControllerEventDelegate>
/**
 UITableView IBOutlet - Main table
 */
@property (nonatomic, weak) IBOutlet UITableView *tblView;
@property (nonatomic, weak) IBOutlet UILabel *lblNoRecords;

@property (nonatomic, weak) IBOutlet UIButton *btnBell;

@property (nonatomic, strong) M13BadgeView *badgeView;

@property (nonatomic, strong) NSMutableArray *tableUsers;
@property (nonatomic, strong) SearchBarView *searchBarView;
 
@property (nonatomic, strong) NSMutableArray *sectionTitles;
@property (nonatomic, strong) NSMutableDictionary *clientsWithKeys;
 

@end

@implementation ContactsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view
    
    self.sectionTitles = [[NSMutableArray alloc] init];
    self.clientsWithKeys = [[NSMutableDictionary alloc] init];
    
    [self setUpBadge];
    [self.tblView registerNib:[UINib nibWithNibName:@"ContactsCell" bundle:nil] forCellReuseIdentifier:@"ContactsCell"];
    self.tblView.contentInset = UIEdgeInsetsMake(0.0, 0.0, 80.0, 0.0);
    
    self.tableUsers = [[NSMutableArray alloc] init];
    // [self.tableUsers addObjectsFromArray:APP_DELEGATE.contacts];
    
    self.searchBarView = [SearchBarView view];
    self.searchBarView.delegate = self;
    [self recalculateDataSectionWise:@""];
    [self reloadTable];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [APP_DELEGATE loadContacts:^{
        [self applyFilter];
        [self recalculateDataSectionWise:@""];
        [self reloadTable];
    }];
    
    [APP_DELEGATE refreshUnreadNotifications:^{
        [self refreshBadgeValue];
    }];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    [self adjustBadgeFrame];
}

- (IBAction)bellAction:(id)sender {
    [APP_DELEGATE goToNotificationsFrom:self];
}

- (IBAction)searchAction:(id)sender {
    [self.searchBarView showOnView:[sender superview] animated:YES];
}

- (IBAction)settingsAction:(id)sender {
    [APP_DELEGATE goToSettingsFrom:self];
}

- (IBAction)addContactAction:(id)sender {
    [APP_DELEGATE.tabBarController showAddContactPopup];
}

#pragma mark - Methods
- (void)setUpBadge {
    self.badgeView = [[M13BadgeView alloc] initWithFrame:CGRectMake(0, 0, 24.0, 24.0)];
    self.badgeView.font = [UIFont fontWithName:@"Helvetica Neue" size:14.0];
    self.badgeView.text = @"";
    self.badgeView.horizontalAlignment = M13BadgeViewHorizontalAlignmentRight;
    self.badgeView.verticalAlignment = M13BadgeViewVerticalAlignmentTop;
    [self.btnBell addSubview:self.badgeView];
    self.badgeView.hidden = YES;
}

- (void)refreshBadgeValue {
    dispatch_async(dispatch_get_main_queue(), ^{
        if ([APP_DELEGATE.unreadCount integerValue] == 0) {
            self.badgeView.text = @"";
            self.badgeView.hidden = YES;
        }
        else {
            self.badgeView.text = [NSString stringWithFormat:@"%@", APP_DELEGATE.unreadCount];
            self.badgeView.hidden = NO;
        }
        [self adjustBadgeFrame];
    });
}

- (void)adjustBadgeFrame {
    [self.badgeView autoSetBadgeFrame];
    
    CGRect frame = self.badgeView.frame;
    frame.origin.x -= 10.0;
    frame.origin.y += 10.0;
    self.badgeView.frame = frame;
}

- (void)applyFilter {
    [self.tableUsers removeAllObjects];
    NSString *text = self.searchBarView.textField.text;
    if (text.length > 0) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.fullName CONTAINS[cd] %@", text];
        NSArray *logs = [APP_DELEGATE.contacts filteredArrayUsingPredicate:predicate];
        [self.tableUsers addObjectsFromArray:logs];
    }
    else {
        [self.tableUsers addObjectsFromArray:APP_DELEGATE.contacts];
    }
}

- (void)reloadTable {
    dispatch_async(dispatch_get_main_queue(), ^{
        if ([self.tableUsers count] > 0) {
            self.tblView.hidden = NO;
            self.lblNoRecords.hidden = YES;
        }
        else {
            self.tblView.hidden = YES;
            self.lblNoRecords.hidden = NO;
        }
        
        [self.tblView reloadData];
    });
}

- (void)recalculateDataSectionWise:(NSString*)text {
    /// Vanish old data
    [self.sectionTitles removeAllObjects];
    [self.clientsWithKeys removeAllObjects];
     
    for (int i = 0; i<[self.tableUsers count]; i++ ) {
        /// If client billing name not started with alphabets or empty then move it into # key
        RUser *con = self.tableUsers[i];
        NSString *prefix = [con.fullName isEmpty] ? @"#" : [con.fullName substringToIndex:1];
        NSString *key = [self isAlphaNumeric:prefix.lowercaseString] ? prefix.uppercaseString : @"#";
        [self setContact:con forKey:key searchedText:text];
        self.sectionTitles = [self.clientsWithKeys.allKeys mutableCopy];
        [self.sectionTitles sortUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
            return obj1 < obj2;
        }];

        /// After sorting hash will appear on top of the list so need to move it to bottom of the list
        NSInteger hashIndex = [self.sectionTitles indexOfObject:@"#"];
        if (hashIndex > 0 && hashIndex < self.sectionTitles.count) {
            [self.sectionTitles removeObjectAtIndex:hashIndex];
            [self.sectionTitles addObject:@"#"];
        }
    }
     
    [self.sectionTitles sortUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
        return [obj1 caseInsensitiveCompare:obj2];
    }];
 }
- (BOOL)isAlphaNumeric:(NSString*)str{
    NSCharacterSet *unwantedCharacters =
       [[NSCharacterSet alphanumericCharacterSet] invertedSet];
    return ([str rangeOfCharacterFromSet:unwantedCharacters].location == NSNotFound);
}


- (void)setContact:(RUser *)contact forKey:(NSString*)key searchedText:(NSString*)text {

    /// Add only if searched text is empty OR billingName/Email contains the text
    if (text.length == 0 || [contact.fullName.lowercaseString containsString:text.lowercaseString] || [contact.Email.lowercaseString containsString:text.lowercaseString]) {
        NSMutableArray *contacts = [[NSMutableArray alloc] init];
        if (self.clientsWithKeys[key]) {
            contacts = self.clientsWithKeys[key];
        }
        [contacts addObject:contact];
        NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"fullName" ascending:YES];
        NSMutableArray *sortedArray = [[contacts sortedArrayUsingDescriptors:@[sort]] mutableCopy];
        if ([sortedArray count]) {
            self.clientsWithKeys[key] = sortedArray;
        }
    }
}
 
 
  
#pragma mark  UITableView Delegate & Datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [self.sectionTitles count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    id key = self.sectionTitles[section];
    id clients = self.clientsWithKeys[key];
    return [clients count];
 }

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellId = @"ContactsCell";
    ContactsCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
     cell.selectionStyle = UITableViewCellSelectionStyleNone;
    id key = self.sectionTitles[indexPath.section];
    id clients = self.clientsWithKeys[key];
    cell.user = clients[indexPath.row];
    
    cell.phoneButtonblock = ^(UIButton * _Nonnull sender, ContactsCell * _Nonnull cell) {
        // Only consultant can call to every user.
        if (![APP_DELEGATE.currentUser isPhysician]) {
            [self call:cell.user];
        }
    };
     
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    id key = self.sectionTitles[indexPath.section];
    id clients = self.clientsWithKeys[key];
    if (indexPath.row < [clients count] ){
        // RUser *contact = clients[indexPath.row];
    }
}
//- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
//    return  YES;
//}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
        //I have a static list of section titles in SECTION_ARRAY for reference.
        //Obviously your own section title code handles things differently to me.
    return self.sectionTitles[section];
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    return @[@"A", @"B", @"C", @"D", @"E", @"F", @"G", @"H", @"I", @"J", @"K", @"L", @"M", @"N", @"O", @"P", @"Q", @"R", @"S", @"T", @"U", @"V", @"W", @"X", @"Y", @"Z", @"#"];
}
 
- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index {
    return [self.sectionTitles indexOfObject:title];
}
 


#pragma mark - SearchBarViewDelegates
- (void)searchBarDidEditingChange:(SearchBarView *)view {
    [self applyFilter];
    [self recalculateDataSectionWise:@""];
    [self reloadTable];
}

- (void)searchBarDidClose:(SearchBarView *)view {
    [view hide:YES];
    [view clearText];
    [self applyFilter];
    [self recalculateDataSectionWise:@""];
    [self reloadTable];
}



#pragma mark - SINCallNotification

- (void)call:(RUser *)user {
        
    // We can pass the extra information in headers
    // id<SINCall> call = [APP_DELEGATE.callClient callUserVideoWithId:[NSString stringWithFormat:@"%@", user.Id]];
    id<SINCall> call = [APP_DELEGATE createCallInstanceForUserId:[NSString stringWithFormat:@"%@", user.Id]];
    
    [APP_DELEGATE showCall:call from:APP_DELEGATE.window.rootViewController delegate:self];
    
    // Call the API for call logs - > Dial API
    [APP_DELEGATE dialCall:call.callId callerId:[NSString stringWithFormat:@"%@", APP_DELEGATE.currentUser.Id] receiverId:[NSString stringWithFormat:@"%@", user.Id] completion:^{}];
}

- (void)callEvent:(CallEventType)eventType controller:(CallViewController *)controller call:(id<SINCall>)call {
    
    if (eventType == kCallEventTypeProgress) {
        
    }
    else if (eventType == kCallEventTypeEstablished) {
        [self sinCallDidEstablish:call];
    }
    else if (eventType == kCallEventTypeEnd) {
        [self sinCallDidEnd:call];
    }
}

- (void)sinCallDidEnd:(id <SINCall>)call {
    NSLog(@"%ld", call.details.endCause);
    if (call.details.endCause == SINCallEndCauseCanceled) {
        // Call canceled by caller
        // Update it's log on server
        [APP_DELEGATE updateCallStatus:call.callId callStatus:@"decline" callDuration:nil completion:^{}];
        return;
    }
    
    NSDate *callEstablishedTime = call.details.establishedTime;
    NSDate *callEndTime   = call.details.endedTime;
    BOOL callEstablished = NO;
    NSTimeInterval interval = 0;
    if (callEstablishedTime && callEndTime) {
        interval = fabs([callEstablishedTime timeIntervalSinceDate:callEndTime]);
        if (interval > 0) {
            callEstablished = YES;
        }
    }
    
    if (callEstablished) {
        // Call is surely established because call duration is greater than 0 seconds.
        [APP_DELEGATE updateCallStatus:call.callId callStatus:@"RECEIVED" callDuration:@(interval) completion:^{}];
    }
    else {
        
        // Update it's log on server
        NSString *status = [APP_DELEGATE status:call];
        [APP_DELEGATE updateCallStatus:call.callId callStatus:status callDuration:nil completion:^{}];
    }
}

- (void)sinCallDidEstablish:(id <SINCall>)call {
    
}

@end
