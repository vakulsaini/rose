//
//  ChangePasswordViewController.m
//  Rose
//
//  Created by Vakul Saini on 31/12/19.
//  Copyright © 2019 QualiteSoft. All rights reserved.
//

#import "ChangePasswordViewController.h"

@interface ChangePasswordViewController () <UITextFieldDelegate>
@property (nonatomic, strong) IBOutlet UIButton *btnBack;
@property (weak, nonatomic) IBOutlet UIButton *btnChangePassword;
@property (weak, nonatomic) IBOutlet UITextField *tfOldPassword;
@property (weak, nonatomic) IBOutlet UITextField *tfNewPassword;
@property (weak, nonatomic) IBOutlet UITextField *tfConfirmPassword;
@property (weak, nonatomic) IBOutlet UIImageView *iVLogo;

@end

@implementation ChangePasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setUpUI];
    
    [self.iVLogo.superview makeRound];
    NSURL *imageUrl = [NSURL URLWithString:APP_DELEGATE.currentUser.ProfilePic];
    [self.iVLogo sd_setImageWithURL:imageUrl placeholderImage:kUserProfilePlaceholder];
}

#pragma mark Actions
- (IBAction)backAction:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated: YES];
}

- (IBAction)submitAction:(id)sender {
    BOOL success = [self validateFields];
    if (!success) {
        return;
    }
    
    /// Checking for internet connection
    if (!IsInternetAvailable) {
        [self showAlertWithTitle:kNoInternetTitle message:kNoInternetMessage okButtonTitle:@"OK"];
        return;
    }
    
    /// Proceed
    [self proceedToChangePassword];
}

#pragma mark - Methods
- (void)setUpUI {
    [self.tfOldPassword.superview makeRoundWithRadius:2.0];
    [self.tfNewPassword.superview makeRoundWithRadius:2.0];
    [self.tfConfirmPassword.superview makeRoundWithRadius:2.0];
    [self.btnChangePassword makeRoundWithRadius:2.0];
}

- (void)proceedToChangePassword {
    
    SHOW_LOADER;
    NSDictionary *params = @{@"Email": APP_DELEGATE.currentUser.Email,
                             @"Oldpassword": self.tfOldPassword.text,
                             @"Newpassword": self.tfNewPassword.text,
                             @"_method": @"PUT"};
    
    [[ApiHandler shared] sendHTTPRequest:APIRequestTypePOST url:WEB_SERVICE_CHANGE_PASSWORD params:params withCompletion:^(id response, NSError *error) {
        HIDE_LOADER;
        if (error) {
            [self showAlertWithTitle:@"" message:error.localizedDescription okButtonTitle:@"OK"];
            return;
        }
        
        if ([response[@"status"] boolValue]) {
            [self showAlertWithTitle:@"" message:response[@"message"] okButtonTitle:@"OK" withCompletionBlock:^(NSString *title, UIAlertController *alert) {
                if ([title isEqualToString:@"OK"]) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self backAction:nil];
                    });
                }
            }];
        }
        else {
            [self showAlertWithTitle:@"" message:response[@"message"] okButtonTitle:@"OK"];
        }
    }];
}

- (BOOL)validateFields {
    
    NSString *oldPassword = self.tfOldPassword.text;
    if (oldPassword.isEmpty) {
        [self showAlertWithTitle:@"" message:@"Please enter your old password." okButtonTitle:@"OK"];
        return NO;
    }

    NSString *newPassword = self.tfNewPassword.text;
    if (newPassword.isEmpty) {
        [self showAlertWithTitle:@"" message:@"Please enter new password." okButtonTitle:@"OK"];
        return NO;
    }
    
    NSString *confirmPassword = self.tfConfirmPassword.text;
    if (confirmPassword.isEmpty) {
        [self showAlertWithTitle:@"" message:@"Please confirm your password." okButtonTitle:@"OK"];
        return NO;
    }
    
    if (![newPassword isEqualToString:confirmPassword]) {
        [self showAlertWithTitle:@"" message:@"Password and confirm password do not match." okButtonTitle:@"OK"];
        return NO;
    }
    
    return YES;
}

#pragma mark UItextField Delegates
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

@end
