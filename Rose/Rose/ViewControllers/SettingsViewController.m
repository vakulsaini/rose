//
//  SettingsViewController.m
//  Rose
//
//  Created by Vakul Saini on 31/12/19.
//  Copyright © 2019 QualiteSoft. All rights reserved.
//

#import "SettingsViewController.h"
#import "SettingsCell.h"
#import "ChangePasswordViewController.h"
#import "SettingsHeaderView.h"
#import "EditProfileViewController.h"
#import "PreviewView.h"

@interface SettingsViewController () <UITableViewDelegate, UITableViewDataSource>

/**
UITableView IBOutlet - Main table
*/
@property (nonatomic, strong) IBOutlet UITableView *tblView;
@property (nonatomic, strong) IBOutlet UIButton *btnBack;
@property (nonatomic, strong) NSMutableArray *items;

@end

@implementation SettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.items = [[NSMutableArray alloc] init];
    [self.tblView registerNib:[UINib nibWithNibName:@"SettingsCell" bundle:nil] forCellReuseIdentifier:@"SettingsCell"];
    
    [self.items addObject:@{@"title": @"Account", @"image": [UIImage imageNamed: @"avatar"],
                            @"options": @[@{@"title": @"Edit Profile", @"image": [UIImage imageNamed: @"right_arrow"]},
                                          @{@"title": @"Change Password", @"image": [UIImage imageNamed: @"right_arrow"]},
                                          @{@"title": @"Logout", @"image": [NSNull null]},
                                          @{@"title": @"Delete Account", @"image": [NSNull null]},
                                          @{@"title": @"Show Me", @"image": [NSNull null]}]}];
    

    [self.items addObject:@{@"title": @"Notifications", @"image": [UIImage imageNamed: @"bell"],
                            @"options": @[@{@"title": @"New Consult", @"image": [NSNull null]},
                                          @{@"title": @"New Transcription", @"image": [NSNull null]}]}];
    
    [self.items addObject:@{@"title": @"Information", @"image": [UIImage imageNamed: @"info"],
                            @"options": @[@{@"title": @"Privacy Policy", @"image": [UIImage imageNamed: @"right_arrow"]}]}];
    
}

#pragma mark Actions
- (IBAction)backAction:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated: YES];
}

#pragma mark Methods
- (void)reloadTable {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tblView reloadData];
    });
}

- (void)performActionForIndex:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0) {
        // Main section
        if (indexPath.row == 0) {
            EditProfileViewController *editProfileVC = [EditProfileViewController instance];
            [self.navigationController pushViewController:editProfileVC animated:YES];
        }
        else if (indexPath.row == 1) {
            // Change password
            ChangePasswordViewController *chPwdVC = [ChangePasswordViewController instance];
            [self.navigationController pushViewController:chPwdVC animated:YES];
        }
        else if (indexPath.row == 2) {
            // Logout
            [self showAlertWithTitle:@"ROSe" message:@"Are you sure you want to logout?" okButtonTitle:@"Yes" cancelButtonTitle:@"No" withCompletionBlock:^(NSString *title, UIAlertController *alert) {
                if ([title isEqualToString:@"Yes"]) {
                    [APP_DELEGATE clearUserData];
                    [APP_DELEGATE showAuthScreen:YES];
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_USER_DID_LOGOUT object:nil];
                    
                    APP_DELEGATE.currentUser = nil;
                }
            }];
        }
        else {
            // Delete account
            [self showAlertWithTitle:@"ROSe" message:@"Are you sure you want to delete this account? You will lose all of its data and no longer to use this account again." okButtonTitle:@"Yes" cancelButtonTitle:@"No" withCompletionBlock:^(NSString *title, UIAlertController *alert) {
                if ([title isEqualToString:@"Yes"]) {
                    [self proceedWithDeletingTheAccount];
                }
            }];
        }
    }
    else if (indexPath.section == 1) {
        // Notifications section
    }
    else if (indexPath.section == 2) {
        // Information section
        // NSString *path = [[NSBundle mainBundle] pathForResource:@"PrivacyPolicy" ofType:@"pdf"];
        NSString *path = @"http://99.79.23.145/uploads/SevasHealthIncPrivacyPolicy.pdf";
        PreviewView *preview = [PreviewView view];
        preview.url = path;
        preview.viewPrintOption.hidden = YES;
        preview.lblTitle.text = @"Privacy Policy";
        [preview showInView:APP_DELEGATE.window];
    }
}

- (void)proceedWithDeletingTheAccount {
    SHOW_LOADER;
    [APP_DELEGATE.currentUser deleteAccountWithCompletionBlock:^(id response, NSError *error) {
        HIDE_LOADER;
        if (error) {
            [self showAlertWithTitle:@"" message:error.localizedDescription okButtonTitle:@"Ok" withCompletionBlock:^(NSString *title, UIAlertController *alert) {
                if (error.code == 6000) {
                    // User has been deleted already.
                    [APP_DELEGATE clearUserData];
                    [APP_DELEGATE showAuthScreen:YES];
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_USER_DID_LOGOUT object:nil];
                    
                    APP_DELEGATE.currentUser = nil;
                }
            }];
        }
        else {
            
            if ([response[@"status"] boolValue]) {
                [self showAlertWithTitle:@"" message:response[@"message"] okButtonTitle:@"Ok" withCompletionBlock:^(NSString *title, UIAlertController *alert) {
                    // User has been deleted.
                    [APP_DELEGATE clearUserData];
                    [APP_DELEGATE showAuthScreen:YES];
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_USER_DID_LOGOUT object:nil];
                    
                    APP_DELEGATE.currentUser = nil;
                }];
            }
            else {
                [self showAlertWithTitle:@"" message:response[@"message"] okButtonTitle:@"OK"];
            }
        }
    }];
}


- (void)performOnDuty:(UISwitch *)swtch {
    
    NSDictionary *params = @{@"Id": APP_DELEGATE.currentUser.Id,
                             @"Status": swtch.isOn ? @"1" : @"0",
                             @"_method": @"PUT"};
    
    SHOW_LOADER;
    [[ApiHandler shared] sendHTTPRequest:APIRequestTypePOST url:WEB_SERVICE_ENABLE_LIST params:params withCompletion:^(id response, NSError *error) {
        HIDE_LOADER;
        if (error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                swtch.on = !swtch.isOn;
            });
        
            [self showAlertWithTitle:@"" message:error.localizedDescription okButtonTitle:@"OK"];
            return;
        }
        
        if ([response[@"status"] boolValue]) {
            APP_DELEGATE.currentUser.Status = @(swtch.isOn);
            [APP_DELEGATE saveUserLocally];
            [self showAlertWithTitle:@"" message:response[@"message"] okButtonTitle:@"OK"];
        }
        else {
            dispatch_async(dispatch_get_main_queue(), ^{
                swtch.on = !swtch.isOn;
            });
            [self showAlertWithTitle:@"" message:response[@"message"] okButtonTitle:@"OK"];
        }
    }];
}


- (void)performConsultNotification:(UISwitch *)swtch {
    
    NSDictionary *params = @{@"Id": APP_DELEGATE.currentUser.Id,
                             @"Value": swtch.isOn ? @"1" : @"0",
                             @"_method": @"PUT"};
    
    SHOW_LOADER;
    [[ApiHandler shared] sendHTTPRequest:APIRequestTypePOST url:WEB_SERVICE_ENABLE_CONSULT_NOTIFICATIONS params:params withCompletion:^(id response, NSError *error) {
        HIDE_LOADER;
        if (error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                swtch.on = !swtch.isOn;
            });
        
            [self showAlertWithTitle:@"" message:error.localizedDescription okButtonTitle:@"OK"];
            return;
        }
        
        if ([response[@"status"] boolValue]) {
            APP_DELEGATE.currentUser.ConsultantNotification = @(swtch.isOn);
            [APP_DELEGATE saveUserLocally];
            [self showAlertWithTitle:@"" message:response[@"message"] okButtonTitle:@"OK"];
        }
        else {
            dispatch_async(dispatch_get_main_queue(), ^{
                swtch.on = !swtch.isOn;
            });
            [self showAlertWithTitle:@"" message:response[@"message"] okButtonTitle:@"OK"];
        }
    }];
}


- (void)performTranscriptNotification:(UISwitch *)swtch {
    
    NSDictionary *params = @{@"Id": APP_DELEGATE.currentUser.Id,
                             @"Value": swtch.isOn ? @"1" : @"0",
                             @"_method": @"PUT"};
    
    SHOW_LOADER;
    [[ApiHandler shared] sendHTTPRequest:APIRequestTypePOST url:WEB_SERVICE_ENABLE_TRANSCRIPT_NOTIFICATIONS params:params withCompletion:^(id response, NSError *error) {
        HIDE_LOADER;
        if (error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                swtch.on = !swtch.isOn;
            });
        
            [self showAlertWithTitle:@"" message:error.localizedDescription okButtonTitle:@"OK"];
            return;
        }
        
        if ([response[@"status"] boolValue]) {
            APP_DELEGATE.currentUser.TranscriptionNotification = @(swtch.isOn);
            [APP_DELEGATE saveUserLocally];
            [self showAlertWithTitle:@"" message:response[@"message"] okButtonTitle:@"OK"];
        }
        else {
            dispatch_async(dispatch_get_main_queue(), ^{
                swtch.on = !swtch.isOn;
            });
            [self showAlertWithTitle:@"" message:response[@"message"] okButtonTitle:@"OK"];
        }
    }];
}


#pragma mark  UITableView Delegate & Datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.items.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSDictionary *data = self.items[section];
    return [data[@"options"] count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellId = @"SettingsCell";
    SettingsCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    cell.backgroundColor = [UIColor whiteColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    NSDictionary *data = self.items[indexPath.section];
    NSArray *options = data[@"options"];
    
    cell.lblTitle.text = options[indexPath.row][@"title"];
    
    UIImage *image = options[indexPath.row][@"image"];
    if ([image isEqual:[NSNull null]]) {
        [cell.btnImage setImage:nil forState:UIControlStateNormal];
    } else {
        [cell.btnImage setImage:image forState:UIControlStateNormal];
    }
    
    cell.toggleButtonCell = (indexPath.section == 1);
    cell.toggleButtonblock = nil;
    if (indexPath.section == 0) {
        if (indexPath.row == options.count - 1) {
            cell.toggleButtonCell = YES;
            cell.swtch.on = [APP_DELEGATE.currentUser.Status boolValue];
            cell.toggleButtonblock = ^(UISwitch *swtch, SettingsCell *cell) {
                [self performOnDuty:swtch];
            };
        }
    }
    else if (indexPath.section == 1) {
        cell.toggleButtonCell = YES;
        if (indexPath.row == 0) {
            // Consult
            cell.swtch.on = [APP_DELEGATE.currentUser.ConsultantNotification boolValue];
            cell.toggleButtonblock = ^(UISwitch *swtch, SettingsCell *cell) {
                [self performConsultNotification:swtch];
            };
        }
        else if (indexPath.row == 1) {
            // Transcript
            cell.swtch.on = [APP_DELEGATE.currentUser.TranscriptionNotification boolValue];
            cell.toggleButtonblock = ^(UISwitch *swtch, SettingsCell *cell) {
                [self performTranscriptNotification:swtch];
            };
        }
    }
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self performActionForIndex: indexPath];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    SettingsHeaderView *headerView = [SettingsHeaderView view];
    headerView.lblTitle.text = self.items[section][@"title"];
    UIImage *image = self.items[section][@"image"];
    if ([image isEqual:[NSNull null]]) {
        [headerView.btn setImage:nil forState:UIControlStateNormal];
    } else {
        [headerView.btn setImage:image forState:UIControlStateNormal];
    }
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 44;
}

@end
