//
//  HomeViewController.m
//  Rose
//
//  Created by Vakul Saini on 23/12/19.
//  Copyright © 2019 QualiteSoft. All rights reserved.
//

#import "HomeViewController.h"
#import "HomeCell.h"
#import "M13BadgeView.h"
#import "SearchBarView.h"
#import "CallInfoView.h"
#import "TabBarController.h"

@interface HomeViewController () <SearchBarViewDelegate, CallViewControllerEventDelegate>

// 0 = All, 1 = Missed
@property (nonatomic) NSInteger segmentIndex;

/**
UITableView IBOutlet - Main table
*/
@property (nonatomic, strong) IBOutlet UITableView *tblView;
@property (nonatomic, strong) NSMutableArray *callLogs;
@property (nonatomic, strong) NSMutableArray *tableLogs;
@property (nonatomic, weak) IBOutlet UIButton *btnBell;
@property (nonatomic, weak) IBOutlet UILabel *lblNoRecords;

@property (nonatomic, retain) M13BadgeView *badgeView;
@property (nonatomic, strong) SearchBarView *searchBarView;
@property (nonatomic, strong) CallInfoView *callInfoView;

@property (nonatomic, weak) IBOutlet UIView *viewMissedDot;
@property (nonatomic, weak) IBOutlet CustomTitleLabel *lblAll;
@property (nonatomic, weak) IBOutlet CustomTitleLabel *lblMissed;
@property (nonatomic, weak) IBOutlet UIButton *btnAll;
@property (nonatomic, weak) IBOutlet UIButton *btnMissed;
@property (nonatomic, weak) IBOutlet UIView *viewStrip;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *layoutConstraintStripLeading;

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.tblView.backgroundColor = [UIColor clearColor];
    [self.tblView registerNib:[UINib nibWithNibName:@"HomeCell" bundle:nil] forCellReuseIdentifier:@"HomeCell"];
    self.tblView.contentInset = UIEdgeInsetsMake(0.0, 0.0, 80.0, 0.0);
    
    self.callLogs = [[NSMutableArray alloc] init];
    self.tableLogs = [[NSMutableArray alloc] init];
    self.segmentIndex = 0;
    [self setUpBadge];
    
    self.viewStrip.backgroundColor = APP_PINK_COLOR;
    [self.viewStrip makeRoundWithRadius:self.viewStrip.height/2];
    [self.viewMissedDot makeRound];
    
    self.callInfoView = [CallInfoView view];
    [self reloadTable];
    
    self.searchBarView = [SearchBarView view];
    self.searchBarView.delegate = self;
    [self moveStripAtIndex:0 animated:NO];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    [self adjustBadgeFrame];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self loadLogs:^{
        [self applyFilter];
        [self reloadTable];
    }];
    
    [APP_DELEGATE refreshUnreadNotifications:^{
        [self refreshBadgeValue];
    }];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];

}

#pragma mark - Actions
- (IBAction)segmentAction:(id)sender {
    
    if (sender == self.btnAll) {
        if (self.segmentIndex == 0) {
            // Already on same tab
            return;
        }
        
        self.segmentIndex = 0;
    }
    else {
        if (self.segmentIndex == 1) {
            // Already on same tab
            return;
        }
        
        self.segmentIndex = 1;
    }
    
    [self applyFilter];
    [self reloadTable];
    [self moveStripAtIndex:self.segmentIndex animated:YES];
}

- (IBAction)bellAction:(id)sender {
    [APP_DELEGATE goToNotificationsFrom:self];
}

- (IBAction)searchAction:(id)sender {
    [self.searchBarView showOnView:[sender superview] animated:YES];
}

- (IBAction)settingsAction:(id)sender {
    [APP_DELEGATE goToSettingsFrom:self];
}

- (IBAction)addContactAction:(id)sender {
    [APP_DELEGATE.tabBarController showAddContactPopup];
}

#pragma mark - Methods
- (void)setUpBadge {
    self.badgeView = [[M13BadgeView alloc] initWithFrame:CGRectMake(0, 0, 24.0, 24.0)];
    self.badgeView.font = [UIFont fontWithName:@"Helvetica Neue" size:14.0];
    self.badgeView.text = @"";
    self.badgeView.horizontalAlignment = M13BadgeViewHorizontalAlignmentRight;
    self.badgeView.verticalAlignment = M13BadgeViewVerticalAlignmentTop;
    [self.btnBell addSubview:self.badgeView];
    self.badgeView.hidden = YES;
}

- (void)refreshBadgeValue {
    dispatch_async(dispatch_get_main_queue(), ^{
        if ([APP_DELEGATE.unreadCount integerValue] == 0) {
            self.badgeView.text = @"";
            self.badgeView.hidden = YES;
        }
        else {
            self.badgeView.text = [NSString stringWithFormat:@"%@", APP_DELEGATE.unreadCount];
            self.badgeView.hidden = NO;
        }
        [self adjustBadgeFrame];
    });
}

- (void)adjustBadgeFrame {
    [self.badgeView autoSetBadgeFrame];
    
    CGRect frame = self.badgeView.frame;
    frame.origin.x -= 10.0;
    frame.origin.y += 10.0;
    self.badgeView.frame = frame;
}

- (void)moveStripAtIndex:(NSInteger)index animated:(BOOL)animated {
    CGFloat leading = (self.lblAll.superview.width - self.viewStrip.width)/2;
    if (index == 0) {
        self.lblAll.textColor = APP_PINK_COLOR;
        self.lblMissed.textColor = APP_GRAY_COLOR;
    }
    else {
        leading += self.lblAll.superview.width;
        self.lblAll.textColor = APP_GRAY_COLOR;
        self.lblMissed.textColor = APP_PINK_COLOR;
    }
    
    if (animated) {
        self.layoutConstraintStripLeading.constant = leading;
        [UIView animateWithDuration:0.3 animations:^{
            [self.view layoutIfNeeded];
        }];
    }
    else {
        self.layoutConstraintStripLeading.constant = leading;
        [self.view layoutIfNeeded];
    }
}

- (void)applyFilter {
    [self.tableLogs removeAllObjects];
    NSString *text = self.searchBarView.textField.text;
    if (self.segmentIndex == 0) {
        // Show all
        if (text.length > 0) {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.name CONTAINS[cd] %@", text];
            NSArray *logs = [self.callLogs filteredArrayUsingPredicate:predicate];
            [self.tableLogs addObjectsFromArray:logs];
        }
        else {
            [self.tableLogs addObjectsFromArray:self.callLogs];
        }
    }
    else {
        // Show missed calls
        if (text.length > 0) {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"callType == %d && SELF.name CONTAINS[cd] %@", 1, text];
            NSArray *logs = [self.callLogs filteredArrayUsingPredicate:predicate];
            [self.tableLogs addObjectsFromArray:logs];
        }
        else {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"callType == %d", 1];
            NSArray *logs = [self.callLogs filteredArrayUsingPredicate:predicate];
            [self.tableLogs addObjectsFromArray:logs];
        }
    }
}

- (void)reloadTable {
    dispatch_async(dispatch_get_main_queue(), ^{
        
        if ([self.tableLogs count] > 0) {
            self.tblView.hidden = NO;
            self.lblNoRecords.hidden = YES;
        }
        else {
            self.tblView.hidden = YES;
            self.lblNoRecords.hidden = NO;
        }
        
        [self.tblView reloadData];
    });
}

- (void)loadLogs:(void (^) (void))completion {
    
    /// Checking for internet connection
    if (!IsInternetAvailable) {
        return;
    }
    
    if (!self.callLogs) {
        self.callLogs = [[NSMutableArray alloc] init];
    }
    
    NSString *consultantsAPIUrl = [NSString stringWithFormat:@"%@/%@", WEB_SERVICE_CALL_LOG, APP_DELEGATE.currentUser.Id];
    [[ApiHandler shared] sendHTTPRequest:APIRequestTypeGET url:consultantsAPIUrl params:nil withCompletion:^(id response, NSError *error) {
        HIDE_LOADER;
        if (error) {
            completion();
            return;
        }
        
        if ([response[@"status"] boolValue]) {
            NSArray *logs = response[@"logdetail"];
            if (logs && [logs isKindOfClass:[NSArray class]]) {
                [self.callLogs removeAllObjects];
                for (NSDictionary *l in logs) {
                    CallHistory *history = [[CallHistory alloc] initWithDicionary:l];
                    [self.callLogs addObject:history];
                }
            }
        }
        else {
            [self showAlertWithTitle:@"" message:response[@"message"] okButtonTitle:@"Ok"];
        }
        completion();
    }];
}

#pragma mark  UITableView Delegate & Datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.tableLogs.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellId = @"HomeCell";
    HomeCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    cell.backgroundColor = [UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.callHistory = self.tableLogs[indexPath.row];
    cell.infoButtonblock = ^(UIButton *sender, HomeCell *cell) {
        self.callInfoView.callHistory = cell.callHistory;
        [self.callInfoView showOnView:APP_DELEGATE.window animated:YES];
    };
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    CallHistory *history = self.tableLogs[indexPath.row];
    [self call:history.personID];
}

#pragma mark - SearchBarViewDelegates
- (void)searchBarDidEditingChange:(SearchBarView *)view {
    [self applyFilter];
    [self reloadTable];
}

- (void)searchBarDidClose:(SearchBarView *)view {
    [view hide:YES];
    [view clearText];
    [self applyFilter];
    [self reloadTable];
}


#pragma mark - SINCallNotification
- (void)call:(NSString *)userID {
        
    id<SINCall> call = [APP_DELEGATE.callClient callUserVideoWithId:[NSString stringWithFormat:@"%@", userID]];
    [APP_DELEGATE showCall:call from:APP_DELEGATE.window.rootViewController delegate:self];
    
    // Call the API for call logs - > Dial API
    [APP_DELEGATE dialCall:call.callId callerId:[NSString stringWithFormat:@"%@", APP_DELEGATE.currentUser.Id] receiverId:[NSString stringWithFormat:@"%@", userID] completion:^{}];
}

- (void)callEvent:(CallEventType)eventType controller:(CallViewController *)controller call:(id<SINCall>)call {
    
    if (eventType == kCallEventTypeProgress) {
        
    }
    else if (eventType == kCallEventTypeEstablished) {
        [self sinCallDidEstablish:call];
    }
    else if (eventType == kCallEventTypeEnd) {
        [self sinCallDidEnd:call];
    }
}

- (void)sinCallDidEnd:(id <SINCall> )call {
    NSLog(@"%ld", call.details.endCause);
    if (call.details.endCause == SINCallEndCauseCanceled) {
        // Call canceled by caller
        // Update it's log on server
        [APP_DELEGATE updateCallStatus:call.callId callStatus:@"decline" callDuration:nil completion:^{}];
        return;
    }
    
    NSDate *callEstablishedTime = call.details.establishedTime;
    NSDate *callEndTime   = call.details.endedTime;
    BOOL callEstablished = NO;
    NSTimeInterval interval = 0;
    if (callEstablishedTime && callEndTime) {
        interval = fabs([callEstablishedTime timeIntervalSinceDate:callEndTime]);
        if (interval > 0) {
            callEstablished = YES;
        }
    }
    
    if (callEstablished) {
        // Call is surely established because call duration is greater than 0 seconds.
        [APP_DELEGATE updateCallStatus:call.callId callStatus:@"RECEIVED" callDuration:@(interval) completion:^{}];
    }
    else {
        
        // Update it's log on server
        NSString *status = [APP_DELEGATE status:call];
        [APP_DELEGATE updateCallStatus:call.callId callStatus:status callDuration:nil completion:^{}];
    }
}

- (void)sinCallDidEstablish:(id <SINCall> )call {
    
}


@end
