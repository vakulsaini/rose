//
//  SettingsViewController.h
//  Rose
//
//  Created by Vakul Saini on 31/12/19.
//  Copyright © 2019 QualiteSoft. All rights reserved.
//

#import "RBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface SettingsViewController : RBaseViewController

@end

NS_ASSUME_NONNULL_END
