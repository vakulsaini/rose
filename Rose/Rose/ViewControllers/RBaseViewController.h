//
//  RBaseViewController.h
//
//  Copyright © 2018 TheTiger. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomHeadingLabel.h"

@interface RBaseViewController : UIViewController

/**
 A universal Label for all of its child classes.
 */
@property (nonatomic, weak) IBOutlet CustomHeadingLabel *lblTitle;


/**
 A universal method to create an instance of this class (the child class by which this function is called) from Storyboard.
 
 @return returns an instance of refrenced child class.
 */
+ (instancetype)instance;

/**
 A universal method to method to adjust the navigation bar height for all of its child classes specially for iPhoneX.
 */
- (void)adjustNavBarHeight;

@end
