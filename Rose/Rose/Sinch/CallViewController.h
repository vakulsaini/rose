#import <UIKit/UIKit.h>
#import <Sinch/Sinch.h>

#import "SINUIViewController.h"

typedef enum { kButtonsAnswerDecline, kButtonsHangup, kButtonsWakenByCallKit } EButtonsBar;
typedef enum { kCallEventTypeEnd, kCallEventTypeProgress, kCallEventTypeEstablished} CallEventType;

@class CallViewController;

@protocol CallViewControllerEventDelegate <NSObject>

- (void)callEvent:(CallEventType)eventType controller:(CallViewController *)controller call:(id <SINCall>)call;

@end


@interface CallViewController : SINUIViewController

@property (weak, nonatomic) IBOutlet UILabel *remoteUsername;
@property (weak, nonatomic) IBOutlet UILabel *callStateLabel;
@property (weak, nonatomic) IBOutlet UIButton *answerButton;
@property (weak, nonatomic) IBOutlet UIButton *declineButton;
@property (weak, nonatomic) IBOutlet UIButton *endCallButton;
@property (weak, nonatomic) IBOutlet UIButton *switchCameraButton;
@property (weak, nonatomic) IBOutlet UIView *remoteVideoView;
@property (weak, nonatomic) IBOutlet UIView *localVideoView;

@property (nonatomic, readwrite, strong) NSTimer *durationTimer;

@property (nonatomic, readwrite, strong) id<SINCall> call;
@property (nonatomic, assign) id <CallViewControllerEventDelegate> eventDelegate;

- (IBAction)accept:(id)sender;
- (IBAction)decline:(id)sender;
- (IBAction)hangup:(id)sender;

@property (weak, nonatomic) IBOutlet UIView *rViewIncomingCall;
@property (weak, nonatomic) IBOutlet UILabel *rLabelUsername;
@property (weak, nonatomic) IBOutlet UILabel *rLabelCallStatus;
@property (weak, nonatomic) IBOutlet UIImageView *rImageViewUser;

@end
