#import "AppDelegate.h"
#import "CallViewController.h"
#import "CallViewController+UI.h"
#import "SINCallKitProvider.h"
#import <Sinch/SINUIView+Fullscreen.h>

// Usage of gesture recognizers to toggle between front- and back-camera and fullscreen.
//
// * Triple tap the local preview view to pause sending local video stream to remote.
// * Double tap the local preview view to switch between front- and back-camera.
// * Single tap the local video stream view to make it go full screen.
//     Tap again to go back to normal size.
// * Single tap the remote video stream view to make it go full screen.
//     Tap again to go back to normal size.

@interface CallViewController () <SINCallDelegate>
@property (weak, nonatomic) IBOutlet UIGestureRecognizer *remoteVideoFullscreenGestureRecognizer;
@property (weak, nonatomic) IBOutlet UIGestureRecognizer *localVideoFullscreenGestureRecognizer;
@property (weak, nonatomic) IBOutlet UIGestureRecognizer *switchCameraGestureRecognizer;
@property (weak, nonatomic) IBOutlet UITapGestureRecognizer *pauseResumeVideoRecognizer;
@property (nonatomic, readwrite) BOOL videoPaused;
@end

@implementation CallViewController

- (id<SINAudioController>)audioController {
    return [[(AppDelegate *)[[UIApplication sharedApplication] delegate] client] audioController];
}

- (id<SINVideoController>)videoController {
    return [[(AppDelegate *)[[UIApplication sharedApplication] delegate] client] videoController];
}

- (void)setCall:(id<SINCall>)call {
    _call = call;
    _call.delegate = self;
}

#pragma mark - UIViewController Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.rViewIncomingCall.hidden = YES;
    
    if ([self.call direction] == SINCallDirectionIncoming) {
        // Enter the view from CallKit incoming call
        if ([[(AppDelegate *)[[UIApplication sharedApplication] delegate] callKitProvider] callExists:self.call.callId]) {
            // Enter from CallKit lock screen
            if ([self.call state] == SINCallStateEstablished) {
                [self startCallDurationTimerWithSelector:@selector(onDurationTimer:)];
                [self showButtons:kButtonsHangup];
                [self showRemoteVideoView];
                
            }
            // Enter from CallKit but not lock screen
            else {
                //[self setCallStatusText:@""];
                self.rViewIncomingCall.hidden = NO;
                [self setCallStatusText:@"Incoming..."];
                [self showButtons:kButtonsWakenByCallKit];
                [self showRemoteVideoView];
            }
        }
        // Enter the view from normal incoming call
        else {
            //[self setCallStatusText:@""];
            self.rViewIncomingCall.hidden = NO;
            [self setCallStatusText:@"Incoming..."];
            [self showButtons:kButtonsAnswerDecline];
            [[self audioController] startPlayingSoundFile:[self pathForSound:@"phone_loud1.mp3"] loop:YES];
        }

    } else {
        [self setCallStatusText:@"Calling..."];
        [self showButtons:kButtonsHangup];
    }
    
    if ([self.call.details isVideoOffered]) {
        [self.videoController localView].transform = CGAffineTransformMakeScale(-1, 1);
        [self.localVideoView addSubview:[self.videoController localView]];
        // [self.localVideoFullscreenGestureRecognizer requireGestureRecognizerToFail:self.switchCameraGestureRecognizer];
        //  [self.localVideoFullscreenGestureRecognizer requireGestureRecognizerToFail:self.pauseResumeVideoRecognizer];
        //  [self.switchCameraGestureRecognizer requireGestureRecognizerToFail:self.pauseResumeVideoRecognizer];
        [[[self videoController] localView] addGestureRecognizer:self.localVideoFullscreenGestureRecognizer];
        //  [[[self videoController] localView] addGestureRecognizer:self.pauseResumeVideoRecognizer];
        [[[self videoController] remoteView] addGestureRecognizer:self.remoteVideoFullscreenGestureRecognizer];
        _videoPaused = NO;
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.remoteUsername.text = [self.call remoteUserId];
    [[self audioController] enableSpeaker];
    // Fetch user details and showthe name and picture
    [self loadData];
}

- (void)loadData {
    NSInteger value = [[self.call remoteUserId] integerValue];
    if (value > 0) {
        [RUser userWithId:@(value) completion:^(RUser *user) {
            dispatch_async(dispatch_get_main_queue(), ^{
                self.remoteUsername.text = [[user fullName] showThisIfEmpty:[self.call remoteUserId]];
                self.rLabelUsername.text = [[user fullName] showThisIfEmpty:[self.call remoteUserId]];
                self.rLabelCallStatus.text = @"Incoming...";
                [self.rImageViewUser sd_setImageWithURL:[NSURL URLWithString:user.ProfilePic] placeholderImage:kUserProfilePlaceholder];
            });
        }];
    }
}

#pragma mark - Call Actions

- (IBAction)accept:(id)sender {
    [[self audioController] stopPlayingSoundFile];
    [self.call answer];
}

- (IBAction)decline:(id)sender {
    [self.call hangup];
    [self dismiss];
}

- (IBAction)hangup:(id)sender {
    [self.call hangup];
    [self dismiss];
}

- (IBAction)onSwitchCameraTapped:(id)sender {
    AVCaptureDevicePosition current = self.videoController.captureDevicePosition;
    self.videoController.captureDevicePosition = SINToggleCaptureDevicePosition(current);
}

- (IBAction)onFullScreenTapped:(id)sender {
    UIView *view = [sender view];
    if ([view sin_isFullscreen]) {
        view.contentMode = UIViewContentModeScaleAspectFit;
        [view sin_disableFullscreen:YES];
    } else {
        view.contentMode = UIViewContentModeScaleAspectFill;
        [view sin_enableFullscreen:YES];
    }
}

- (IBAction)onPauseResumeVideoTapped:(id)sender {
    if (_videoPaused) {
        [self.call resumeVideo];
        _videoPaused = NO;
    } else {
        [self.call pauseVideo];
        _videoPaused = YES;
    }
}

- (void)onDurationTimer:(NSTimer *)unused {
    NSInteger duration = [[NSDate date] timeIntervalSinceDate:[[self.call details] establishedTime]];
    [self setDuration:duration];
}

#pragma mark - SINCallDelegate

- (void)callDidProgress:(id<SINCall>)call {
    [self setCallStatusText:@"Ringing..."];
    [[self audioController] startPlayingSoundFile:[self pathForSound:@"ringback.wav"] loop:YES];
    
    if (self.eventDelegate && [self.eventDelegate respondsToSelector:@selector(callEvent:controller:call:)]) {
        [self.eventDelegate callEvent:kCallEventTypeProgress controller:self call:call];
    }
}

- (void)callDidEstablish:(id<SINCall>)call {
    [self startCallDurationTimerWithSelector:@selector(onDurationTimer:)];
    self.remoteVideoView.hidden = NO;
    [self showButtons:kButtonsHangup];
    [[self audioController] stopPlayingSoundFile];
    self.rViewIncomingCall.hidden = YES;
    
    
    if (self.eventDelegate && [self.eventDelegate respondsToSelector:@selector(callEvent:controller:call:)]) {
        [self.eventDelegate callEvent:kCallEventTypeEstablished controller:self call:call];
    }
}

- (void)callDidEnd:(id<SINCall>)call {
    [self dismiss];
    [[self audioController] stopPlayingSoundFile];
    [self stopCallDurationTimer];
    [[self audioController] stopPlayingSoundFile];
    [[self audioController] disableSpeaker];
    
    if (self.eventDelegate && [self.eventDelegate respondsToSelector:@selector(callEvent:controller:call:)]) {
        [self.eventDelegate callEvent:kCallEventTypeEnd controller:self call:call];
    }
}

- (void)callDidAddVideoTrack:(id<SINCall>)call {
    [self.remoteVideoView addSubview:[self.videoController remoteView]];
    self.remoteVideoView.hidden = YES;
}

- (void)callDidPauseVideoTrack:(id<SINCall>)call {
    self.remoteVideoView.hidden = YES;
}

- (void)callDidResumeVideoTrack:(id<SINCall>)call {
    self.remoteVideoView.hidden = NO;
    self.rViewIncomingCall.hidden = YES;
}

- (void)showRemoteVideoView {
    [self.remoteVideoView addSubview:[self.videoController remoteView]];
    self.remoteVideoView.hidden = NO;
    self.rViewIncomingCall.hidden = YES;
}

#pragma mark - Sounds
- (NSString *)pathForSound:(NSString *)soundName {
    return [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:soundName];
}

@end
