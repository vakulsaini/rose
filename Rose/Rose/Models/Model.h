//
//  PPModel.h
//  PaintPad
//
//  Created by Vakul Saini on 14/06/18.
//  Copyright © 2018 TheTiger. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Model : NSObject

@property (strong, nonatomic) NSArray *excludedKeys;
@property (strong, nonatomic) NSDictionary *extraInfo;
@property (strong, nonatomic) NSNumber *createdAt;
@property (strong, nonatomic) NSNumber *updatedAt;

@end
