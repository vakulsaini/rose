//
//  ConsultUser.m
//  Rose
//
//  Created by Virender on 31/12/19.
//  Copyright © 2019 QualiteSoft. All rights reserved.
//

#import "ConsultUser.h"

@implementation ConsultUser


- (id)init {
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (id)initWithDicionary:(NSDictionary *)dicionary {
    self = [super init];
    if (self) {
        [self updateDictionary: dicionary];
    }
    return self;
}

- (void)updateDictionary:(NSDictionary *)dictionary {
    [self setDictionary:dictionary];
}

- (NSDictionary *)dictionaryObject {
    NSMutableDictionary *dict = [[self getDictionary] mutableCopy];
    return dict;
}

- (NSString *)Enable {
    if ([_Enable isKindOfClass:[NSString class]]) {
        return _Enable;
    }
    return @"0";
}

- (NSString *)EnableID {
    if ([_EnableID isKindOfClass:[NSString class]]) {
        return _EnableID;
    }
    return @"0";
}

- (BOOL)isEnabled {
    return [self.Enable integerValue] == 1;
}

@end
