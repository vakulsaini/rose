//
//  CallHistory.m
//  Rose
//
//  Created by Vakul Saini on 30/12/19.
//  Copyright © 2019 QualiteSoft. All rights reserved.
//

#import "CallHistory.h"

@implementation CallHistory

- (id)init {
    self = [super init];
    if (self) {
        self.CallEndDate = @"";
    }
    return self;
}

- (id)initWithDicionary:(NSDictionary *)dicionary {
    self = [super init];
    if (self) {
        [self updateDictionary: dicionary];
    }
    return self;
}

- (void)updateDictionary:(NSDictionary *)dictionary {
    [self setDictionary:dictionary];
}

- (NSDictionary *)dictionaryObject {
    NSMutableDictionary *dict = [[self getDictionary] mutableCopy];
    return dict;
}

- (CallType)callType {
    if (self.isCaller) {
        return CallTypeDialed;
    }
    else {
        // Check if Call end date exists
        if ([self.CallDuration integerValue] == 0) {
            // Missed call
            return CallTypeMissedCall;
        }
        else {
            // Received call
            return CallTypeReceived;
        }
    }
}

- (NSString *)name {
    if (self.isCaller) {
        return self.ReceiverName;
    }
    else {
        return self.CallerName;
    }
}

- (NSString *)location {
    if (self.isCaller) {
        // Receiver info
        return [[NSString stringWithFormat:@"%@, %@", self.ReceiverSpeciality, self.ReceiverLocation] trimmed];
    }
    else {
        // Caller Info
        return [[NSString stringWithFormat:@"%@, %@", self.CallerSpeciality, self.CallerLocation] trimmed];
    }
}

- (NSString *)profilePic {
    if (self.isCaller) {
        return self.ReceiverProfilepic;
    }
    else {
        return self.CallerProfilepic;
    }
}

- (NSString *)speciality {
    if (self.isCaller) {
        return self.ReceiverSpeciality;
    }
    else {
        return self.CallerSpeciality;
    }
}

- (NSString *)personID {
    if (self.isCaller) {
        return self.ReceiverID;
    }
    else {
        return self.CallerID;
    }
}

- (BOOL)isCaller {
    return [APP_DELEGATE.currentUser.Id integerValue] == [self.CallerID integerValue];
}

#pragma mark - Caller Getters
- (NSString *)CallerName {
    if ([_CallerName isKindOfClass:[NSString class]]) {
        return _CallerName;
    }
    return @"";
}

- (NSString *)CallerProfilepic {
    if ([_CallerProfilepic isKindOfClass:[NSString class]]) {
        return _CallerProfilepic;
    }
    return @"";
}

- (NSString *)CallerSpeciality {
    if ([_CallerSpeciality isKindOfClass:[NSString class]]) {
        return _CallerSpeciality;
    }
    return @"";
}


- (NSString *)CallerLocation {
    if ([_CallerLocation isKindOfClass:[NSString class]]) {
        return _CallerLocation;
    }
    return @"";
}

- (NSString *)CallerCity {
    if ([_CallerCity isKindOfClass:[NSString class]]) {
        return _CallerCity;
    }
    return @"";
}

#pragma mark - Receiver Getters
- (NSString *)ReceiverName {
    if ([_ReceiverName isKindOfClass:[NSString class]]) {
        return _ReceiverName;
    }
    return @"";
}

- (NSString *)ReceiverProfilepic {
    if ([_ReceiverProfilepic isKindOfClass:[NSString class]]) {
        return _ReceiverProfilepic;
    }
    return @"";
}

- (NSString *)ReceiverSpeciality {
    if ([_ReceiverSpeciality isKindOfClass:[NSString class]]) {
        return _ReceiverSpeciality;
    }
    return @"";
}


- (NSString *)ReceiverLocation {
    if ([_ReceiverLocation isKindOfClass:[NSString class]]) {
        return _ReceiverLocation;
    }
    return @"";
}

- (NSString *)ReceiverCity {
    if ([_ReceiverCity isKindOfClass:[NSString class]]) {
        return _ReceiverCity;
    }
    return @"";
}

- (NSString *)CallDuration {
    if ([_CallDuration isKindOfClass:[NSString class]]) {
        return _CallDuration;
    }
    return @"0";
}

@end
