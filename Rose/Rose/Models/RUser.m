//
//  RUser.m
//  Rose
//
//  Created by Vakul Saini on 23/12/19.
//  Copyright © 2019 QualiteSoft. All rights reserved.
//

#import "RUser.h"

@implementation RUser

- (id)init {
    self = [super init];
    if (self) {
        self.userType = UserTypePhysician;
        self.excludedKeys = @[@"Id", @"userType"];
    }
    return self;
}

/*- (NSString *)firstName {
    return [[self.name componentsSeparatedByString:@" "] firstObject];
}

- (NSString *)lastName {
    NSString *text = [[self.name componentsSeparatedByString:@" "] firstObject];
    return [self.name stringByReplacingOccurrencesOfString:text withString:@""];
}*/

- (void)syncWithCompletionBlock:(void (^)(void))block {
    
    if (!self.Id) {
        block();
        return;
    }
    
    NSString *profileAPIUrl = [NSString stringWithFormat:@"%@/id/%@", WEB_SERVICE_NEW_SIGNUP, self.Id];
    [[ApiHandler shared] sendHTTPRequest:APIRequestTypeGET url:profileAPIUrl params:nil withCompletion:^(id response, NSError *error) {
        if (error) {
            block();
            return;
        }
        
        if ([response isKindOfClass:[NSDictionary class]]) {
            if ([[response allKeys] containsObject:@"status"]) {
                if ([response[@"status"] boolValue]) {
                    if ([response[@"User"] isKindOfClass:[NSDictionary class]]) {
                        [self setUpDictionary:response[@"User"]];
                    }
                    else if ([response[@"User"] isKindOfClass:[NSArray class]] && [response[@"User"] count] > 0) {
                        NSDictionary *data = response[@"User"][0];
                        if ([data isKindOfClass:[NSDictionary class]]) {
                            [self setUpDictionary:response[@"data"]];
                        }
                    }
                }
            }
            else {
                [self setUpDictionary:response];
            }
        }
        
        block();
    }];
}

- (void)deleteAccountWithCompletionBlock:(APIClientCompletion)block {
    if (!self.Id) {
        NSError *err = [NSError errorWithDomain:@"com.ROSe" code:6000 userInfo:@{NSLocalizedDescriptionKey: @"User id is not valid or this user has been deleted already."}];
        block(nil, err);
        return;
    }

    NSString *deleteAPIUrl = [NSString stringWithFormat:@"%@/%@", WEB_SERVICE_NEW_SIGNUP, APP_DELEGATE.currentUser.Id];
    [[ApiHandler shared] sendHTTPRequest:APIRequestTypeDELETE url:deleteAPIUrl params:nil withCompletion:^(id response, NSError *error) {
        block(response, error);
    }];
}

+ (void)userWithId:(NSNumber *)Id completion:(void (^) (RUser *))block {
    RUser *user = [[RUser alloc] init];
    user.Id = Id;
    [user syncWithCompletionBlock:^{
        block(user);
    }];
}

- (void)setUpDictionary:(NSDictionary *)dict {
    [self setDictionary:dict];
    id Id = dict[@"Id"];
    
    if ([Id isKindOfClass:[NSNumber class]]) {
        self.Id = Id;
    } else {
        // String
        self.Id = @([Id integerValue]);
    }
    
    if (dict[@"User_type"] && [dict[@"User_type"] isKindOfClass:[NSString class]]) {
        self.userType = (UserType)[dict[@"User_type"] integerValue];
    }
}

- (BOOL)isPhysician {
    return self.userType == UserTypePhysician;
}

- (NSString *)location {
    return [[NSString stringWithFormat:@"%@, %@", self.Speciality, self.Location] trimmed];
}

- (NSDictionary *)dictionary {
    NSMutableDictionary *dict = [[self getDictionary] mutableCopy];
    dict[@"Id"] = self.Id;
    dict[@"User_type"] = [NSString stringWithFormat:@"%lu", (unsigned long)self.userType];
    return dict;
}

- (NSString *)fullName {
    
    if (![self.FirstName isKindOfClass:[NSString class]]) {
        self.FirstName = @"";
    }
    if (![self.LastName isKindOfClass:[NSString class]]) {
        self.LastName = @"";
    }
    
    return [[NSString stringWithFormat:@"%@ %@", self.FirstName, self.LastName] trimmed];
}

#pragma mark - Setters
- (void)setProfilePic:(NSString *)ProfilePic {
    if (![ProfilePic isKindOfClass:[NSString class]]) {
        _ProfilePic = @"";
    } else {
        _ProfilePic = ProfilePic;
    }
}

- (void)setLatitude:(NSNumber *)Latitude {
    if ([Latitude isKindOfClass:[NSString class]]) {
        _Latitude = @([Latitude doubleValue]);
    }
    else if ([Latitude isKindOfClass:[NSNumber class]]) {
        _Latitude = Latitude;
    }
    else {
        _Latitude = @0;
    }
}

- (void)setLongitude:(NSNumber *)Longitude {
    if ([Longitude isKindOfClass:[NSString class]]) {
        _Longitude = @([Longitude doubleValue]);
    }
    else if ([Longitude isKindOfClass:[NSNumber class]]) {
        _Longitude = Longitude;
    }
    else {
        _Longitude = @0;
    }
}

- (void)setFirstName:(NSString *)FirstName {
    if ([FirstName isKindOfClass:[NSString class]]) {
        _FirstName = FirstName;
    } else {
        _FirstName = @"";
    }
}

- (void)setLastName:(NSString *)LastName {
    if ([LastName isKindOfClass:[NSString class]]) {
        _LastName = LastName;
    } else {
        _LastName = @"";
    }
}
 
@end
