//
//  CallHistory.h
//  Rose
//
//  Created by Vakul Saini on 30/12/19.
//  Copyright © 2019 QualiteSoft. All rights reserved.
//

#import "Model.h"

typedef enum : NSUInteger {
    CallTypeMissedCall = 1,
    CallTypeReceived,
    CallTypeDialed
} CallType;

NS_ASSUME_NONNULL_BEGIN

@interface CallHistory : Model

@property (nonatomic, strong) NSString *ID;
@property (nonatomic, strong) NSString *SinchID;

@property (nonatomic, strong) NSString *CallerName;
@property (nonatomic, strong) NSString *CallerProfilepic;
@property (nonatomic, strong) NSString *CallerID;
@property (nonatomic, strong) NSString *CallerSpeciality;
@property (nonatomic, strong) NSString *CallerLocation;
@property (nonatomic, strong) NSString *CallerCity;
@property (nonatomic, strong) NSString *CallerType;

@property (nonatomic, strong) NSString *ReceiverName;
@property (nonatomic, strong) NSString *ReceiverProfilepic;
@property (nonatomic, strong) NSString *ReceiverID;
@property (nonatomic, strong) NSString *ReceiverSpeciality;
@property (nonatomic, strong) NSString *ReceiverLocation;
@property (nonatomic, strong) NSString *ReceiverCity;
@property (nonatomic, strong) NSString *ReceiverType;

@property (nonatomic, strong) NSString *CallStatus;
@property (nonatomic, strong) NSString *CallPlaceDate;
@property (nonatomic, strong) NSString *CallEndDate;
@property (nonatomic, strong) NSString *CallDuration;

- (id)initWithDicionary:(NSDictionary *)dicionary;
- (void)updateDictionary:(NSDictionary *)dictionary;
- (NSDictionary *)dictionaryObject;
- (CallType)callType;
- (NSString *)name;
- (NSString *)location;
- (NSString *)profilePic;
- (NSString *)speciality;
- (NSString *)personID;
- (BOOL)isCaller;

@end

NS_ASSUME_NONNULL_END
