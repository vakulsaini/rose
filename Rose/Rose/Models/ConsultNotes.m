//
//  ConsultNotes.m
//  Rose
//
//  Created by Vakul Saini on 01/02/20.
//  Copyright © 2020 QualiteSoft. All rights reserved.
//

#import "ConsultNotes.h"

@implementation ConsultNotes

@synthesize ConsultID = _ConsultID;
@synthesize ConsultantID = _ConsultantID;
@synthesize ConsultantName = _ConsultantName;
@synthesize User_type = _User_type;
@synthesize ConsultationNote = _ConsultationNote;
@synthesize Signature = _Signature;
@synthesize ConsultationDate = _ConsultationDate;

- (id)init {
    self = [super init];
    if (self) {
        self.userType = UserTypePhysician;
        self.excludedKeys = @[@"userType"];
    }
    return self;
}

- (id)initWithDicionary:(NSDictionary *)dicionary {
    self = [super init];
    if (self) {
        [self updateDictionary: dicionary];
    }
    return self;
}

- (void)updateDictionary:(NSDictionary *)dictionary {
    [self setDictionary:dictionary];
    
    if (dictionary[@"User_type"] && [dictionary[@"User_type"] isKindOfClass:[NSString class]]) {
        self.userType = (UserType)[dictionary[@"User_type"] integerValue];
    }
}

- (NSDictionary *)dictionaryObject {
    NSMutableDictionary *dict = [[self getDictionary] mutableCopy];
    dict[@"User_type"] = [NSString stringWithFormat:@"%lu", (unsigned long)self.userType];
    return dict;
}


#pragma mark - Getters
- (NSString *)ConsultID {
    if ([_ConsultID isKindOfClass:[NSString class]]) {
        return _ConsultID;
    }
    return @"";
}

- (NSString *)ConsultantID {
    if ([_ConsultantID isKindOfClass:[NSString class]]) {
        return _ConsultantID;
    }
    return @"";
}

- (NSString *)ConsultantName {
    if ([_ConsultantName isKindOfClass:[NSString class]]) {
        return _ConsultantName;
    }
    return @"";
}

- (NSString *)User_type {
    if ([_User_type isKindOfClass:[NSString class]]) {
        return _User_type;
    }
    return @"1";
}

- (NSString *)ConsultationNote {
    if ([_ConsultationNote isKindOfClass:[NSString class]]) {
        return _ConsultationNote;
    }
    return @"";
}

- (NSString *)Signature {
    if ([_Signature isKindOfClass:[NSString class]]) {
        return _Signature;
    }
    return @"";
}

- (NSString *)ConsultationDate {
    if ([_ConsultationDate isKindOfClass:[NSString class]]) {
        return _ConsultationDate;
    }
    return @"";
}

#pragma mark - Setters
- (void)setConsultID:(NSString *)ConsultID {
    if ([ConsultID isKindOfClass:[NSString class]]) {
        _ConsultID = ConsultID;
    }
    else {
        _ConsultID = @"";
    }
}

- (void)setConsultantID:(NSString *)ConsultantID {
    if ([ConsultantID isKindOfClass:[NSString class]]) {
        _ConsultantID = ConsultantID;
    }
    else {
        _ConsultantID = @"";
    }
}

- (void)setConsultantName:(NSString *)ConsultantName {
    if ([ConsultantName isKindOfClass:[NSString class]]) {
        _ConsultantName = ConsultantName;
    }
    else {
        _ConsultantName = @"";
    }
}

- (void)setUser_type:(NSString *)User_type {
    if ([User_type isKindOfClass:[NSString class]]) {
        _User_type = User_type;
    }
    else {
        _User_type = @"1";
    }
}

- (void)setConsultationNote:(NSString *)ConsultationNote {
    if ([ConsultationNote isKindOfClass:[NSString class]]) {
        _ConsultationNote = ConsultationNote;
    }
    else {
        _ConsultationNote = @"";
    }
}

- (void)setSignature:(NSString *)Signature {
    if ([Signature isKindOfClass:[NSString class]]) {
        _Signature = Signature;
    }
    else {
        _Signature = @"";
    }
}

- (void)setConsultationDate:(NSString *)ConsultationDate {
    if ([ConsultationDate isKindOfClass:[NSString class]]) {
        _ConsultationDate = ConsultationDate;
    }
    else {
        _ConsultationDate = @"";
    }
}

@end
