//
//  Notification.m
//
//  Created by Vakul Saini on 06/12/18.
//  Copyright © 2018 TheTiger. All rights reserved.
//

#import "Notification.h"

@implementation Notification

- (id)init {
    self = [super init];
    if (self) {

    }
    return self;
}

- (id)initWithInfo:(NSDictionary *)info {
    self = [super init];
    if (self) {
        [self setDictionary:info];
    }
    return self;
}

- (NSString *)combinedMessage {
    NSString *msg = [NSString stringWithFormat:@"You have a missed from %@.", self.CallerName];
    return msg;
}

@end
