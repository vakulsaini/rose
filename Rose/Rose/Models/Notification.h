//
//  Notification.h
//
//  Created by Vakul Saini on 06/12/18.
//  Copyright © 2018 TheTiger. All rights reserved.
//

#import "Model.h"

@interface Notification : Model

/*@property (nonatomic, strong) NSNumber *NotificationID;
@property (nonatomic, strong) NSString *Message;
@property (nonatomic, strong) NSString *ReadbyMe;
@property (nonatomic, strong) NSNumber *RowID;
@property (nonatomic, strong) NSString *NotificationDate;*/


@property (nonatomic, strong) NSString *ID;
@property (nonatomic, strong) NSString *SinchID;
@property (nonatomic, strong) NSString *CallerName;
@property (nonatomic, strong) NSString *CallerProfilepic;
@property (nonatomic, strong) NSString *CallerID;
@property (nonatomic, strong) NSString *CallerSpeciality;
@property (nonatomic, strong) NSString *CallerCity;
@property (nonatomic, strong) NSString *CallerType;
@property (nonatomic, strong) NSString *CallDate;

- (id)initWithInfo:(NSDictionary *)info;
- (NSString *)combinedMessage;

@end
