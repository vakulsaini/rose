//
//  RUser.h
//  Rose
//
//  Created by Vakul Saini on 23/12/19.
//  Copyright © 2019 QualiteSoft. All rights reserved.
//

#import "Model.h"
#import "ApiHandler.h"

typedef enum : NSUInteger {
    UserTypePhysician = 1,
    UserTypeConsultant,
} UserType;


NS_ASSUME_NONNULL_BEGIN

@interface RUser : Model

@property (nonatomic, strong) NSNumber *Id;
@property (nonatomic, strong) NSString *FirstName;
@property (nonatomic, strong) NSString *LastName;
@property (nonatomic, strong) NSString *Email;
@property (nonatomic, strong) NSString *DOB;
@property (nonatomic, strong) NSString *Phone;
@property (nonatomic, strong) NSString *Speciality;
@property (nonatomic, strong) NSString *MSPNumber;
@property (nonatomic) UserType userType;
@property (nonatomic, strong) NSString *City;
@property (nonatomic, strong) NSString *Location;
@property (nonatomic, strong) NSString *ProfilePic;
@property (nonatomic, strong) NSNumber *Latitude;
@property (nonatomic, strong) NSNumber *Longitude;
@property (nonatomic, strong) NSNumber *Status;
@property (nonatomic, strong) NSNumber *ConsultantNotification;
@property (nonatomic, strong) NSNumber *TranscriptionNotification;


- (void)setUpDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionary;
- (void)syncWithCompletionBlock:(void (^) (void))block;
- (void)deleteAccountWithCompletionBlock:(APIClientCompletion)block;
- (NSString *)fullName;
+ (void)userWithId:(NSNumber *)Id completion:(void (^) (RUser *))block;
- (BOOL)isPhysician;
- (NSString *)location;

@end

NS_ASSUME_NONNULL_END
