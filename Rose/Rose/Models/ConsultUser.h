//
//  ConsultUser.h
//  Rose
//
//  Created by Virender on 31/12/19.
//  Copyright © 2019 QualiteSoft. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ConsultUser : RUser
@property (strong, nonatomic) NSString *EnableID;
@property (strong, nonatomic) NSString *Enable;

- (BOOL)isEnabled;
@end

NS_ASSUME_NONNULL_END
