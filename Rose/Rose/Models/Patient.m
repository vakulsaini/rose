//
//  Patient.m
//  Rose
//
//  Created by Vakul Saini on 27/01/20.
//  Copyright © 2020 QualiteSoft. All rights reserved.
//

#import "Patient.h"

@implementation Patient

- (id)init {
    self = [super init];
    if (self) {
        self.userType = UserTypePhysician;
        self.excludedKeys = @[@"userType"];
    }
    return self;
}

- (id)initWithDicionary:(NSDictionary *)dicionary {
    self = [super init];
    if (self) {
        [self updateDictionary: dicionary];
    }
    return self;
}

- (void)updateDictionary:(NSDictionary *)dictionary {
    [self setDictionary:dictionary];
    
    if (dictionary[@"User_type"] && [dictionary[@"User_type"] isKindOfClass:[NSString class]]) {
        self.userType = (UserType)[dictionary[@"User_type"] integerValue];
    }
}

- (NSDictionary *)dictionaryObject {
    NSMutableDictionary *dict = [[self getDictionary] mutableCopy];
    dict[@"User_type"] = [NSString stringWithFormat:@"%lu", (unsigned long)self.userType];
    return dict;
}


- (void)setPatientPhone:(NSString *)PatientPhone {
    _PatientPhone = PatientPhone;
}

- (void)setUserPhone:(NSString *)UserPhone {
    _UserPhone = UserPhone;
}

#pragma mark - Getters
- (NSString *)PatientName {
    if ([_PatientName isKindOfClass:[NSString class]]) {
        return _PatientName;
    }
    return @"";
}

- (NSString *)PatientDOB {
    if ([_PatientDOB isKindOfClass:[NSString class]]) {
        return _PatientDOB;
    }
    return @"";
}

- (NSString *)PatientPHN {
    if ([_PatientPHN isKindOfClass:[NSString class]]) {
        return _PatientPHN;
    }
    return @"";
}

- (NSString *)PatientCreateDate {
    if ([_PatientCreateDate isKindOfClass:[NSString class]]) {
        return _PatientCreateDate;
    }
    return @"";
}

- (NSString *)ReferringSite {
    if ([_ReferringSite isKindOfClass:[NSString class]]) {
        return _ReferringSite;
    }
    return @"";
}

- (NSString *)MSP {
    if ([_MSP isKindOfClass:[NSString class]]) {
        return _MSP;
    }
    return @"";
}

- (NSString *)Note {
    if ([_Note isKindOfClass:[NSString class]]) {
        return _Note;
    }
    return @"";
}


@end
