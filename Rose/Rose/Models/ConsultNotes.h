//
//  ConsultNotes.h
//  Rose
//
//  Created by Vakul Saini on 01/02/20.
//  Copyright © 2020 QualiteSoft. All rights reserved.
//

#import "Model.h"

NS_ASSUME_NONNULL_BEGIN

@interface ConsultNotes : Model

@property (nonatomic, strong) NSString *ConsultID;
@property (nonatomic, strong) NSString *ConsultantID;
@property (nonatomic, strong) NSString *ConsultantName;
@property (nonatomic, strong) NSString *User_type;
@property (nonatomic, strong) NSString *ConsultationNote;
@property (nonatomic, strong) NSString *Signature;
@property (nonatomic, strong) NSString *ConsultationDate;
@property (nonatomic) UserType userType;

- (id)initWithDicionary:(NSDictionary *)dicionary;

- (void)updateDictionary:(NSDictionary *)dictionary;

- (NSDictionary *)dictionaryObject;

@end

NS_ASSUME_NONNULL_END
