//
//  Patient.h
//  Rose
//
//  Created by Vakul Saini on 27/01/20.
//  Copyright © 2020 QualiteSoft. All rights reserved.
//

#import "RUser.h"

NS_ASSUME_NONNULL_BEGIN

@interface Patient : RUser

@property (nonatomic, strong) NSString *PatientId;
@property (nonatomic, strong) NSString *MSP;
@property (nonatomic, strong) NSString *Note;
@property (nonatomic, strong) NSString *PatientCreateDate;
@property (nonatomic, strong) NSString *PatientDOB;
@property (nonatomic, strong) NSString *PatientName;
@property (nonatomic, strong) NSString *PatientPHN;
@property (nonatomic, strong) NSString *PatientPhone;
@property (nonatomic, strong) NSString *UserPhone;
@property (nonatomic, strong) NSString *ReferralName;
@property (nonatomic, strong) NSString *ReferringID;
@property (nonatomic, strong) NSString *ReferringSite;

- (id)initWithDicionary:(NSDictionary *)dicionary;

- (void)updateDictionary:(NSDictionary *)dictionary;

- (NSDictionary *)dictionaryObject;

@end

NS_ASSUME_NONNULL_END
