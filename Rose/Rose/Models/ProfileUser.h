//
//  ProfileUser.h
//  Rose
//
//  Created by Vakul Saini on 31/12/19.
//  Copyright © 2019 QualiteSoft. All rights reserved.
//

#import "Model.h"

NS_ASSUME_NONNULL_BEGIN

@interface ProfileUser : Model

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *image;
@property (nonatomic, strong) NSString *hospital;
@property (nonatomic, strong) NSString *phoneNumber;
@property (nonatomic, strong) NSDate   *date;

- (id)initWithDicionary:(NSDictionary *)dicionary;

- (void)updateDictionary:(NSDictionary *)dictionary;

- (NSDictionary *)dictionaryObject;

@end

NS_ASSUME_NONNULL_END
