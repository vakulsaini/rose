//
//  ProfileUser.m
//  Rose
//
//  Created by Vakul Saini on 31/12/19.
//  Copyright © 2019 QualiteSoft. All rights reserved.
//

#import "ProfileUser.h"

@implementation ProfileUser

- (id)init {
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (id)initWithDicionary:(NSDictionary *)dicionary {
    self = [super init];
    if (self) {
        [self updateDictionary: dicionary];
    }
    return self;
}

- (void)updateDictionary:(NSDictionary *)dictionary {
    [self setDictionary:dictionary];
}

- (NSDictionary *)dictionaryObject {
    NSMutableDictionary *dict = [[self getDictionary] mutableCopy];
    return dict;
}


@end
